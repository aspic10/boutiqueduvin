<?php
/**
 * Méthodes statiques pour les clients (utilisateurs)
 */
namespace App\Helpers;

use App\Models\City;

class ClientHelper {
    /**
     * Récupérer ou créer la ville
     */
    public static function getCity($city, $postalCode, $countryId = 1){
        return City::firstOrCreate([
            'name' => $city,
            'postal_code' => $postalCode,
            'country_id' => $countryId,
        ]);
    }
}
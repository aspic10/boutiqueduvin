<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\Client;
use App\Models\Company;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Helpers\ClientHelper;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'email/verify';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'lastname' => ['required', 'string', 'max:120'],
            'firstname' => ['required', 'string', 'max:120'],
            'company' => ['nullable', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'postal_code' => ['required', 'integer'],
            'city' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', new \App\Rules\UniqueEmailClient],
            'phone' => ['nullable', 'string', 'max:20'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    
    /**
     * Modifier le nom pour qu'il ne reste que les trois premiers caractères en 
     * majuscules -> pour utiliser comme code du client
     */
    private function transformNameToCode($string){
        // To uppercase
        $string = mb_strtoupper($string);
        
        // Replace accented characters
        $string = strtr(utf8_decode($string), utf8_decode('ÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'AAAAACEEEEIIIINOOOOOUUUUY');
        
        // Only keep A-Z
        $string = preg_replace("/[^A-Z]+/", "", $string);
        
        // Only keep first 3 characters
        $string = substr($string, 0, 3);
        
        return $string;
    }
    
    
    /**
     * Create a new client instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\Client
     */
    protected function create(array $data) {
        $lastname = trim($data['lastname']);
        $firstname = trim($data['firstname']);
        
        // Modifier le nom et prénom pour former le code
        $lastnameCode = $this->transformNameToCode($lastname);
        $firstnameCode = $this->transformNameToCode($firstname);
        
        // Générer un code unique
        $code = "SHOP-" . $lastnameCode . "_" . $firstnameCode;
        $newCode = $code;
        $codeExists = true;
        $i = 0;
        while($codeExists == true){
            // Code déjà utilisé -> ajouter un chiffre
            if(Company::where('code', $newCode)->exists()) {
                $newCode = $code . "_" . ++$i;
            }
            // Code OK
            else {
                $codeExists = false;
            }
        }
        
        // Récupérer ou créer la ville
        $city = ClientHelper::getCity($data['city'], $data['postal_code']);
        
        // Créer une entreprise
        $companyId = Company::create([
            'code' => $newCode,
            'name' => $firstname . " " . $lastname,
            'address' => $data['address'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'city_id' => $city->id,
        ])->id;
        
        // Créer le client
        $client = new Client();
        
        $client->id = $companyId;
        $client->password = Hash::make($data['password']);
        $client->company_name = $data['company'];
        
        $client->save();
        
        return $client;
    }
}
<?php
/**
 * Contrôleur pour les clients (utilisateurs)
 */
namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Helpers\ClientHelper;
use App\Models\Bill;
use App\Models\ClientsArticle;

class ClientController extends Controller {
    /**
     * Liste des commandes du client
     */
    public function bills() {
        $client = Auth::user();
        
        $bills = Bill::where("client_id", $client->id)
            ->orderBy("id", "DESC")
            ->paginate(10);
        
        return view('shop.client.bills', ["bills" => $bills]);
    }
    
    
    /**
     * Détail d'une commande
     */
    public function bill($billId){
        $bill = \App\Models\Bill::find($billId);
        $client = Auth::user();
        
        // Commande inexistante ou pas au client -> rediriger à l'accueil
        if(is_null($bill) || $client != $bill->client){
            return redirect(route("home"));
        }
        else {
            return view('shop.client.bill', ["client" => $client, "bill" => $bill]);
        }
    }
    
    
    /**
     * Vins favoris du client
     */
    public function wines() {
        $client = Auth::user();
        
        $articles = $client->articles;
        
        return view('shop.client.wines', ["articles" => $articles]);
    }
    
    
    /**
     * Ajouter / Retirer un article des favoris
     */
    public function setFavourite(Request $request){
        $client = Auth::user();
        $clientId = $client->id;
        $articleId = $request->post("articleId");
//        $article = \App\Models\Article::find($articleId);
        
        $clientArticle = ClientsArticle::where([
            ["client_id", "=", $clientId],
            ["article_id", "=", $articleId],
        ])
        ->first();

        if(is_null($clientArticle)){
            $clientArticle = new ClientsArticle();
            $clientArticle->client_id = $clientId;
            $clientArticle->article_id = $articleId;
            $clientArticle->quantity = "";
            $clientArticle->save();
            $favourite = 1;
            
        }
        else {
            ClientsArticle::where([
                ["client_id", "=", $clientId],
                ["article_id", "=", $articleId],
            ])->delete();
            $favourite = 0;
        }
        
//        
//        if($client->articles->contains($articleId)){
//            $client->articles->detach($articleId);
//            
//        }
//        else {
//            
//            
//            $client->articles->save($article);
//            
//        }
        
//        $client->save();
        
        return $favourite;
    }
    
    
    /**
     * Bon cadeaux du client
     */
    public function giftCards() {
        $client = Auth::user();
        
        return view('shop.client.gift-cards', $client->getGiftCards());
    }
    
    
    /**
     * Mettre à jour les coordonnées du client
     */
    public function update(Request $request){
        $addressArray = $request->post("address");
        
        // Récupérer ou créer la ville
        $city = ClientHelper::getCity($addressArray["city"], $addressArray["postal-code"]);
        
        // Mettre à jour le client
        $client = Auth::user();
        
        $client->company_name = $addressArray["company"];
        
        // Mettre à jour l'entreprise
        $company = $client->company;
        
        $company->name = $addressArray["name"];
        $company->address = $addressArray["address"];
        $company->phone = $addressArray["phone"];
        $company->city_id = $city->id;
        
        // Email modifié
        if(isset($addressArray["email"]) && $addressArray["email"] != $company->email){
            $company->email = $addressArray["email"];
            
            // Renvoyer un email de vérification
            $client->email_verified_at = null;
            $client->sendEmailVerificationNotification();
        }
        
        $client->save();
        $company->save();
    }
    
    
    /**
     * Changer le mot de passe
     */
    public function changePassword(Request $request){
        // Vérifier le formulaire
        $request->validate([
            "current_password" => "required",
            "new_password" => "required|string|min:8|confirmed",
            "new_password_confirmation" => "required",
        ]);
        
        $client = Auth::user();
        
        // Vérifier le mot de passe actuel
        if(!Hash::check($request->current_password, $client->password)) {
            return back()->withErrors(["current_password" => "Mot de passe incorrect"]);
        }
        // Changer le mot de passe
        else {
            $client->password = Hash::make($request->new_password);
            $client->save();

            return back()->with("success", "OK");
        }
    }
    
    
    /**
     * Supprimer le compte
     */
    public function deleteAccount(Request $request){
        $client = Auth::user();
        $company = $client->company;
        
        // Anonymiser les informations
        $client->contact = null;
        $client->company_name = null;
        $client->password = null;
        $client->email_verified_at = null;
        
        $company->name = "Compte supprimé";
        $company->address = "";
        $company->phone = null;
        $company->email = null;
        $company->remark = "Compte supprimé le " . date("d.m.Y");
        $company->archived = true;
        
        $client->save();
        $company->save();
        
        // Déconnecter l'utilisateur connecté
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
    }
}
<?php
/**
 * Contrôleur pour la page d'accueil
 */
namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Enomatic;
use App\Models\ShopIcon;
use App\Models\ShopSlide;
use App\Models\SiteText;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class IndexController extends Controller {
    /**
     * Page d'accueil
     */
    public function index() {
        // Texte d'introduction
        $text = SiteText::where("label", "accueil")
            ->firstOr(function(){
                return new SiteText();
            });
            
        // Slides
        $slides = ShopSlide::orderBy("rank", "ASC")
            ->get();
        
        // Retirer du slider si les articles ne sont pas visibles sur le shop
        foreach($slides as $key => $slide){
            $article = $slide->article;
            
            if(!$article->isVisible()){
                $slides->forget($key);
            }
        }
        
        // Icônes
        $icons = ShopIcon::orderBy("rank", "ASC")
            ->get();
        
        return view('shop.index.index', [
            "slides" => $slides,
            "icons" => $icons,
            "text" => $text->content,
        ]);
    }
    
    
    /**
     * CGV
     */
    public function terms() {
        // Texte des CGV
        $terms = SiteText::where("label", "cgv")
            ->firstOr(function(){
                return new SiteText();
            });
            
        return view('shop.index.terms', [
            "terms" => $terms->content,
        ]);
    }
    
    
    /**
     * Contact et horaires
     */
    public function contact() {
        // Texte des horaires
        $hours = SiteText::where("label", "horaires")
            ->firstOr(function(){
                return new SiteText();
            });
            
        return view('shop.index.contact', [
            "hours" => $hours->content,
        ]);
    }
    
    
    /**
     * Présentation de l'oenothèque
     */
    public function presentation() {
        // Photos de l'oenothèque
        $photosOenotheque = $this->getPhotosArray("img/oenotheque");
        
        // Photos du magasin
        $photosMagasin = $this->getPhotosArray("img/magasin");
        
        // Photos des cadeaux
        $photosCadeaux = $this->getPhotosArray("img/cadeaux");
        
        return view('shop.index.presentation', [
            "photosOenotheque" => $photosOenotheque,
            "photosMagasin" => $photosMagasin,
            "photosCadeaux" => $photosCadeaux,
        ]);
    }
    
    
    /**
     * Espace Découverte
     */
    public function decouverte() {
        $today = new \DateTime();
        
        // Enomatic actuel
        $enomatic = Enomatic::where("from", "<=", $today)
            ->where("to", ">=", $today)
            ->first();
        
        return view('shop.index.decouverte', [
            "enomatic" => $enomatic
        ]);
    }
    
    
    /**
     * Récupérer toutes les photos d'un dossier
     */
    private function getPhotosArray($folder){
        $supportedFormats = array("gif","jpg","jpeg","png");
        
        $resultArray = array();
        $files = glob($folder . "/*.*");
        foreach($files as $filename){
            $extension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
            if(in_array($extension, $supportedFormats)){
                $resultArray = $this::getPhotoInfo($filename, $resultArray);
            }
        }
        
        return $resultArray;
    }
}
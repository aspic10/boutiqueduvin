<?php
/**
 * Contrôleur pour les producteurs
 */
namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Wine;
use App\Models\Region;
use App\Models\Country;
use App\Models\Producer;

class ProducerController extends Controller {
    /**
     * Liste des producteurs
     */
    public function index(int $countryId = null,
        int $regionId = null,
        $search = "") {
        /***********************************************************************
         * Récupérer les valeurs des filtres
         **********************************************************************/
        // Requête de base
        $filtersQbBase = \Illuminate\Support\Facades\DB::table("producers")
            ->join("articles", "articles.producer_id", "=", "producers.id");
        
        $filtersQb = Wine::defaultQueryElements($filtersQbBase);
        
        // Filtre des pays
        $countriesQb = clone $filtersQb;
        $countriesFilter = $countriesQb->select("countries.*")
            ->join("regions", "regions.id", "=", "producers.region_id")
            ->join("countries", "countries.id", "=", "regions.country_id")
            ->orderBy("countries.name", "asc")
            ->distinct()
            ->get();
        
        // Filtre des régions
        $regionsQb = clone $filtersQb;
        $regionsFilter = $regionsQb->select("regions.*")
            ->join("regions", "regions.id", "=", "producers.region_id")
            ->orderBy("regions.name", "asc")
            ->distinct()
            ->get();
        
        
        /***********************************************************************
         * Appliquer les filtres sélectionnés
         **********************************************************************/
        $region = Region::find($regionId);
        $country = Country::find($countryId);
        
        // Requête de base
        $queryBase = Producer::select("producers.*")
            ->join("articles", "articles.producer_id", "=", "producers.id")
            ->orderBy("producers.name", "asc")
            ->groupBy("producers.id");
        
        $producersQb = Wine::defaultQueryElements($queryBase);
        
        $appliedFilters = array();
        
        // Recherche
        if($search != ""){
            $searchArray = explode(" ", $search);
            
            foreach($searchArray as $searchItem){
                if(trim($searchItem) != ""){
                    $producersQb->where("producers.name", "like", "%" . addcslashes(trim($searchItem), "%_") . "%");
                }
            }
            
            $appliedFilters[] = array(
                "icon" => "fas fa-search",
                "label" => $search,
                "function" => "resetInput('search')",
            );
        }
        
        // Région
        if(!is_null($region)){
            // La région n'est pas dans le pays sélectionné
            if($region->country != $country){
                $region = null;
                $regionId = null;
            }
            else {
                $producersQb->where("producers.region_id", $region->id);
                
                $appliedFilters[] = array(
                    "icon" => "fas fa-map-marked-alt",
                    "label" => $region->name,
                    "function" => "resetSelect('region')",
                );
            }
        }
        
        // Pays
        if(!is_null($country)){
            $producersQb->join("regions", "regions.id", "=", "producers.region_id")
                ->where("regions.country_id", $country->id);
            
            $appliedFilters[] = array(
                "icon" => "fas fa-globe-americas",
                "label" => $country->name,
                "function" => "resetSelect('country')",
            );
        }
        
        $producers = $producersQb->paginate(12);
        
        return view('shop.producer.index', [
            "producers" => $producers,
            
            "appliedFilters" => $appliedFilters,
            
            // Valeurs des filtres sélectionnés
            "values" => array(
                "country" => $countryId,
                "region" => $regionId,
                "search" => $search,
            ),
            
            // Filtres
            "filters" => array(
                "countries" => $countriesFilter,
                "regions" => $regionsFilter,
            ),
        ]);
    }
    
    
    /**
     * Détail d'un producteur
     */
    public function detail(Producer $producer) {
        $producerPhotos = array();

        $producerWines = Wine::take(4)
            ->select("wines.*", "articles.*", "prices.price AS shopPrice")
            ->leftJoin('articles', 'articles.id', '=', 'wines.id')
            ->join('prices', 'prices.article_id', '=', 'articles.id')
            ->join('clients_types', 'clients_types.id', '=', 'prices.client_type_id')
            ->where([
                ['articles.archived', '=', false],
                ['articles.shop', '=', true],
                ['clients_types.name', '=', "Shop"],
                ['articles.producer_id', '=', $producer->id],
            ])
            ->get();

        // Nombre de vins du producteur
        $nbProducerWines = Wine::select("wines.*")
            ->leftJoin('articles', 'articles.id', '=', 'wines.id')
            ->join('prices', 'prices.article_id', '=', 'articles.id')
            ->join('clients_types', 'clients_types.id', '=', 'prices.client_type_id')
            ->where([
                ['articles.archived', '=', false],
                ['articles.shop', '=', true],
                ['clients_types.name', '=', "Shop"],
                ['articles.producer_id', '=', $producer->id],
            ])
            ->count();

        // Photos du producteur
        foreach($producer->photos as $producerPhoto) {
            $filename = config('app.photo_url') . "producer/" . $producer->id . "/" . $producerPhoto->name;

            $producerPhotos = $this::getPhotoInfo($filename, $producerPhotos);
        }

        return view('shop.producer.detail', [
            "producer" => $producer,
            "producerWines" => $producerWines,
            "producerPhotos" => $producerPhotos,
            "nbProducerWines" => $nbProducerWines,
        ]);
    }
}
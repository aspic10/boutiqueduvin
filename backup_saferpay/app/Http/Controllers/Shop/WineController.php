<?php
/**
 * Contrôleur pour les vins
 */
namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Wine;
use App\Models\Colour;
use App\Models\Country;
use App\Models\Region;
use App\Models\Producer;
use App\Models\Variety;
use App\Models\Characteristic;
use App\Models\Appellation;

use Illuminate\Support\Facades\DB;

class WineController extends Controller {
    /**
     * Liste des vins
     */
    public function index(
        int $producerId = null,
        int $colourId = null,
        int $countryId = null,
        int $regionId = null,
        int $varietyId = null,
//        $characteristics = "",
        int $year = 0,
        float $litre = 0,
        bool $favourite = null,
//        bool $sale = null,
        bool $distinction = null,
        bool $rare = null,
        int $appellationId = null,
        int $minPrice = 0,
        int $maxPrice = 0,
        bool $available = true,
        $search = "") {
        /***********************************************************************
         * Récupérer les valeurs des filtres
         **********************************************************************/
        // Requête de base
        $filtersQb = Wine::defaultQueryFilters();
        
        // Filtre des couleurs
        $coloursQb = clone $filtersQb;
        $coloursFilter = $coloursQb->select("colours.*")
            ->join("colours", "colours.id", "=", "wines.colour_id")
            ->orderBy("colours.name", "asc")
            ->distinct()
            ->get();
        
        // Filtre des pays
        $countriesQb = clone $filtersQb;
        $countriesFilter = $countriesQb->select("countries.*")
            ->join("regions", "regions.id", "=", "wines.region_id")
            ->join("countries", "countries.id", "=", "regions.country_id")
            ->orderBy("countries.name", "asc")
            ->distinct()
            ->get();
        
        // Filtre des régions
        $regionsQb = clone $filtersQb;
        $regionsFilter = $regionsQb->select("regions.*")
            ->join("regions", "regions.id", "=", "wines.region_id")
            ->orderBy("regions.name", "asc")
            ->distinct()
            ->get();
        
        // Filtre des producteurs
        $producersQb = clone $filtersQb;
        $producersFilter = $producersQb->select("producers.id", "producers.name")
            ->join("producers", "producers.id", "=", "articles.producer_id")
            ->orderBy("producers.name", "asc")
            ->distinct()
            ->get();
        
        // Filtre des appellations
        $appellationsQb = clone $filtersQb;
        $appellationsFilter = $appellationsQb->select("appellations.id", "appellations.name")
            ->join("appellations", "appellations.id", "=", "wines.appellation_id")
            ->orderBy("appellations.name", "asc")
            ->distinct()
            ->get();
        
        // Filtre des cépages
        $varietiesQb = clone $filtersQb;
        $varietiesFilter = $varietiesQb->select("varieties.*")
            ->join("wines_varieties", "wines_varieties.wine_id", "=", "wines.id")
            ->join("varieties", "varieties.id", "=", "wines_varieties.variety_id")
            ->orderBy("varieties.name", "asc")
            ->distinct()
            ->get();
        
//        // Filtre des caractéristiques
//        $characteristicsQb = clone $filtersQb;
//        $characteristicsFilter = $characteristicsQb->select("characteristics.*")
//            ->join("wines_characteristics", "wines_characteristics.wine_id", "=", "wines.id")
//            ->join("characteristics", "characteristics.id", "=", "wines_characteristics.characteristic_id")
//            ->orderBy("characteristics.name", "asc")
//            ->distinct()
//            ->get();
        
        // Filtre des millésimes
        $yearsQb = clone $filtersQb;
        $yearsFilter = $yearsQb->select("wines.year AS id", "wines.year AS name")
            ->whereNotNull("wines.year")
            ->orderBy("wines.year", "asc")
            ->distinct()
            ->get();
        
        $yearFound = false;
        foreach($yearsFilter as $yearFilter){
            if($year == $yearFilter->id){
                $yearFound = true;
                break;
            }
        }
        
        if($yearFound == false){
            $year = 0;
        }
        
        // Filtre des litrages
        $litresQb = clone $filtersQb;
        $litresFilter = $litresQb->select("articles.litres AS id",
                DB::raw("CONCAT(articles.litres,'cl') AS name"))
            ->whereNotNull("articles.litres")
            ->orderBy("articles.litres", "asc")
            ->distinct()
            ->get();
        
        $litreFound = false;
        foreach($litresFilter as $litreFilter){
            if($litre == $litreFilter->id){
                $litreFound = true;
                break;
            }
        }
        
        if($litreFound == false){
            $litre = 0;
        }
        
        // Filtre des prix
        $minPriceQb = clone $filtersQb;
        $maxPriceQb = clone $filtersQb;
        $minPriceFilter = floor($minPriceQb->min("prices.price"));
        $maxPriceFilter = ceil($maxPriceQb->max("prices.price"));
        
        if($minPrice < $minPriceFilter || $minPrice > $maxPrice){
            $minPrice = $minPriceFilter;
        }
        
        if($maxPrice == 0 || $maxPrice > $maxPriceFilter){
            $maxPrice = $maxPriceFilter;
        }
        
        
        /***********************************************************************
         * Appliquer les filtres sélectionnés
         **********************************************************************/
        $colour = Colour::find($colourId);
        $region = Region::find($regionId);
        $country = Country::find($countryId);
        $producer = Producer::find($producerId);
        $variety = Variety::find($varietyId);
        $appellation = Appellation::find($appellationId);
        
        // Requête de base
        $winesQb = Wine::defaultQuery();
        
        $appliedFilters = array();
        
        // Recherche
        if($search != ""){
            $searchArray = explode(" ", $search);
            
            foreach($searchArray as $searchItem){
                if(trim($searchItem) != ""){
                    $winesQb->where("articles.name", "like", "%" . addcslashes(trim($searchItem), "%_") . "%");
                }
            }
            
            $appliedFilters[] = array(
                "icon" => "fas fa-search",
                "label" => $search,
                "function" => "resetInput('search')",
            );
        }
        
        // Couleur
        if(!is_null($colour)){
            $winesQb->where("colour_id", $colour->id);
            
            $appliedFilters[] = array(
                "icon" => "fas fa-wine-glass-alt",
                "label" => $colour->name,
                "function" => "resetColour()",
            );
        }
        
        // Région
        if(!is_null($region)){
            // La région n'est pas dans le pays sélectionné
            if($region->country != $country){
                $region = null;
                $regionId = null;
            }
            else {
                $winesQb->where("wines.region_id", $region->id);
                
                $appliedFilters[] = array(
                    "icon" => "fas fa-map-marked-alt",
                    "label" => $region->name,
                    "function" => "resetSelect('region')",
                );
            }
        }
        
        // Pays
        if(!is_null($country)){
            $winesQb->join("regions", "regions.id", "=", "wines.region_id")
                ->where("regions.country_id", $country->id);
            
            $appliedFilters[] = array(
                "icon" => "fas fa-globe-americas",
                "label" => $country->name,
                "function" => "resetSelect('country')",
            );
        }
        
        // Producteur
        if(!is_null($producer)){
            $winesQb->where("articles.producer_id", $producer->id);
            
            $appliedFilters[] = array(
                "icon" => "far fa-user",
                "label" => $producer->name,
                "function" => "resetSelect('producer')",
            );
        }
        
        // Cépage
        if(!is_null($variety)){
            $winesQb->join("wines_varieties", "wines_varieties.wine_id", "=", "wines.id")
                ->join("varieties", "varieties.id", "=", "wines_varieties.variety_id")
                ->where("varieties.id", $variety->id);
            
            $appliedFilters[] = array(
                "iconify" => "vs-grapes",
                "label" => $variety->name,
                "function" => "resetSelect('variety')",
            );
        }
        
//        // Caractéristique
//        // Tableau des caractéristiques
//        $characteristicsIds = explode("-", $characteristics);
//        foreach($characteristicsIds as $characteristicId){
//            $characteristic = Characteristic::find($characteristicId);
//            
//            if(!is_null($characteristic)){
//                $winesQb->join("wines_characteristics as wines_characteristics" . $characteristicId, "wines_characteristics" . $characteristicId . ".wine_id", "=", "wines.id")
//                    ->where("wines_characteristics" . $characteristicId . ".characteristic_id", $characteristicId);
//                
//                $appliedFilters[] = array(
//                    "icon" => "fas fa-tag",
//                    "label" => $characteristic->name,
//                    "function" => "resetCheckbox('characteristic" . $characteristic->id . "')",
//                );
//            }
//        }
        
        // Millésime
        if($year != 0){
            $winesQb->where("wines.year", $year);
            
            $appliedFilters[] = array(
                "icon" => "far fa-calendar-alt",
                "label" => $year,
                "function" => "resetSelect('year')",
            );
        }
        
        // Litrage
        if($litre != 0){
            $winesQb->where("articles.litres", $litre);
            
            $appliedFilters[] = array(
                "icon" => "fas fa-wine-bottle",
                "label" => $litre . "cl",
                "function" => "resetSelect('litres')",
            );
        }
        
        // Prix
        $winesQb->where("prices.price", ">=", $minPrice)
            ->where("prices.price", "<=", $maxPrice);
        
        if($minPrice != $minPriceFilter || $maxPrice != $maxPriceFilter){
            $labelPrice = "CHF " . $minPrice . ".00";
            
            if($minPrice != $maxPrice){
                $labelPrice .= " - " . $maxPrice . ".00";
            }
            
            $appliedFilters[] = array(
                "icon" => "fas fa-dollar-sign",
                "label" => $labelPrice,
                "function" => "resetPrice()",
            );
        }
        
        // Coup de coeur
        if($favourite == true){
            $winesQb->where("articles.favourite", true);
            
            $appliedFilters[] = array(
                "icon" => "far fa-heart",
                "label" => "Coups de cœur",
                "function" => "resetCheckbox('favourite')",
            );
        }
        
        // Uniquement les vins disponibles
        if($available == true){
            $winesQb->where(function($query){
                $query->where("articles.limited_quantity", ">", 0)
                    ->orWhere(function($query2){
                        $query2->whereNull("articles.limited_quantity")
                            ->where(function($query3){
                                $query3->where("articles.soon_available", true)
                                    ->orWhere("articles.quantity", ">", 0);
                            });
                    });
            });
            
            $appliedFilters[] = array(
                "icon" => "fas fa-check",
                "label" => "Disponibles",
                "function" => "resetCheckbox('available')",
            );
        }
        
        // En action
//        if($sale == true){
//            $appliedFilters[] = array(
//                "icon" => "fas fa-percent",
//                "label" => "Promotions",
//                "function" => "resetCheckbox('sale')",
//            );
//        }
        
        // Vin primé
        if($distinction == true){
            $winesQb->join("wines_distinctions", "wines_distinctions.wine_id", "=", "wines.id");
            
            $appliedFilters[] = array(
                "icon" => "fas fa-award",
                "label" => "Vins primés",
                "function" => "resetCheckbox('distinction')",
            );
        }
        
        // Rareté
        if($rare == true){
            $winesQb->whereNotNull("articles.limited_quantity");
            
            $appliedFilters[] = array(
                "icon" => "far fa-gem",
                "label" => "Raretés",
                "function" => "resetCheckbox('rare')",
            );
        }
        
        // Appellation
        if(!is_null($appellation)){
            $winesQb->where("wines.appellation_id", $appellation->id);
            
            $appliedFilters[] = array(
                "icon" => "fas fa-certificate",
                "label" => $appellation->name,
                "function" => "resetSelect('appellation')",
            );
        }
        
        $wines = $winesQb->paginate(12);
        
        return view('shop.wine.index', [
            "wines" => $wines,
            
            "appliedFilters" => $appliedFilters,
                
            // Valeurs des filtres sélectionnés
            "values" => array(
                "colour" => $colourId,
                "country" => $countryId,
                "region" => $regionId,
                "producer" => $producerId,
                "variety" => $varietyId,
//                "characteristics" => $characteristicsIds,
                "year" => $year,
                "litre" => $litre,
                "favourite" => $favourite,
                "available" => $available,
//                "sale" => $sale,
                "distinction" => $distinction,
                "rare" => $rare,
                "appellation" => $appellationId,
                "minPrice" => $minPrice,
                "maxPrice" => $maxPrice,
                "search" => $search,
            ),
            
            // Filtres
            "filters" => array(
                "colours" => $coloursFilter,
                "countries" => $countriesFilter,
                "regions" => $regionsFilter,
                "producers" => $producersFilter,
                "appellations" => $appellationsFilter,
                "varieties" => $varietiesFilter,
//                "characteristics" => $characteristicsFilter,
                "years" => $yearsFilter,
                "litres" => $litresFilter,
                "minPrice" => $minPriceFilter,
                "maxPrice" => $maxPriceFilter,
            ),
        ]);
    }
    
    
    /**
     * Détail d'un vin
     */
    public function detail(Wine $wine) {
        $article = $wine->article;
        
        $producer = $article->producer;
        $producerPhotos = array();

        // Producteur existant
        if(!is_null($producer)){
            // Autres vins du producteur
            $producerWines = Wine::take(4)
                ->select("wines.*", "articles.*", "prices.price AS shopPrice")
                ->leftJoin('articles', 'articles.id', '=', 'wines.id')
                ->join('prices', 'prices.article_id', '=', 'articles.id')
                ->join('clients_types', 'clients_types.id', '=', 'prices.client_type_id')
                ->where([
                    ['articles.archived', '=', false],
                    ['articles.shop', '=', true],
                    ['clients_types.name', '=', "Shop"],
                    ['articles.id', '!=', $article->id],
                    ['articles.producer_id', '=', $producer->id],
                ])
                ->get();

            // Nombre de vins du producteur
            $nbProducerWines = Wine::select("wines.*")
                ->leftJoin('articles', 'articles.id', '=', 'wines.id')
                ->join('prices', 'prices.article_id', '=', 'articles.id')
                ->join('clients_types', 'clients_types.id', '=', 'prices.client_type_id')
                ->where([
                    ['articles.archived', '=', false],
                    ['articles.shop', '=', true],
                    ['clients_types.name', '=', "Shop"],
                    ['articles.id', '!=', $article->id],
                    ['articles.producer_id', '=', $producer->id],
                ])
                ->count();

            // Photos du producteur
            foreach($producer->photos as $producerPhoto) {
                $filename = config('app.photo_url') . "producer/" . $producer->id . "/" . $producerPhoto->name;

                $producerPhotos = $this::getPhotoInfo($filename, $producerPhotos);
            }
        }
        else {
            $producerWines = array();
            $nbProducerWines = 0;
        }

        return view('shop.wine.detail', [
            "wine" => $wine,
            "producerWines" => $producerWines,
            "producerPhotos" => $producerPhotos,
            "nbProducerWines" => $nbProducerWines,
        ]);
    }
}
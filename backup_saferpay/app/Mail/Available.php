<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Available extends Mailable {
    use Queueable,
        SerializesModels;

    // Email et vin
    public $email;
    public $wine;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $wine) {
        $this->email = $email;
        $this->wine = $wine;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject("Shop : tenir informé")
            ->replyTo($this->email)
            ->view('mail.available');
    }
}
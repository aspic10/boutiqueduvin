<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Appellation
 * 
 * @property int $id
 * @property string $name
 * 
 * @property Collection|Wine[] $wines
 *
 * @package App\Models
 */
class Appellation extends Model
{
	protected $table = 'appellations';
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function wines()
	{
		return $this->hasMany(Wine::class);
	}
}

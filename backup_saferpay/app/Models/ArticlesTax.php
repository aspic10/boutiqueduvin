<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticlesTax
 * 
 * @property int $article_id
 * @property int $tax_id
 * @property int $quantity
 * 
 * @property Article $article
 * @property Tax $tax
 *
 * @package App\Models
 */
class ArticlesTax extends Model
{
	protected $table = 'articles_taxes';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'article_id' => 'int',
		'tax_id' => 'int',
		'quantity' => 'int'
	];

	protected $fillable = [
		'quantity'
	];

	public function article()
	{
		return $this->belongsTo(Article::class);
	}

	public function tax()
	{
		return $this->belongsTo(Tax::class);
	}
}

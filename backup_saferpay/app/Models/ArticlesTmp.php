<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticlesTmp
 * 
 * @property int $id
 * @property string $code
 * @property string $name
 * @property float $price
 * @property bool $no_discount
 * @property string $format
 * @property float $quantity
 * @property bool $price_list
 * @property string $description
 * @property float $weight
 * @property int $number
 * @property float $vat
 * @property string $container
 * @property string $packagings
 * @property string $prices
 * @property float $buying_price
 * @property string $remarks
 * @property string $brand
 * @property string $supplier
 * @property string $reference
 *
 * @package App\Models
 */
class ArticlesTmp extends Model
{
	protected $table = 'articles_tmp';
	public $timestamps = false;

	protected $casts = [
		'price' => 'float',
		'no_discount' => 'bool',
		'quantity' => 'float',
		'price_list' => 'bool',
		'weight' => 'float',
		'number' => 'int',
		'vat' => 'float',
		'buying_price' => 'float'
	];

	protected $fillable = [
		'code',
		'name',
		'price',
		'no_discount',
		'format',
		'quantity',
		'price_list',
		'description',
		'weight',
		'number',
		'vat',
		'container',
		'packagings',
		'prices',
		'buying_price',
		'remarks',
		'brand',
		'supplier',
		'reference'
	];
}

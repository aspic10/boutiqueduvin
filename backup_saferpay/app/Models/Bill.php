<?php
/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bill
 * 
 * @property int $id
 * @property int $client_order_id
 * @property int $bill_status_id
 * @property int $client_id
 * @property int $payment_term_id
 * @property Carbon $date
 * @property float $discount
 * @property string $address
 * @property string $remark
 * @property string $internal_remark
 * @property float $total
 * @property float $total_taxes
 * @property Carbon $due_date
 * @property float $subtotal
 * @property float $total_no_discount
 * @property int $year
 * @property string $balance_label
 * @property float $balance_amount
 * @property float $total_buying
 * @property int $refund_id
 * 
 * @property PaymentsTerm $payments_term
 * @property Refund $refund
 * @property Client $client
 * @property BillsStatus $bills_status
 * @property ClientsOrder $clients_order
 * @property Collection|Article[] $articles
 * @property Collection|Fee[] $fees
 * @property Collection|Packaging[] $packagings
 * @property Collection|Reminder[] $reminders
 * @property Collection|GiftCard[] $gift_cards
 * @property Collection|GiftCard[] $gift_cards_bought
 * @property Collection|Tax[] $taxes
 * @property Collection|User[] $users
 * @property Collection|BillsVat[] $bills_vats
 * @property Collection|Payment[] $payments
 *
 * @package App\Models
 */
class Bill extends Model {

    protected $table = 'bills';
    public $timestamps = false;
    protected $casts = [
        'client_order_id' => 'int',
        'bill_status_id' => 'int',
        'client_id' => 'int',
        'payment_term_id' => 'int',
        'discount' => 'float',
        'total' => 'float',
        'total_taxes' => 'float',
        'subtotal' => 'float',
        'total_no_discount' => 'float',
        'year' => 'int',
        'balance_amount' => 'float',
        'total_buying' => 'float',
        'refund_id' => 'int'
    ];
    protected $dates = [
        'date',
        'due_date'
    ];
    protected $fillable = [
        'client_order_id',
        'bill_status_id',
        'client_id',
        'payment_term_id',
        'date',
        'discount',
        'address',
        'remark',
        'internal_remark',
        'total',
        'total_taxes',
        'due_date',
        'subtotal',
        'total_no_discount',
        'year',
        'balance_label',
        'balance_amount',
        'total_buying',
        'refund_id'
    ];

    /**************************************************************************/
    /* FONCTIONS                                                              */
    /**************************************************************************/

    /**
     * Total de la facture (articles + taxes)
     */
    public function getBillTotal() {
        $totalPrice = 0;

        // Article + TVA arrondi à 5ct
        foreach($this->articles as $article){
            $billArticle = $article->pivot;
            $quantity = $billArticle->quantity;
            $price = round(($billArticle->price + $billArticle->price * $billArticle->vat / 100) / 0.05) * 0.05;
            $articlePrice = $price * $quantity;
            $totalPrice += $articlePrice;
        }
        
        // Cartes cadeaux achetées
        foreach($this->gift_cards_bought as $giftCard){
            $totalPrice += $giftCard->initial_amount;
        }
        
        // Frais de port
        $totalPrice += round($this->total_taxes / 0.05) * 0.05;

        return $totalPrice;
    }
    
    
    /**************************************************************************/
    /* RELATIONS                                                              */
    /**************************************************************************/

    public function payments_term() {
        return $this->belongsTo(PaymentsTerm::class, 'payment_term_id');
    }

    public function refund() {
        return $this->belongsTo(Refund::class);
    }

    public function client() {
        return $this->belongsTo(Client::class);
    }

    public function bills_status() {
        return $this->belongsTo(BillsStatus::class, 'bill_status_id');
    }

    public function clients_order() {
        return $this->belongsTo(ClientsOrder::class, 'client_order_id');
    }

    public function articles() {
        return $this->belongsToMany(Article::class, 'bills_articles_' . $this->year)
                ->withPivot('id', 'code', 'name', 'format', 'quantity', 'price', 'vat', 'vat_code', 'sale', 'no_discount', 'remark', 'boxes', 'last', 'buying_price')
                ->orderBy("code", "ASC");
    }

    public function fees() {
        return $this->belongsToMany(Fee::class, 'bills_fees')
                ->withPivot('price');
    }

    public function packagings() {
        return $this->belongsToMany(Packaging::class, 'bills_packagings')
                ->withPivot('price', 'quantity', 'quantity_returned');
    }

    public function reminders() {
        return $this->belongsToMany(Reminder::class, 'bills_reminders')
                ->withPivot('amount');
    }

    public function gift_cards() {
        return $this->belongsToMany(GiftCard::class, 'gift_cards_bills')
                ->withPivot('amount');
    }
    
    public function taxes() {
        return $this->belongsToMany(Tax::class, 'bills_taxes')
                ->withPivot('price', 'quantity', 'vat');
    }

    public function users() {
        return $this->belongsToMany(User::class, 'bills_users');
    }

    public function bills_vats() {
        return $this->hasMany(BillsVat::class);
    }

    public function payments() {
        return $this->hasMany(Payment::class);
    }
    
    public function gift_cards_bought() {
        return $this->hasMany(GiftCard::class)->orderBy('id', 'DESC');
    }
}
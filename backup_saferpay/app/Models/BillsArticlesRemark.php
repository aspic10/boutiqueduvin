<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BillsArticlesRemark
 * 
 * @property int $id
 * @property string $name
 *
 * @package App\Models
 */
class BillsArticlesRemark extends Model
{
	protected $table = 'bills_articles_remarks';
	public $timestamps = false;

	protected $fillable = [
		'name'
	];
}

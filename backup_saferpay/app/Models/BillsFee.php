<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BillsFee
 * 
 * @property int $bill_id
 * @property int $fee_id
 * @property float $price
 * 
 * @property Bill $bill
 * @property Fee $fee
 *
 * @package App\Models
 */
class BillsFee extends Model
{
	protected $table = 'bills_fees';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'bill_id' => 'int',
		'fee_id' => 'int',
		'price' => 'float'
	];

	protected $fillable = [
		'price'
	];

	public function bill()
	{
		return $this->belongsTo(Bill::class);
	}

	public function fee()
	{
		return $this->belongsTo(Fee::class);
	}
}

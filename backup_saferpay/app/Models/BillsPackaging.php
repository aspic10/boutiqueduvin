<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BillsPackaging
 * 
 * @property int $bill_id
 * @property int $packaging_id
 * @property float $price
 * @property float $quantity
 * @property float $quantity_returned
 * 
 * @property Bill $bill
 * @property Packaging $packaging
 *
 * @package App\Models
 */
class BillsPackaging extends Model
{
	protected $table = 'bills_packagings';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'bill_id' => 'int',
		'packaging_id' => 'int',
		'price' => 'float',
		'quantity' => 'float',
		'quantity_returned' => 'float'
	];

	protected $fillable = [
		'price',
		'quantity',
		'quantity_returned'
	];

	public function bill()
	{
		return $this->belongsTo(Bill::class);
	}

	public function packaging()
	{
		return $this->belongsTo(Packaging::class);
	}
}

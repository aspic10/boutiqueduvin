<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BillsReminder
 * 
 * @property int $bill_id
 * @property int $reminder_id
 * @property float $amount
 * 
 * @property Bill $bill
 * @property Reminder $reminder
 *
 * @package App\Models
 */
class BillsReminder extends Model
{
	protected $table = 'bills_reminders';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'bill_id' => 'int',
		'reminder_id' => 'int',
		'amount' => 'float'
	];

	protected $fillable = [
		'amount'
	];

	public function bill()
	{
		return $this->belongsTo(Bill::class);
	}

	public function reminder()
	{
		return $this->belongsTo(Reminder::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BillsStatus
 * 
 * @property int $id
 * @property string $name
 * @property int $open
 * 
 * @property Collection|Bill[] $bills
 * @property Collection|SuppliersBill[] $suppliers_bills
 *
 * @package App\Models
 */
class BillsStatus extends Model
{
	protected $table = 'bills_status';
	public $timestamps = false;

	protected $casts = [
		'open' => 'int'
	];

	protected $fillable = [
		'name',
		'open'
	];

	public function bills()
	{
		return $this->hasMany(Bill::class, 'bill_status_id');
	}

	public function suppliers_bills()
	{
		return $this->hasMany(SuppliersBill::class, 'bill_status_id');
	}
}

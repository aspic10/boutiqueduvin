<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BillsUser
 * 
 * @property int $bill_id
 * @property int $user_id
 * 
 * @property Bill $bill
 * @property User $user
 *
 * @package App\Models
 */
class BillsUser extends Model
{
	protected $table = 'bills_users';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'bill_id' => 'int',
		'user_id' => 'int'
	];

	public function bill()
	{
		return $this->belongsTo(Bill::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}

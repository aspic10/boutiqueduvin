<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BillsVat
 * 
 * @property int $id
 * @property int $bill_id
 * @property float $vat
 * @property float $amount
 * 
 * @property Bill $bill
 *
 * @package App\Models
 */
class BillsVat extends Model
{
	protected $table = 'bills_vats';
	public $timestamps = false;

	protected $casts = [
		'bill_id' => 'int',
		'vat' => 'float',
		'amount' => 'float'
	];

	protected $fillable = [
		'bill_id',
		'vat',
		'amount'
	];

	public function bill()
	{
		return $this->belongsTo(Bill::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * 
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property int $rank
 * 
 * @property Category $category
 * @property Collection|Article[] $articles
 * @property Collection|Category[] $categories
 *
 * @package App\Models
 */
class Category extends Model
{
	protected $table = 'categories';
	public $timestamps = false;

	protected $casts = [
		'parent_id' => 'int',
		'rank' => 'int'
	];

	protected $fillable = [
		'parent_id',
		'name',
		'rank'
	];

	public function category()
	{
		return $this->belongsTo(Category::class, 'parent_id');
	}

	public function articles()
	{
		return $this->hasMany(Article::class);
	}

	public function categories()
	{
		return $this->hasMany(Category::class, 'parent_id');
	}
}

<?php
/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Notifications\Notifiable;

/**
 * Class Client
 * 
 * @property int $id
 * @property int $refund_type_id
 * @property string $advertisement
 * @property int $client_type_id
 * @property int $title_id
 * @property string $contact
 * @property string $monthly_bill_address
 * @property int $monthly_bill_days
 * @property bool $code_order
 * @property bool $visible
 * @property string $balance_label
 * @property float $balance_amount
 * @property string $company_name
 * @property string $order_remark
 * @property string $password
 * @property datetime $email_verified_at
 * 
 * @property ClientsType $clients_type
 * @property RefundsType $refunds_type
 * @property Title $title
 * @property Company $company
 * @property Collection|Bill[] $bills
 * @property Collection|GiftCard[] $gift_cards
 * @property Collection|Article[] $articles
 * @property Collection|Article[] $articles_prices
 * @property Collection|Day[] $days
 * @property Collection|ClientsOrder[] $clients_orders
 * @property Collection|Packaging[] $packagings
 * @property Collection|User[] $users
 * @property Collection|Refund[] $refunds
 * @property Collection|Supplier[] $suppliers
 *
 * @package App\Models
 */
class Client extends Company implements Authenticatable, MustVerifyEmail, CanResetPassword {

    use Notifiable;

    protected $table = 'clients';
    public $incrementing = false;
    public $timestamps = false;
    protected $casts = [
        'id' => 'int',
        'refund_type_id' => 'int',
        'client_type_id' => 'int',
        'title_id' => 'int',
        'monthly_bill_days' => 'int',
        'code_order' => 'bool',
        'visible' => 'bool',
        'balance_amount' => 'float'
    ];
    protected $hidden = [
        'password'
    ];
    protected $fillable = [
        'refund_type_id',
        'advertisement',
        'client_type_id',
        'title_id',
        'contact',
        'monthly_bill_address',
        'monthly_bill_days',
        'code_order',
        'visible',
        'balance_label',
        'balance_amount',
        'company_name',
        'order_remark',
        'password',
    ];

    /**
     * Default values for attributes
     */
    protected $attributes = [
        'client_type_id' => 5,
        'visible' => 0,
    ];

    /*     * *********************************************************************** */
    /* CONSTRUCTOR                                                            */
    /*     * *********************************************************************** */

    public function __construct() {
        // Initialisation du parent
        parent::__construct();
    }
    /*     * *********************************************************************** */
    /* FONCTIONS                                                              */
    /*     * *********************************************************************** */

    public function __toString() {
        return $this->name;
    }

    /**
     * Récupérer l'adresse complète
     */
    public function getFullAddress($br = true) {
        $address = "";

        if(!is_null($this->company_name)) {
            $address .= $this->company_name;

            // <br/>
            if($br == true) {
                $address .= "<br/>";
            }
            // \n
            else {
                $address .= "\n";
            }
        }

        // Nom du client
        $address .= $this->name;

        // <br/>
        if($br == true) {
            $address .= "<br/>";
        }
        // \n
        else {
            $address .= "\n";
        }

        // Adresse
        if(!is_null($this->address) && $this->address != "") {
            // <br/>
            if($br == true) {
                $address .= nl2br($this->address) . "<br/>";
            }
            // \n
            else {
                $address .= $this->address . "\n";
            }
        }

        // NPA et ville
        $address .= $this->city->__toString();

        return $address;
    }
    
    /**
     * Bons cadeaux du client
     */
    public function getGiftCards(){
        $giftCards = $this->gift_cards;
        
        $validGiftCards = array();
        $usedGiftCards = array();
        $availableAmount = 0;
        
        foreach($giftCards as $giftCard){
            $amount = round($giftCard->amount, 2);
            
            if($amount >= 0.05){
                $availableAmount += $amount;
                $validGiftCards[] = $giftCard;
            }
            else {
                $usedGiftCards[] = $giftCard;
            }
        }
        
        return [
            "validGiftCards" => $validGiftCards,
            "usedGiftCards" => $usedGiftCards,
            "availableAmount" => number_format($availableAmount, 2, ".", "'")
        ];
    }
    
    
    /*     * *********************************************************************** */
    /* GETTERS ET SETTERS DES PROPRIÉTÉS DU PARENT                            */
    /*     * *********************************************************************** */

    public function getCityAttribute() {
        return $this->company->city;
    }

    public function getCodeAttribute() {
        return $this->company->code;
    }

    public function getNameAttribute() {
        return $this->company->name;
    }

    public function getAddressAttribute() {
        return $this->company->address;
    }

    public function getPhoneAttribute() {
        return $this->company->phone;
    }

    public function getEmailAttribute() {
        return $this->company->email;
    }

    public function getRemarkAttribute() {
        return $this->company->remark;
    }

    public function getArchivedAttribute() {
        return $this->company->archived;
    }

    // Inutiles : Fax / Type / Website / PaymentTerm / Discount


    /*     * *********************************************************************** */
    /* RELATIONS                                                              */
    /*     * *********************************************************************** */
    public function clients_type() {
        return $this->belongsTo(ClientsType::class, 'client_type_id');
    }

    public function refunds_type() {
        return $this->belongsTo(RefundsType::class, 'refund_type_id');
    }

    public function title() {
        return $this->belongsTo(Title::class);
    }

    public function company() {
        return $this->belongsTo(Company::class, 'id');
    }

    public function bills() {
        return $this->hasMany(Bill::class)->orderBy('id', 'DESC');
    }
    
    public function gift_cards() {
        return $this->hasMany(GiftCard::class)->orderBy('id', 'ASC');
    }

    public function articles() {
        return $this->belongsToMany(Article::class, 'clients_articles')
                ->withPivot('rank', 'quantity');
    }
    
    public function articles_prices() {
        return $this->belongsToMany(Article::class, 'clients_articles_prices')
                ->withPivot('id', 'from', 'to', 'price', 'fixed');
    }

    public function days() {
        return $this->belongsToMany(Day::class, 'clients_days')
                ->withPivot('hours');
    }

    public function clients_orders() {
        return $this->hasMany(ClientsOrder::class);
    }

    public function packagings() {
        return $this->belongsToMany(Packaging::class, 'clients_packagings')
                ->withPivot('quantity');
    }

    public function users() {
        return $this->belongsToMany(User::class, 'clients_users');
    }

    public function refunds() {
        return $this->hasMany(Refund::class);
    }

    public function suppliers() {
        return $this->belongsToMany(Supplier::class, 'suppliers_clients');
    }
}

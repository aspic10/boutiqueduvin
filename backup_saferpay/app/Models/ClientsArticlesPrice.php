<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientsArticlesPrice
 * 
 * @property int $id
 * @property int $client_id
 * @property int $article_id
 * @property Carbon $from
 * @property Carbon $to
 * @property float $price
 * @property bool $fixed
 * 
 * @property Client $client
 * @property Article $article
 *
 * @package App\Models
 */
class ClientsArticlesPrice extends Model
{
	protected $table = 'clients_articles_prices';
	public $timestamps = false;

	protected $casts = [
		'client_id' => 'int',
		'article_id' => 'int',
		'price' => 'float',
		'fixed' => 'bool'
	];

	protected $dates = [
		'from',
		'to'
	];

	protected $fillable = [
		'client_id',
		'article_id',
		'from',
		'to',
		'price',
		'fixed'
	];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function article()
	{
		return $this->belongsTo(Article::class);
	}
}

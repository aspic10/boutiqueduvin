<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientsPackaging
 * 
 * @property int $client_id
 * @property int $packaging_id
 * @property float $quantity
 * 
 * @property Client $client
 * @property Packaging $packaging
 *
 * @package App\Models
 */
class ClientsPackaging extends Model
{
	protected $table = 'clients_packagings';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'client_id' => 'int',
		'packaging_id' => 'int',
		'quantity' => 'float'
	];

	protected $fillable = [
		'quantity'
	];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function packaging()
	{
		return $this->belongsTo(Packaging::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientsType
 * 
 * @property int $id
 * @property string $name
 * @property bool $blocked
 * @property bool $vat_included
 * 
 * @property Collection|Client[] $clients
 * @property Collection|Price[] $prices
 *
 * @package App\Models
 */
class ClientsType extends Model
{
	protected $table = 'clients_types';
	public $timestamps = false;

	protected $casts = [
		'blocked' => 'bool',
		'vat_included' => 'bool'
	];

	protected $fillable = [
		'name',
		'blocked',
		'vat_included'
	];

	public function clients()
	{
		return $this->hasMany(Client::class, 'client_type_id');
	}

	public function prices()
	{
		return $this->hasMany(Price::class, 'client_type_id');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Foundation\Auth\User as UserAuthenticate;

/**
 * Class Company
 * 
 * @property int $id
 * @property int $city_id
 * @property string $code
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $fax
 * @property string $email
 * @property string $remark
 * @property bool $archived
 * @property string $type
 * @property string $website
 * @property int $payment_term_id
 * @property float $discount
 * 
 * @property PaymentsTerm $payments_term
 * @property City $city
 * @property Client $client
 * @property Supplier $supplier
 *
 * @package App\Models
 */
class Company extends UserAuthenticate
{
	protected $table = 'companies';
	public $timestamps = false;

	protected $casts = [
		'city_id' => 'int',
		'archived' => 'bool',
		'payment_term_id' => 'int',
		'discount' => 'float'
	];

	protected $fillable = [
		'city_id',
		'code',
		'name',
		'address',
		'phone',
		'fax',
		'email',
		'remark',
		'archived',
		'type',
		'website',
		'payment_term_id',
		'discount'
	];
    
    
    /**
     * Default values for attributes
     */
    protected $attributes = [
        'payment_term_id' => 1,
        'type' => 'client',
    ];
    
    
    /**************************************************************************/
    /* RELATIONS                                                              */
    /**************************************************************************/
    public function client() {
        return $this->hasOne(Client::class, 'id');
    }

    public function supplier() {
        return $this->hasOne(Supplier::class, 'id');
    }

    public function payments_term() {
        return $this->belongsTo(PaymentsTerm::class, 'payment_term_id');
    }

    public function city() {
        return $this->belongsTo(City::class);
    }
}
<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Container
 * 
 * @property int $id
 * @property string $name
 * @property string $name_plural
 * @property string $abbreviation
 * @property string $abbreviation_plural
 * 
 * @property Collection|Article[] $articles
 *
 * @package App\Models
 */
class Container extends Model
{
	protected $table = 'containers';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'name_plural',
		'abbreviation',
		'abbreviation_plural'
	];

	public function articles()
	{
		return $this->hasMany(Article::class);
	}
}

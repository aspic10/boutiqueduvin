<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Drink
 * 
 * @property int $id
 * 
 * @property Article $article
 *
 * @package App\Models
 */
class Drink extends Model
{
	protected $table = 'drinks';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];
    
    public function article()
    {
        return $this->morphOne('App\Models\Article', 'articleable');
    }
        /**************************************************************************/
        /* CONSTRUCTOR                                                            */
        /**************************************************************************/
//        public function __construct() {
//            // Initialisation du parent
//            parent::__construct();
//        }
//        
//        
//	public function article()
//	{
//		return $this->belongsTo(Article::class, 'id');
//	}
}
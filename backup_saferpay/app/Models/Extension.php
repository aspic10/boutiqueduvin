<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Extension
 * 
 * @property int $id
 * @property string $name
 * @property string $mime_type
 * 
 * @property Collection|Photo[] $photos
 *
 * @package App\Models
 */
class Extension extends Model
{
	protected $table = 'extensions';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'mime_type'
	];

	public function photos()
	{
		return $this->hasMany(Photo::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Fee
 * 
 * @property int $id
 * @property string $name
 * @property float $price
 * @property bool $archived
 * 
 * @property Collection|Bill[] $bills
 *
 * @package App\Models
 */
class Fee extends Model
{
	protected $table = 'fees';
	public $timestamps = false;

	protected $casts = [
		'price' => 'float',
		'archived' => 'bool'
	];

	protected $fillable = [
		'name',
		'price',
		'archived'
	];

	public function bills()
	{
		return $this->belongsToMany(Bill::class, 'bills_fees')
					->withPivot('price');
	}
}

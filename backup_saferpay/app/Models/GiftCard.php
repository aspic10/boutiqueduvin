<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GiftCard
 * 
 * @property int $id
 * @property int $client_id
 * @property string $number
 * @property string $pin
 * @property Carbon $date
 * @property float $amount
 * @property float $initial_amount
 * @property int $bill_id
 * 
 * @property Client $client
 * @property Collection|Bill[] $bills
 *
 * @package App\Models
 */
class GiftCard extends Model
{
	protected $table = 'gift_cards';
	public $timestamps = false;

	protected $casts = [
		'client_id' => 'int',
		'amount' => 'float',
		'initial_amount' => 'float',
		'bill_id' => 'int',
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'client_id',
		'number',
		'pin',
		'date',
		'amount',
		'initial_amount',
		'bill_id',
	];

	public function client()
	{
		return $this->belongsTo(Client::class, 'client_id');
	}

	public function bills()
	{
		return $this->belongsToMany(Bill::class, 'gift_cards_bills')
					->withPivot('amount');
	}
    
    public function bill()
	{
		return $this->belongsTo(Bill::class);
	}
}

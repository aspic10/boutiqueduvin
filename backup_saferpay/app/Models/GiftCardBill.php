<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GiftCardBill
 * 
 * @property int $bill_id
 * @property int $gift_card_id
 * @property float $amount
 * 
 * @property Bill $bill
 * @property GiftCard $gift_card
 *
 * @package App\Models
 */
class GiftCardBill extends Model
{
	protected $table = 'gift_cards_bills';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'bill_id' => 'int',
		'gift_card_id' => 'int',
		'amount' => 'float'
	];

	protected $fillable = [
		'amount'
	];

	public function bill()
	{
		return $this->belongsTo(Bill::class);
	}

	public function gift_card()
	{
		return $this->belongsTo(GiftCard::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InventoriesArticle
 * 
 * @property int $inventory_id
 * @property int $article_id
 * @property float $quantity
 * @property float $previous_quantity
 * 
 * @property Article $article
 * @property Inventory $inventory
 *
 * @package App\Models
 */
class InventoriesArticle extends Model
{
	protected $table = 'inventories_articles';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'inventory_id' => 'int',
		'article_id' => 'int',
		'quantity' => 'float',
		'previous_quantity' => 'float'
	];

	protected $fillable = [
		'quantity',
		'previous_quantity'
	];

	public function article()
	{
		return $this->belongsTo(Article::class);
	}

	public function inventory()
	{
		return $this->belongsTo(Inventory::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Margin
 * 
 * @property int $id
 * @property string $colour
 * @property int $from
 * @property int $to
 *
 * @package App\Models
 */
class Margin extends Model
{
	protected $table = 'margins';
	public $timestamps = false;

	protected $casts = [
		'from' => 'int',
		'to' => 'int'
	];

	protected $fillable = [
		'colour',
		'from',
		'to'
	];
}

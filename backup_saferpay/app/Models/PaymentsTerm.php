<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentsTerm
 * 
 * @property int $id
 * @property string $name
 * @property int $days
 * @property bool $blocked
 * @property bool $archived
 * 
 * @property Collection|Bill[] $bills
 * @property Collection|Company[] $companies
 * @property Collection|SuppliersBill[] $suppliers_bills
 *
 * @package App\Models
 */
class PaymentsTerm extends Model
{
	protected $table = 'payments_terms';
	public $timestamps = false;

	protected $casts = [
		'days' => 'int',
		'blocked' => 'bool',
		'archived' => 'bool'
	];

	protected $fillable = [
		'name',
		'days',
		'blocked',
		'archived'
	];

	public function bills()
	{
		return $this->hasMany(Bill::class, 'payment_term_id');
	}

	public function companies()
	{
		return $this->hasMany(Company::class, 'payment_term_id');
	}

	public function suppliers_bills()
	{
		return $this->hasMany(SuppliersBill::class, 'payment_term_id');
	}
}

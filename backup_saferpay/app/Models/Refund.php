<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Refund
 * 
 * @property int $id
 * @property int $client_id
 * @property float $discount
 * @property float $amount
 * @property Carbon $date
 * @property int $year
 * 
 * @property Client $client
 * @property Bill $bill
 *
 * @package App\Models
 */
class Refund extends Model
{
	protected $table = 'refunds';
	public $timestamps = false;

	protected $casts = [
		'client_id' => 'int',
		'discount' => 'float',
		'amount' => 'float',
		'year' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'client_id',
		'discount',
		'amount',
		'date',
		'year'
	];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function bill()
	{
		return $this->hasOne(Bill::class);
	}
}

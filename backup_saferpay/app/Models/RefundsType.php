<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RefundsType
 * 
 * @property int $id
 * @property string $name
 * 
 * @property Collection|Client[] $clients
 * @property Collection|RefundsLevel[] $refunds_levels
 *
 * @package App\Models
 */
class RefundsType extends Model
{
	protected $table = 'refunds_types';
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function clients()
	{
		return $this->hasMany(Client::class, 'refund_type_id');
	}

	public function refunds_levels()
	{
		return $this->hasMany(RefundsLevel::class, 'refund_type_id');
	}
}

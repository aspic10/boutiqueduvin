<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Remark
 * 
 * @property int $id
 * @property string $name
 * @property string $abbreviation
 * 
 * @property Collection|Article[] $articles
 *
 * @package App\Models
 */
class Remark extends Model
{
	protected $table = 'remarks';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'abbreviation'
	];

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'articles_remarks');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Reminder
 * 
 * @property int $id
 * @property int $reminder_type_id
 * @property float $fee
 * @property Carbon $date
 * @property float $interest
 * 
 * @property RemindersType $reminders_type
 * @property Collection|Bill[] $bills
 *
 * @package App\Models
 */
class Reminder extends Model
{
	protected $table = 'reminders';
	public $timestamps = false;

	protected $casts = [
		'reminder_type_id' => 'int',
		'fee' => 'float',
		'interest' => 'float'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'reminder_type_id',
		'fee',
		'date',
		'interest'
	];

	public function reminders_type()
	{
		return $this->belongsTo(RemindersType::class, 'reminder_type_id');
	}

	public function bills()
	{
		return $this->belongsToMany(Bill::class, 'bills_reminders')
					->withPivot('amount');
	}
}

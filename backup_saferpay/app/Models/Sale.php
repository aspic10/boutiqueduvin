<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Sale
 * 
 * @property int $id
 * @property Carbon $from
 * @property Carbon $to
 * @property string $name
 * @property int $sale_type_id
 * @property string $colour
 * 
 * @property SalesType $sales_type
 * @property Collection|Article[] $articles
 *
 * @package App\Models
 */
class Sale extends Model
{
	protected $table = 'sales';
	public $timestamps = false;

	protected $casts = [
		'sale_type_id' => 'int'
	];

	protected $dates = [
		'from',
		'to'
	];

	protected $fillable = [
		'from',
		'to',
		'name',
		'sale_type_id',
		'colour'
	];

	public function sales_type()
	{
		return $this->belongsTo(SalesType::class, 'sale_type_id');
	}

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'sales_articles')
					->withPivot('sale_category_id', 'price', 'rank', 'name', 'photo');
	}
}

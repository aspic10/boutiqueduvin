<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SalesType
 * 
 * @property int $id
 * @property string $name
 * @property int $nb_articles
 * @property bool $has_categories
 * @property string $pdf_view
 * @property bool $select_photos
 * 
 * @property Collection|Sale[] $sales
 *
 * @package App\Models
 */
class SalesType extends Model
{
	protected $table = 'sales_types';
	public $timestamps = false;

	protected $casts = [
		'nb_articles' => 'int',
		'has_categories' => 'bool',
		'select_photos' => 'bool'
	];

	protected $fillable = [
		'name',
		'nb_articles',
		'has_categories',
		'pdf_view',
		'select_photos'
	];

	public function sales()
	{
		return $this->hasMany(Sale::class, 'sale_type_id');
	}
}

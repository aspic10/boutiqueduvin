<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ShopIcon
 * 
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $icon
 * @property int $rank
 * 
 * @property Collection|ShopIcon[] $icons
 *
 * @package App\Models
 */
class ShopIcon extends Model {
	protected $table = 'shop_icons';
	public $timestamps = false;

        protected $casts = [
		'rank' => 'int'
	];
        
	protected $fillable = [
		'title',
		'description',
		'icon',
		'rank'
	];
}

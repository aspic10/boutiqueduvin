<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteText
 * 
 * @property int $id
 * @property string $label
 * @property string $content
 * 
 * @property Collection|SiteText[] $texts
 *
 * @package App\Models
 */
class SiteText extends Model {
	protected $table = 'site_texts';
	public $timestamps = false;

	protected $fillable = [
		'label',
		'content',
	];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersArticle
 * 
 * @property int $id
 * @property int $supplier_id
 * @property int $article_id
 * @property float $price
 * @property string $reference
 * @property int $supplier_article_category_id
 * @property int $rank
 * @property float $suggested_price
 * @property int $refund
 * 
 * @property Supplier $supplier
 * @property Article $article
 * @property SuppliersArticlesCategory $suppliers_articles_category
 * @property Collection|SuppliersArticlesSchedule[] $suppliers_articles_schedules
 * @property Collection|SuppliersSalesArticle[] $suppliers_sales_articles
 *
 * @package App\Models
 */
class SuppliersArticle extends Model
{
	protected $table = 'suppliers_articles';
	public $timestamps = false;

	protected $casts = [
		'supplier_id' => 'int',
		'article_id' => 'int',
		'price' => 'float',
		'supplier_article_category_id' => 'int',
		'rank' => 'int',
		'suggested_price' => 'float',
		'refund' => 'int'
	];

	protected $fillable = [
		'supplier_id',
		'article_id',
		'price',
		'reference',
		'supplier_article_category_id',
		'rank',
		'suggested_price',
		'refund'
	];

	public function supplier()
	{
		return $this->belongsTo(Supplier::class);
	}

	public function article()
	{
		return $this->belongsTo(Article::class);
	}

	public function suppliers_articles_category()
	{
		return $this->belongsTo(SuppliersArticlesCategory::class, 'supplier_article_category_id');
	}

	public function suppliers_articles_schedules()
	{
		return $this->hasMany(SuppliersArticlesSchedule::class, 'supplier_article_id');
	}

	public function suppliers_sales_articles()
	{
		return $this->hasMany(SuppliersSalesArticle::class, 'supplier_article_id');
	}
}

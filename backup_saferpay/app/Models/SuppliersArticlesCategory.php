<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersArticlesCategory
 * 
 * @property int $id
 * @property string $name
 * @property bool $default
 * 
 * @property Collection|SuppliersArticle[] $suppliers_articles
 *
 * @package App\Models
 */
class SuppliersArticlesCategory extends Model
{
	protected $table = 'suppliers_articles_categories';
	public $timestamps = false;

	protected $casts = [
		'default' => 'bool'
	];

	protected $fillable = [
		'name',
		'default'
	];

	public function suppliers_articles()
	{
		return $this->hasMany(SuppliersArticle::class, 'supplier_article_category_id');
	}
}

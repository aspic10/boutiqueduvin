<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersBillsVat
 * 
 * @property int $id
 * @property int $supplier_bill_id
 * @property float $vat
 * @property float $amount
 * 
 * @property SuppliersBill $suppliers_bill
 *
 * @package App\Models
 */
class SuppliersBillsVat extends Model
{
	protected $table = 'suppliers_bills_vats';
	public $timestamps = false;

	protected $casts = [
		'supplier_bill_id' => 'int',
		'vat' => 'float',
		'amount' => 'float'
	];

	protected $fillable = [
		'supplier_bill_id',
		'vat',
		'amount'
	];

	public function suppliers_bill()
	{
		return $this->belongsTo(SuppliersBill::class, 'supplier_bill_id');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersOrder
 * 
 * @property int $id
 * @property int $supplier_id
 * @property Carbon $date
 * @property string $remark
 * @property string $internal_remark
 * 
 * @property Supplier $supplier
 * @property Collection|SuppliersBill[] $suppliers_bills
 * @property Collection|Article[] $articles
 *
 * @package App\Models
 */
class SuppliersOrder extends Model
{
	protected $table = 'suppliers_orders';
	public $timestamps = false;

	protected $casts = [
		'supplier_id' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'supplier_id',
		'date',
		'remark',
		'internal_remark'
	];

	public function supplier()
	{
		return $this->belongsTo(Supplier::class);
	}

	public function suppliers_bills()
	{
		return $this->hasMany(SuppliersBill::class, 'supplier_order_id');
	}

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'suppliers_orders_articles', 'supplier_order_id')
					->withPivot('quantity', 'quantity_received');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersSale
 * 
 * @property int $id
 * @property int $supplier_id
 * @property Carbon $from
 * @property Carbon $to
 * @property string $name
 * 
 * @property Supplier $supplier
 * @property Collection|SuppliersSalesArticle[] $suppliers_sales_articles
 *
 * @package App\Models
 */
class SuppliersSale extends Model
{
	protected $table = 'suppliers_sales';
	public $timestamps = false;

	protected $casts = [
		'supplier_id' => 'int'
	];

	protected $dates = [
		'from',
		'to'
	];

	protected $fillable = [
		'supplier_id',
		'from',
		'to',
		'name'
	];

	public function supplier()
	{
		return $this->belongsTo(Supplier::class);
	}

	public function suppliers_sales_articles()
	{
		return $this->hasMany(SuppliersSalesArticle::class, 'supplier_sale_id');
	}
}

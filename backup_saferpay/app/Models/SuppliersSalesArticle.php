<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersSalesArticle
 * 
 * @property int $supplier_sale_id
 * @property int $supplier_article_id
 * @property float $price
 * 
 * @property SuppliersArticle $suppliers_article
 * @property SuppliersSale $suppliers_sale
 *
 * @package App\Models
 */
class SuppliersSalesArticle extends Model
{
	protected $table = 'suppliers_sales_articles';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'supplier_sale_id' => 'int',
		'supplier_article_id' => 'int',
		'price' => 'float'
	];

	protected $fillable = [
		'price'
	];

	public function suppliers_article()
	{
		return $this->belongsTo(SuppliersArticle::class, 'supplier_article_id');
	}

	public function suppliers_sale()
	{
		return $this->belongsTo(SuppliersSale::class, 'supplier_sale_id');
	}
}

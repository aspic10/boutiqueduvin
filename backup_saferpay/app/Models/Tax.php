<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tax
 * 
 * @property int $id
 * @property int $vat_id
 * @property string $name
 * @property float $price
 * @property bool $archived
 * 
 * @property Vat $vat
 * @property Collection|Article[] $articles
 * @property Collection|Bill[] $bills
 *
 * @package App\Models
 */
class Tax extends Model
{
	protected $table = 'taxes';
	public $timestamps = false;

	protected $casts = [
		'vat_id' => 'int',
		'price' => 'float',
		'archived' => 'bool'
	];

	protected $fillable = [
		'vat_id',
		'name',
		'price',
		'archived'
	];

	public function vat()
	{
		return $this->belongsTo(Vat::class);
	}

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'articles_taxes')
					->withPivot('quantity');
	}

	public function bills()
	{
		return $this->belongsToMany(Bill::class, 'bills_taxes')
					->withPivot('price', 'quantity', 'vat');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Title
 * 
 * @property int $id
 * @property string $name
 * @property string $abbreviation
 * 
 * @property Collection|Client[] $clients
 *
 * @package App\Models
 */
class Title extends Model
{
	protected $table = 'titles';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'abbreviation'
	];

	public function clients()
	{
		return $this->hasMany(Client::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * 
 * @property int $id
 * @property int $role_id
 * @property string $firstname
 * @property string $lastname
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $phone
 * @property bool $archived
 * @property string $initials
 * 
 * @property Role $role
 * @property Collection|Bill[] $bills
 * @property Collection|ClientsOrder[] $clients_orders
 * @property Collection|Client[] $clients
 * @property Collection|Message[] $messages
 *
 * @package App\Models
 */
class User extends Model
{
	protected $table = 'users';
	public $timestamps = false;

	protected $casts = [
		'role_id' => 'int',
		'archived' => 'bool'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'role_id',
		'firstname',
		'lastname',
		'username',
		'password',
		'email',
		'phone',
		'archived',
		'initials'
	];

	public function role()
	{
		return $this->belongsTo(Role::class);
	}

	public function bills()
	{
		return $this->belongsToMany(Bill::class, 'bills_users');
	}

	public function clients_orders()
	{
		return $this->hasMany(ClientsOrder::class);
	}

	public function clients()
	{
		return $this->belongsToMany(Client::class, 'clients_users');
	}

	public function messages()
	{
		return $this->hasMany(Message::class, 'sender_id');
	}
}

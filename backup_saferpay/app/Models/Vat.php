<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Vat
 * 
 * @property int $id
 * @property string $name
 * @property float $value
 * @property string $code
 * 
 * @property Collection|Article[] $articles
 * @property Collection|Tax[] $taxes
 *
 * @package App\Models
 */
class Vat extends Model
{
	protected $table = 'vats';
	public $timestamps = false;

	protected $casts = [
		'value' => 'float'
	];

	protected $fillable = [
		'name',
		'value',
		'code'
	];

	public function articles()
	{
		return $this->hasMany(Article::class);
	}

	public function taxes()
	{
		return $this->hasMany(Tax::class);
	}
}

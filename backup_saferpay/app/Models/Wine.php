<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Wine
 * 
 * @property int $id
 * @property int $year
 * @property bool $wine_price_list
 * @property int $colour_id
 * @property int $region_id
 * @property string $code_regie
 * @property int $appellation_id
 * 
 * @property Colour $colour
 * @property Appellation $appellation
 * @property Region $region
 * @property Article $article
 * @property Collection|Enomatic[] $enomatics
 * @property Collection|Characteristic[] $characteristics
 * @property Collection|Distinction[] $distinctions
 * @property Collection|Variety[] $varieties
 *
 * @package App\Models
 */
class Wine extends Model
{
	protected $table = "wines";
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
            "id" => "int",
            "year" => "int",
            "wine_price_list" => "bool",
            "colour_id" => "int",
            "region_id" => "int",
            "appellation_id" => "int"
	];

	protected $fillable = [
            "year",
            "wine_price_list",
            "colour_id",
            "region_id",
            "code_regie",
            "appellation_id"
	];
    
    
	public function colour()
	{
		return $this->belongsTo(Colour::class);
	}

	public function appellation()
	{
		return $this->belongsTo(Appellation::class);
	}

	public function region()
	{
		return $this->belongsTo(Region::class);
	}

	public function article()
	{
		return $this->belongsTo(Article::class, "id", "id");
	}

	public function enomatics()
	{
		return $this->belongsToMany(Enomatic::class, "enomatics_wines")
					->withPivot("rank");
	}

	public function characteristics()
	{
		return $this->belongsToMany(Characteristic::class, "wines_characteristics");
	}

	public function distinctions()
	{
		return $this->belongsToMany(Distinction::class, "wines_distinctions")
					->withPivot("rating");
	}

	public function varieties()
	{
            return $this->belongsToMany(Variety::class, "wines_varieties")
					->withPivot("percentage");
	}
        
        
    /**************************************************************************/
    /* FUNCTIONS                                                              */
    /**************************************************************************/
    /**
     * To string
     */
    public function __toString() {
        $name = $this->article->__toString();
        
        if(!is_null($this->year)){
            $name .= " " . $this->year;
        }
        
        return $name;
    }
    
    
    /**
     * Requête par défaut pour récupérer les vins vendus sur le Shop
     */
    public static function defaultQuery(){
        $queryBase = Wine::select("articles.*", "wines.*", "prices.price AS shopPrice")
            ->join("articles", "articles.id", "=", "wines.id")
            ->orderBy("articles.favourite", "desc")
            ->orderBy("articles.code", "asc")
            ->distinct();
        
        $query = self::defaultQueryElements($queryBase);
        
        return $query;
    }
    
    
    /**
     * Requête par défaut pour récupérer les vins vendus sur le Shop
     * sans select ni groupBy
     * Retourne une collection et non des objets Wine
     */
    public static function defaultQueryFilters(){
        $queryBase = DB::table("wines")
            ->join("articles", "articles.id", "=", "wines.id");
        
        $query = self::defaultQueryElements($queryBase);
        
        return $query;
    }
    
    
    /**
     * Eléments de base de la requête pour récupérer les articles en vente sur
     * le Shop
     */
    public static function defaultQueryElements($query){
        $query->join("prices", "prices.article_id", "=", "articles.id")
            ->join("clients_types", "clients_types.id", "=", "prices.client_type_id")
            ->where([
                ["articles.archived", "=", false],
                ["articles.shop", "=", true],
                ["clients_types.name", "=", "Shop"],
            ]);
        
        return $query;
    }
}

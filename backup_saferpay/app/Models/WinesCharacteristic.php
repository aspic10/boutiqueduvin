<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WinesCharacteristic
 * 
 * @property int $wine_id
 * @property int $characteristic_id
 * 
 * @property Wine $wine
 * @property Characteristic $characteristic
 *
 * @package App\Models
 */
class WinesCharacteristic extends Model
{
	protected $table = 'wines_characteristics';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'wine_id' => 'int',
		'characteristic_id' => 'int'
	];

	public function wine()
	{
		return $this->belongsTo(Wine::class);
	}

	public function characteristic()
	{
		return $this->belongsTo(Characteristic::class);
	}
}

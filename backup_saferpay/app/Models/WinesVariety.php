<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WinesVariety
 * 
 * @property int $wine_id
 * @property int $variety_id
 * @property int $percentage
 * 
 * @property Wine $wine
 * @property Variety $variety
 *
 * @package App\Models
 */
class WinesVariety extends Model
{
	protected $table = 'wines_varieties';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'wine_id' => 'int',
		'variety_id' => 'int',
		'percentage' => 'int'
	];

	protected $fillable = [
		'percentage'
	];

	public function wine()
	{
		return $this->belongsTo(Wine::class);
	}

	public function variety()
	{
		return $this->belongsTo(Variety::class);
	}
}

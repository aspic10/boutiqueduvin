<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;
use App\Models\Client;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('customclientprovider', function($app, array $config) {
            return new CustomClientProvider($app['hash'], $config['model']);
        });
        
        VerifyEmail::toMailUsing(function (Client $client, string $verificationUrl) {
            return (new MailMessage)
                ->subject(Lang::get('auth.verify'))
                ->line(Lang::get('auth.click_button'))
                ->action(Lang::get('auth.verify'), $verificationUrl)
                ->line(Lang::get('auth.no_action'));
        });
        
        ResetPassword::toMailUsing(function (Client $client, string $token) {
            return (new MailMessage)
                ->subject(Lang::get('auth.reset_password'))
                ->line(Lang::get('auth.password_request'))
                ->action(Lang::get('auth.reset_password'), url(config('app.url').route('password.reset', $token, false)))
                ->line(Lang::get('auth.password_expire', ['count' => config('auth.passwords.'.config('auth.defaults.passwords').'.expire')]))
                ->line(Lang::get('auth.password_no_action'));
        });
    }
}
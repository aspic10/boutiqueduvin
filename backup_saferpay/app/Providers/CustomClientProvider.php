<?php
namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider as UserProvider;
use App\Models\Client;


class CustomClientProvider extends UserProvider {

    /**
     * Sélectionner le client selon l'email
     *
     * @param array $credentials
     * @return implementation of Authenticatable
     */
    public function retrieveByCredentials(array $credentials) {
        $client = Client::join("companies", "companies.id", "=", "clients.id")
            ->where([
                ["companies.email", $credentials["email"]],
                ["companies.archived", false],
            ])
            ->first();
        
        return $client;
    }
}
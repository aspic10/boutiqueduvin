<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class UniqueEmailClient implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $company = \App\Models\Company::where(["type" => "client", "email" => $value])
            ->count();
        
        return $company == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.email.unique');
    }
}

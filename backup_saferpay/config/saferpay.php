<?php
/* 
 * Configuration de Saferpay
 */

return [
    'clientId' => env('SAFERPAY_CLIENT_ID'),
    'terminalId' => env('SAFERPAY_TERMINAL_ID'),
    'baseUrl' => env('SAFERPAY_BASE_URL', 'https://www.saferpay.com/api/'),
    'specVersion' => env('SAFERPAY_SPEC_VERSION', '1.23'),
    'apiUser' => env('SAFERPAY_API_USER'),
    'apiPassword' => env('SAFERPAY_API_PASSWORD'),
    'emails' => explode(";", env('SAFERPAY_EMAILS')),
];
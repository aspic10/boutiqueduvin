/* 
 * Gestion du panier
 */

// Contenu du panier (tableau d'articles)
var cart = JSON.parse(localStorage.getItem("cart") || null) || {};

// Informations du panier (adresse, paiement, etc.)
var checkout = JSON.parse(localStorage.getItem("checkout") || null) || {};

// Tableau d'aperçu du panier dans le menu
var cartOverview;

// Nombre de bouteilles dans le panier
var spanNbArticlesCart;

// Quantité d'un article déjà dans le panier
var quantityCart = 0;

var quantityChangedSpan = jQuery("<span>").addClass("info max")
    .html("Quantité maximale");
var soonAvailableSpan = jQuery("<span>").addClass("info soon")
    .html("Délai de livraison : 2 semaines");


/**
 * Chargement de la page
 */
document.addEventListener('DOMContentLoaded', function(){
    cartOverview = jQuery("#cart-overview table");
    spanNbArticlesCart = jQuery("#nb-articles-cart");
    
    // Ajouter chaque article dans l'aperçu du panier
    for(var articleId in cart){
        addItemCartOverview(articleId);
    }
    
    totalCartOverview();
});


/**
 * Ajouter un article au panier
 */
function addToCart(element, articleId, name, price){
    var container = element.parents(".cart-container");
    
    // Quantité d'un article déjà dans le panier
    var spanQuantityCart = container.find(".quantity-cart");
    var divAlreadyInCart = container.find(".already-in-cart");

    // Quantité saisie à ajouter au panier
    var inputQuantity = container.find(".quantity");
    if(inputQuantity.length === 0){
        var quantity = 1;
    }
    else {
        var quantity = parseInt(inputQuantity.val()) || 0;
    }
    
    var max = parseInt(inputQuantity.attr("max")) || null;
    var maxSoon = parseInt(inputQuantity.data("max")) || 0;
    
    quantityCart += quantity;
    
    container.find(".soon, .max").remove();
    
    // Commande supérieure au stock disponible
    if(max !== null && max < quantityCart){
        quantityCart = max;
        container.append(quantityChangedSpan.clone());
    }
    else {
        // Commande supérieure au stock disponible mais livrable rapidement
        if(maxSoon !== null && maxSoon < quantityCart){
            container.append(soonAvailableSpan.clone());
        }
    }

    // Remplacer/ajouter le vin dans le localStorage
    var cartItem = {
        quantity: quantityCart,
        name: name,
        price: price,
        type: "wine",
    };

    cart[articleId] = cartItem;
    localStorage.setItem("cart", JSON.stringify(cart));

    // Afficher la quantité déjà dans le panier
    spanQuantityCart.html(quantityCart);
    divAlreadyInCart.show();

    // Ajouter la ligne dans l'aperçu du panier
    addItemCartOverview("" + articleId);

    // Animation pour montrer qu'un article a été ajouté
    spanNbArticlesCart.addClass("add");
    spanQuantityCart.addClass("add");
    setTimeout(function(){
        spanNbArticlesCart.removeClass("add");
        spanQuantityCart.removeClass("add");
    }, 1000);
}


/**
 * Ajouter un bon cadeau au panier
 */
function addGiftToCart(element, price){
    var container = element.parents(".gift-card-overview");
    
    // Quantité d'un article déjà dans le panier
    var spanQuantityCart = container.find(".quantity-cart");
    var divAlreadyInCart = container.find(".already-in-cart");

    // Quantité à ajouter au panier
    var quantity = 1 + (parseInt(spanQuantityCart.html()) || 0);
    
    if(quantity > 1){
        var name = "Bons cadeaux";
    }
    else {
        var name = "Bon cadeau";
    }
    
    // Remplacer/ajouter le vin dans le localStorage
    var cartItem = {
        quantity: quantity,
        name: name,
        price: price,
        type: "gift"
    };

    cart["gift-" + price] = cartItem;
    localStorage.setItem("cart", JSON.stringify(cart));

    // Afficher la quantité déjà dans le panier
    spanQuantityCart.html(quantity);
    divAlreadyInCart.show();

    // Ajouter la ligne dans l'aperçu du panier
    addItemCartOverview("gift-" + price);

    // Animation pour montrer qu'un article a été ajouté
    spanNbArticlesCart.addClass("add");
    spanQuantityCart.addClass("add");
    setTimeout(function(){
        spanNbArticlesCart.removeClass("add");
        spanQuantityCart.removeClass("add");
    }, 1000);
}


/**
 * Ajouter ou mettre à jour une ligne dans l'aperçu du panier
 */
function addItemCartOverview(articleId) {
    // Récupérer le vin dans le localStorage
    var cartItem = cart[articleId] || null;
    var quantity = cartItem.quantity || 0;
    
    if(quantity === 0){
        delete cart[articleId];
        localStorage.setItem("cart", JSON.stringify(cart));
    }
    else {
        var price = cartItem.price;
        var totalArticle = formatFloat(Math.round(quantity * price * 20) / 20);

        // Ligne dans le panier
        var tr = cartOverview.find("tbody tr[data-id='" + articleId + "']");

        // Article déjà dans le panier -> mettre à jour la quantité et le prix
        if(tr.length > 0){
            tr.find(".quantity").html(quantity);
            tr.find(".total").html(totalArticle);
        }
        // Nouvel article du panier -> ajouter une ligne
        else {
            tr = jQuery("<tr data-id='" + articleId + "'>");
            tr.append(jQuery("<td>").html(quantity).addClass("center quantity"));
            tr.append(jQuery("<td>").html(cartItem.name));
            tr.append(jQuery("<td>").html(formatFloat(price)).addClass("right price"));
            tr.append(jQuery("<td>").html(totalArticle).addClass("right bold total"));
            tr.append(jQuery("<td>").html('<i class="far fa-trash-alt action" onclick="removeFromCartOverview(\'' + articleId + '\')"></i>').addClass("center"));

            cartOverview.find("tbody").append(tr);
        }
    }

    // Calculer le total du panier
    totalCartOverview();
}


/**
 * Supprimer un article du panier 
 */
function removeFromCartOverview(articleId){
    // Masquer la quantité déjà présente
    jQuery(".already-in-cart").hide();
    quantityCart = 0;
    jQuery(".already-in-cart .quantity-cart").html("");
    
    // Supprimer la ligne de l'aperçu du panier
    cartOverview.find("tbody tr[data-id='" + articleId + "']").remove();
    
    // Supprimer l'info sur les quantités maximales disponibles
    jQuery(".cart-container .soon, .cart-container .max").remove();
    
    // Supprimer l'article du localStorage
    delete cart[articleId];

    localStorage.setItem("cart", JSON.stringify(cart));
    
    // Mettre à jour le total du panier
    totalCartOverview();
}


/**
 * Calculer le total du panier
 */
function totalCartOverview(){
    // Prix total
    var totalCart = 0;
    // Nombre de bouteilles
    var nbBottles = 0;
    // Nombre d'articles
    var nbArticles = 0;
    // Panier vide
    var emptyCart = true;
    
    // Parcourir chaque article du panier
    for(var articleId in cart){
        emptyCart = false;

        var cartItem = cart[articleId];
        var type = cartItem.type;
        var quantity = parseInt(cartItem.quantity);
        var price = cartItem.price;
        var totalArticle = Math.round(quantity * price * 20) / 20;
        totalCart += totalArticle;
        nbArticles += quantity;
        
        if(type === "wine"){
            nbBottles += quantity;
        }
    }
    
    // Panier vide
    if(emptyCart === true){
        jQuery("#cart-overview, #cart-buttons").hide();
        jQuery(".empty-cart").show();
        spanNbArticlesCart.hide();
    }
    // Articles dans le panier
    else {
        // Nombre total de bouteilles
        if(nbBottles > 0){
            var nbBottlesLabel = nbBottles + " bouteille";
            if(nbBottles > 1){
                nbBottlesLabel += "s";
            }

            cartOverview.find("tfoot .nb-bottles").html(nbBottlesLabel);
        }
        
        // Total du panier
        cartOverview.find("tfoot .total").html(formatFloat(totalCart));

        jQuery("#cart-overview, #cart-buttons").show();
        jQuery(".empty-cart").hide();

        spanNbArticlesCart.html(nbArticles).show();
    }
}


/**
 * Vider le panier
 */
function emptyCart(){
    localStorage.removeItem("cart");
    document.location.reload();
}
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'hello' => 'Hello!',
    'whoops' => 'Whoops!',
    'regards' => 'Regards',
    'trouble_click' => 'If you\'re having trouble clicking the button, copy and paste the URL below into your web browser',
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Identifiants incorrects',
    'throttle' => 'Nombre de tentatives de connexion trop élevé. Veuillez réessayer dans :seconds secondes.',

    /* Mot de passe oublié */
    'reset_password' => 'Définir un nouveau mot de passe',
    'send_password_link' => 'Envoyer un lien',
    'password_request' => 'Vous recevez cet email car nous avons reçu une demande de réinitialisation de mot de passe pour votre compte.',
    'password_expire' => 'Ce lien de réinitialisation expirera dans :count minutes.',
    'password_no_action' => 'Si vous n\'êtes pas à l\'origine de cette demande, aucune action de votre part n\'est nécessaire.',
    'confirm_password' => 'Confirmer le mot de passe',
    'confirm_password_info' => 'Merci de confirmer votre mot de passe avant de continuer.',
    
    /* Email de vérification */
    'verify' => 'Vérifiez votre adresse email',
    'link_sent' => 'Un nouveau lien de vérification a été envoyé à :email',
    'check_email' => 'Avant de poursuivre, veuillez cliquer sur le lien transmis à votre adresse email.',
    'link_not_received' => 'Si vous n\'avez pas reçu d\'email, vérifiez votre dossier spam ou',
    'new_link' => 'pour recevoir un nouveau lien',
    'click' => 'cliquez ici',
    'click_button' => 'Veuillez cliquer sur le bouton ci-dessous pour valider votre adresse email.',
    'no_action' => 'Si vous n\'êtes pas à l\'origine de cette inscription, aucune action de votre part n\'est nécessaire.',
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'hello' => 'Bonjour !',
    'whoops' => 'Oups !',
    'regards' => 'Meilleures salutations',
    'trouble_click' => 'Si vous ne parvenez pas à cliquer sur le bouton, copiez et collez le lien ci-dessous dans votre navigateur',
];

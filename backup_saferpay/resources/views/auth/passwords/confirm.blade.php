@extends('layouts.shop')
@section('headTitle', 'Confirmation')

@section('content')
    <h1>{{ __('auth.confirm_password') }}</h1>
    <p>
        {{ __('auth.confirm_password_info') }}
    </p>

    <form method="POST" action="{{ route('password.confirm') }}">
        @csrf

        <div class="row">
            <label for="password">Mot de passe</label>

            <div>
                <input id="password" type="password" class="@error('password') @enderror" 
                       name="password" required autocomplete="current-password">

                @error('password')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </div>
        </div>
        
        <div class="row">
            <div>
                <button type="submit">
                    {{ __('auth.confirm_password') }}
                </button>

                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}">
                        Mot de passe oublié ?
                    </a>
                @endif
            </div>
        </div>
    </form>
@endsection

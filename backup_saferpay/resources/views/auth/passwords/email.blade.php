@extends('layouts.shop')
@section('headTitle', 'Mot de passe oublié')

@section('content')
    <h1>{{ __('auth.reset_password') }}</h1>

    <div class="card-body">
        @if (session('status'))
            <div class="alert success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="row">
                <label for="email">Adresse email</label>

                <div>
                    <input id="email" type="email" class="@error('email') @enderror" 
                           name="email" value="{{ old('email') }}" 
                           required autocomplete="email" autofocus>

                    @error('email')
                        <span class="error" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div>
                    <button type="submit">
                        {{ __('auth.send_password_link') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

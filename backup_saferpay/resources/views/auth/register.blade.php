@extends('layouts.shop')
@section('headTitle', 'Inscription')

@section('content')
    <div class="columns-container">
        <div class="column one-half">
            <h1>Créer un compte</h1>

            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="row">
                    <label for="lastname">Nom *</label>
                    <div>
                        <input id="lastname" type="text" class="@error('lastname') error @enderror"
                               name="lastname" value="{{ old('lastname') }}" 
                               required autocomplete="lastname" autofocus>

                        @error('lastname')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="firstname">Prénom *</label>
                    <div>
                        <input id="firstname" type="text" class="@error('firstname') error @enderror"
                               name="firstname" value="{{ old('firstname') }}" 
                               required autocomplete="firstname" autofocus>

                        @error('firstname')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="company">Société</label>
                    <div>
                        <input id="company" type="text" class="@error('company') error @enderror" 
                               name="company" value="{{ old('company') }}" 
                               autocomplete="company" autofocus>

                        @error('company')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="address">Adresse *</label>
                    <div>
                        <input id="address" type="text" class="@error('address') error @enderror" 
                               name="address" value="{{ old('address') }}" 
                               required autocomplete="address" autofocus>

                        @error('address')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="postal_code">NPA *</label>
                    <div>
                        <input id="postal_code" type="text" class="@error('postal_code') error @enderror"
                               name="postal_code" value="{{ old('postal_code') }}"
                               required autocomplete="postal_code" autofocus>

                        @error('postal_code')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="city">Localité *</label>
                    <div>
                        <input id="city" type="text" class="@error('city') error @enderror"
                               name="city" value="{{ old('city') }}"
                               required autocomplete="city" autofocus>

                        @error('city')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="city">Pays</label>
                    <div>
                        <input type="text" value="Suisse" disabled="">
                    </div>
                </div>

                <div class="row">
                    <label for="email">Email *</label>
                    <div>
                        <input id="email" type="email" class="@error('email') error @enderror"
                               name="email" value="{{ old('email') }}"
                               required autocomplete="email">

                        @error('email')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="phone">Téléphone</label>
                    <div>
                        <input id="phone" type="phone" class="@error('phone') error @enderror"
                               name="phone" value="{{ old('phone') }}"
                               autocomplete="phone">

                        @error('phone')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="password">Mot de passe *</label>
                    <div>
                        <input id="password" type="password" class="@error('password') error @enderror"
                               name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="password-confirm">Confirmez le mot de passe *</label>
                    <div>
                        <input id="password-confirm" type="password" class="form-control"
                               name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>

                <div class="row">
                    <button type="submit">
                        Créer un compte
                    </button>
                </div>
            </form>
        </div>
        <div class="column one-half">
            <h3>Vous avez déjà un compte ?</h3>
            <button>
                <a href="{{ route('login') }}">
                    Connexion
                </a>
            </button>
        </div>
    </div>
@endsection

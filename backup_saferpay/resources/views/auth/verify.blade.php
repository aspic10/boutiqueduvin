@extends('layouts.shop')

@section('content')
<h1>{{ __('auth.verify') }}</h1>

@if (session('resent'))
    <div class="alert success" role="alert">
        {{ __('auth.link_sent', ["email" => Auth::user()->email]) }}
    </div>
@endif

<p>
    {{ __('auth.check_email') }}
</p>

<p>
    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
        @csrf
        {{ __('auth.link_not_received') }}
        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('auth.click') }}</button> {{ __('auth.new_link') }}.
    </form>
</p>
@endsection

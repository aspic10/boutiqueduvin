{{-- Formulaire d'ajout d'un bon cadeau --}}
<form method="POST" action="" id="form-gift-card">
    @csrf
    <div class="row">
        <label for="number">Numéro du bon</label>
        <div>
            <input id="number" type="text" name="number"/>
        </div>
    </div>
    <div class="row">
        <label for="pin">Code PIN</label>
        <div>
            <input id="pin" type="text" name="pin"/>
        </div>
    </div>
    <span class="gift-card-error error">
        Ce bon n'est pas valable ou a déjà été utilisé
    </span>
    <div class="row">
        <div>
            <button type="submit">
                Utiliser le bon
            </button>
        </div>
    </div>
</form>

<script type="text/javascript">
    var giftCardForm = jQuery("#form-gift-card");
    var giftCardError = jQuery(".gift-card-error");
    
    /**
     * Utiliser un bon cadeau
     */
    giftCardForm.submit(function(e){
        e.preventDefault();
        let number = jQuery("#number").val().trim();

        // La carte n'est pas encore ajoutée
        if(number in giftCards === false){
            var data = giftCardForm.serialize();
            
            jQuery.ajax({
                type: "POST",
                url: "{{ route('getGiftCard') }}",
                data: data,
                success: function(amount){
                    if(amount > 0){
                        // Ajouter la carte au compte client
                        jQuery.ajax({
                            type: "POST",
                            url: "{{ route('addGiftCard') }}",
                            async: false,
                            data: data
                        });
                        
                        @if(Route::currentRouteName() === "clientGiftCards")
                            document.location.reload();
                        @else
                            let pin = jQuery("#pin").val().trim();

                            // Ajouter dans le localStorage
                            giftCards[number] = {
                                pin: pin,
                                amount: amount
                            };
                            checkout["giftCards"] = giftCards;
                            localStorage.setItem("checkout", JSON.stringify(checkout));

                            // Afficher le bon cadeau
                            showGiftCard(number, pin);

                            // Masquer le message d'erreur
                            giftCardError.hide();
                            giftCardForm.trigger("reset");
                        @endif
                    }
                    // Carte introuvable
                    else {
                        // Afficher le message d'erreur
                        giftCardError.show();
                    }
                }
            });
        }

        return false;
    });
</script>
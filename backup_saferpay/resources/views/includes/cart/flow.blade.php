@php
$flow = array(
    "cart" => "Panier",
    "cart/login" => "Connexion",
    "shipping" => "Adresse",
    "payment" => "Paiement",
    "validate" => "Validation",
);

$previous = true;
@endphp

<div id="cart-flow">
    @foreach($flow as $route => $label)
        @php
        $class = "";
        $onclick = "";
        if($current == $route) {
            $class = "current";
            $previous = false;
        }
        
        if($previous == true){
            $class = "previous";
            
            if($current != "confirm"){
                $onclick = "onclick=document.location.href='" . route($route) . "'";
            }
        }
        @endphp
    
        <div class="step {{ $class }}" {{ $onclick }}>
            <span>
                {{ $label }}
            </span>
        </div>
    @endforeach
</div>
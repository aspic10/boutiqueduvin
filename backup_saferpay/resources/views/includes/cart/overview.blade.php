{{-- Contenu du panier --}}
<div id="cart">
    <table id="cart-table" class="table-full-width">
        <thead>
            <tr>
                <th class="center" style="width: 100px;">Quantité</th>
                <th>Désignation</th>
                <th class="center" style="width: 100px;">Format</th>
                <th class="right" style="width: 100px;">Prix</th>
                <th class="right" style="width: 100px;">Total</th>
                @if($page === "cart/index")
                    <th/>
                @endif
            </tr>
        </thead>
        <tbody></tbody>
        <tfoot>
            <tr>
                <th colspan="2" class="nb-bottles"></th>
                <th colspan="2" class="right">Sous-total</th>
                <th class="right subtotal"></th>
                @if($page === "cart/index")
                    <th/>
                @endif
            </tr>
            <tr>
                <th colspan="4" class="right">Frais de port</th>
                <th class="right delivery"></th>
                @if($page === "cart/index")
                    <th/>
                @endif
            </tr>

            @if($page === "cart/index")
                <tr>
                    <th colspan="4" class="right">Retrait à Saxon</th>
                    <th class="right">
                        <input type="checkbox" id="pickup"/>
                    </th>
                    <th/>
                </tr>
            @endif

            <tr class="total">
                <th colspan="4" class="right">Total en CHF</th>
                <th class="right total"></th>
                @if($page === "cart/index")
                    <th/>
                @endif
            </tr>
            
            @if($page === "validate")
                <tr id="gift-card-template" style="display: none;">
                    <td colspan="4" class="right">
                        - Bon <span class="number"></span>
                        <div class="balance-container">
                            Solde : CHF <span class="balance"></span>
                        </div>
                    </td>
                    <td class="right">
                        -<span class="amount"></span>
                    </td>
                </tr>
                
                <tr id="balance-container" style="display: none;">
                    <th colspan="4" class="right">Solde à payer en CHF</th>
                    <th class="right balance"></th>
                </tr>
            @endif
        </tfoot>
    </table>
</div>

<script type="text/javascript">
    var cartContainer = jQuery("#cart");
    var cartTable = jQuery("#cart-table");
    var cartBody = cartTable.find("tbody");
    var nbBottlesTh = cartTable.find("tfoot .nb-bottles");
    var subtotalTh = cartTable.find("tfoot th.subtotal");
    var totalTr = cartTable.find("tfoot tr.total");
    var totalTh = cartTable.find("tfoot th.total");
    var deliveryTh = cartTable.find("tfoot .delivery");
    var giftCardTemplate = jQuery("#gift-card-template");
    var balanceTr = jQuery("#balance-container");
    
    // Prix total
    var totalCart = 0;
    // Nombre de bouteilles
    var nbBottles = 0;

     // Infos sur les modifications du panier
    var nameChangedSpan = jQuery("<span>").addClass("info")
        .html("Désignation modifiée depuis l'ajout");
    var priceChangedSpan = jQuery("<span>").addClass("info")
        .html("Prix modifié depuis l'ajout");
    var quantityChangedSpan = jQuery("<span>").addClass("info")
        .html("Quantité maximale");
    var soonAvailableSpan = jQuery("<span>").addClass("info soon")
        .html("Délai de livraison : 2 semaines");
    var alertModifications = jQuery("#warning-modifications");


    /**
     * Afficher le panier au chargement
     */
    document.addEventListener('DOMContentLoaded', function(){
        @if($page === "cart/index")
            // Retrait à Saxon
            if(checkout["pickup"] === true) {
                checkboxPickup.prop("checked", true);
            }
            else {
                checkout["pickup"] = false;
            }
            
            localStorage.setItem("checkout", JSON.stringify(checkout));
        @endif

        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        if(cart === null){
            setTotalCart();
        }
        else {
            // Vérifier le panier avec le serveur
            jQuery.ajax({
                type: "POST",
                async: false,
                url: "{{ route('getCartInfo') }}",
                data: {
                    cart: cart
                },
                success: function(data){
                    alertModifications.hide();
                    
                    // Mettre à jour le panier dans le localStorage
                    cart = data.cart;
                    localStorage.setItem("cart", JSON.stringify(cart));

                    var info = data.info;

                    // Afficher chaque article
                    for(var articleId in data.cart){
                        let cartItem = data.cart[articleId];
                        let quantity = parseInt(cartItem.quantity);
                        let price = cartItem.price;
                        let type = cartItem.type;
                        let maxQuantity = info[articleId].maxQuantity;
                        
                        // Totaux
                        let totalArticle = Math.round(quantity * price * 20) / 20;
                        totalCart += totalArticle;
                        
                        if(type === "wine"){
                            nbBottles += quantity;
                        }
                        
                        // Ajouter la ligne
                        var tr = jQuery("<tr data-id='" + articleId + "' data-type='" + type + "'>");

                        // Quantité
                        @if($page === "cart/index")
                            var input = jQuery("<input>").val(quantity).addClass("quantity")
                                .attr("type", "number")
                                .attr("min", "0")
                                .attr("data-quantity", quantity)
                                .keypress(function(event){
                                    return onlyInt(event, false);
                                })
                                .change(function(){
                                    udpateItem(jQuery(this), price);
                                });

                            if(info[articleId].soonAvailable === true){
                                input.attr("data-max", maxQuantity);
                            }
                            else {
                                input.attr("max", maxQuantity);
                            }

                            var tdQuantity = jQuery("<td>").html(input)
                                .addClass("center");
                        @else
                            var tdQuantity = jQuery("<td>").html(quantity)
                                .addClass("center");
                        @endif

                        if(info[articleId].quantityChanged === true){
                            tdQuantity.append(quantityChangedSpan.clone());
                            alertModifications.show();
                        }

                        tr.append(tdQuantity);

                        // Nom
                        var tdName = jQuery("<td>").html(cartItem.name)
                            .addClass("name");
                        if(info[articleId].nameChanged === true){
                            tdName.append(nameChangedSpan.clone());
                            alertModifications.show();
                        }

                        if(info[articleId].soonAvailable === true){
                            if(quantity > maxQuantity) {
                                tdName.append(soonAvailableSpan.clone());
                            }
                        }

                        tr.append(tdName);

                        // Format
                        var tdFormat = jQuery("<td>").html(info[articleId].format)
                            .addClass("center");

                        tr.append(tdFormat);

                        // Prix
                        var tdPrice = jQuery("<td>").html(formatFloat(price))
                            .addClass("right price");
                        if(info[articleId].priceChanged === true){
                            tdPrice.append(priceChangedSpan.clone());
                            alertModifications.show();
                        }

                        tr.append(tdPrice);

                        // Total
                        tr.append(jQuery("<td>").html(formatFloat(totalArticle)).addClass("right bold total"));

                        @if($page === "cart/index")
                            tr.append(jQuery("<td>").html('<i class="far fa-trash-alt action" onclick="removeFromCart(\'' + articleId + '\')"></i>').addClass("center"));
                        @endif

                        cartBody.append(tr);
                    }
                    
                    if(data.total !== totalCart || data.bottles !== nbBottles){
                        document.location.reload();
                    }
                    else {
                        setTotalCart();
                    }
                }
            });
        }
    });
    
    /**
     * Calculer le total du panier
     */
    function setTotalCart() {
        // Panier vide
        var emptyCart = cartBody.find("tr").length === 0;
        var total = totalCart;

        // Panier vide
        if(emptyCart === true){
            cartContainer.hide();
        }
        // Articles dans le panier
        else {
            // Nombre total de bouteilles
            if(nbBottles > 0){
                var nbBottlesLabel = nbBottles + " bouteille";
                if(nbBottles > 1){
                    nbBottlesLabel += "s";
                }

                nbBottlesTh.html(nbBottlesLabel);
            }
            
            // Sous-total du panier
            subtotalTh.html(formatFloat(totalCart));

            // Calcul des frais de livraison
            if(checkout["pickup"] !== true){
                jQuery.ajax({
                    type: "POST",
                    async: false,
                    url: "{{ route('getDeliveryCost') }}",
                    data: {
                        nbBottles: nbBottles,
                        total: totalCart
                    },
                    success: function(data){
                        var delivery = parseFloat(data);
                        
                        if(delivery === 0){
                            deliveryTh.html("Offerts");
                        }
                        else {
                            deliveryTh.html(formatFloat(delivery));
                        }
                        
                        total += delivery;
                        totalTh.html(formatFloat(total));
                        
                        @if($page === "validate")
                            setTotalPay(total);
                        @endif
                    }
                });
            }
            // Retrait à Saxon
            else {
                deliveryTh.html("-");
                totalTh.html(formatFloat(total));
                
                @if($page === "validate")
                    setTotalPay(total);
                @endif
            }

            cartContainer.show();
        }
    }
</script>
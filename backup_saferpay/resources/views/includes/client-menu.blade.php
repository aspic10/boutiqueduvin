@php
$flow = array(
    "account" => "Coordonnées",
    "clientBills" => "Commandes",
    "clientGiftCards" => "Bons cadeaux",
    "clientWines" => "Vins favoris",
    "clientDelete" => "Supprimer mon compte",
);
@endphp

<h1>Mon compte</h1>

<div class="columns-container">
    <div class="column one-fourth" id="client-menu">
        @foreach($flow as $route => $label)
            @php
            $class = "";
            if($current == $route) {
                $class = "current";
            }
            @endphp

            <div class="step {{ $class }}" onclick="document.location.href='{{ route($route) }}'">
                <span>
                    {{ $label }}
                </span>
            </div>
        @endforeach
        
        <div class="step">
            <span>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    Déconnexion
                </a>
            </span>
        </div>
    </div>
<div class="dialog" id="dialog-add-to-cart">
    <div class="dialog-container">
        <i class="fas fa-times close-dialog"
           onclick="jQuery(this).parents('.dialog').removeClass('visible')"></i>
        <div class="add-to-cart-container dialog-content">
            <h2 class="title"></h2>
            <div id="price-container">
                <div class="price"></div>
                <div class="cart-container">
                    <span class="add-cart animated-button">
                        <i class="fas fa-cart-plus"></i>
                        Ajouter au panier
                    </span>
                    <div class="already-in-cart">
                        Déjà <span class="quantity-cart"></span> dans le panier
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var addToCartDialog = jQuery("#dialog-add-to-cart");
    var addToCartDiv = addToCartDialog.find(".add-to-cart-container");
    var addToCartContainer = addToCartDiv.find(".cart-container");
    var addToCartTitle = addToCartDiv.find(".title");
    var addToCartPrice = addToCartDiv.find(".price");
    var addToCartButton = addToCartDiv.find(".add-cart");
        

    /**
     * Ouvrir le dialog d'ajout au panier
     */
    function openAddToCartDialog(e, articleId, title, price, litres, soonAvailable, maxQuantity){
        e.stopPropagation();
        e.preventDefault();
        
        var divAlreadyInCart = addToCartDiv.find(".already-in-cart");

        addToCartDiv.find(".soon, .max, .quantity").remove();

        // Nom, prix et format du vin
        addToCartTitle.html(title);
        addToCartPrice.html(formatFloat(price) + " / " + litres + "cl");
        
        // Action sur le bouton d'ajout au panier
        addToCartButton.unbind().click(function(){
            addToCart(jQuery(this), articleId, title, price);
        });

        // Récupérer le vin dans le localStorage
        cartItem = cart[articleId] || null;
        quantityCart = 0;

        // Vin déjà dans le panier
        if(cartItem !== null){
            quantityCart = parseInt(cartItem.quantity);
        }
        
        // Vin pas encore dans le panier
        if(quantityCart === null){
            quantityCart = 0;
        }

        // Pas encore dans le panier -> masquer la quantité déjà présente
        if(quantityCart === 0){
            divAlreadyInCart.hide();
        }
        // Afficher la quantité déjà dans le panier
        else {
            divAlreadyInCart.find(".quantity-cart").html(quantityCart);
            divAlreadyInCart.show();
            
            // Livrable rapidement
            if(soonAvailable === 1 && maxQuantity < quantityCart){
                addToCartContainer.append(soonAvailableSpan.clone());
            }
        }
        
        // Input pour la quantité
        var input = jQuery("<input>").val("6").addClass("quantity")
            .attr("type", "number")
            .attr("min", "0")
            .keypress(function(event){
                return onlyInt(event, false);
            });

        if(soonAvailable === 1){
            input.attr("data-max", maxQuantity);
        }
        else {
            input.attr("max", maxQuantity);
        }
        
        addToCartContainer.prepend(input);

        // Afficher le dialog
        addToCartDialog.addClass("visible");
        
        // Empêche la redirection du <a>
        return false;
    }
</script>
{{-- Filtres --}}
<div id="filters-container">
    {{-- Icônes pour ouvrir / fermer les filtres --}}
    <div id="filters-icon">
        <div class="title">Filtres</div>
        <span class="top"></span>
        <span class="middle"></span>
        <span class="bottom"></span>
    </div>

    {{-- Filtres --}}
    <div id="filters">
        <div class="columns-container padding">
            @foreach($filtersArray as $column => $filters)
                <div class="filter column one-fourth">
                    @foreach($filters as $filter)
                        @isset($filter["title"])
                            <h4>{{ $filter["title"] }}</h4>
                        @endisset

                        @include('includes.filters.' . $filter["type"], $filter)
                    @endforeach
                </div>
            @endforeach
        </div>

        <div class="columns-container padding">
            <div class="column">
                <input type="button" onclick="redirect()" value="Appliquer"/>
                <input type="button" class="grey" value="Effacer"
                       onclick="document.location.href='{{ route($route) }}'"/>
            </div>
        </div>
            
    </div>

    {{-- Filtres sélectionnés --}}
    <div id="applied-filters">
        {{-- Filtres --}}
        @foreach($appliedFilters as $filter)
            <div class="applied-filter">
                @isset($filter["icon"])
                    <i class="{{ $filter["icon"] }}"></i>
                @endisset
                    
                @isset($filter["iconify"])
                    <span class="iconify" data-icon="{{ $filter["iconify"] }}"
                          data-inline="false"></span>
                @endisset
                
                {{ $filter["label"] }}
                <i class="fas fa-times" onclick="{{ $filter["function"] }}"></i>
            </div>
        @endforeach
    </div>
</div>

<script type="text/javascript">
    // Afficher / masquer les filtres
    jQuery("#filters-icon").click(function(){
        jQuery("#filters-icon").toggleClass("open");
        jQuery("#filters").toggleClass("visible");
    });

    // Choix d'un pays
    jQuery("#sel-country").change(function(){
        var countryId = jQuery(this).val();

        // Pas de pays -> masquer les régions
        if(countryId === "0"){
            jQuery("#sel-region").hide();
        }
        // Afficher les régions du pays
        else {
            jQuery("#sel-region option").hide();
            jQuery("#sel-region option[value='0']").show();
            jQuery("#sel-region option[data-country='" + countryId + "']").show();

            if(jQuery("#sel-region option[data-country='" + countryId + "']").length === 0){
                jQuery("#sel-region").hide();
            }
            else {
                jQuery("#sel-region").val("0");
                jQuery("#sel-region").show();
            }
        }
    });

    // Choix d'une couleur de vin
    jQuery(".filter .colour").click(function(){
        jQuery(".filter .colour").removeClass("selected");
        jQuery(this).addClass("selected");
    });

    
    /**
     * Supprimer le filtre d'un select
     */
    function resetSelect(id){
        jQuery("#sel-" + id).val(0);
        redirect();
    }


    /**
     * Supprimer le filtre d'une checkbox
     */
    function resetCheckbox(id){
        jQuery("input[type=checkbox]#" + id).prop("checked", false);
        redirect();
    }


    /**
     * Supprimer le filtre d'un champ textuel
     */
    function resetInput(id){
        jQuery("input[type=text]#" + id).val("");
        redirect();
    }


    /**
     * Supprimer le filtre de la couleur
     */
    function resetColour(){
        jQuery(".filter .colour").removeClass("selected");
        redirect();
    }


    /**
     * Supprimer le filtre du prix
     */
    function resetPrice(){
        jQuery("#min-price").html("0")
        jQuery("#max-price").html("0")
        redirect();
    }
</script>
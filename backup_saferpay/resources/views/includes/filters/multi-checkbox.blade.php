@foreach($options as $option)
    @php
    $checked = "";
    if(in_array($option->id, $filter["selectedValues"])){
        $checked = "checked";
    }
    @endphp

    <div class="checkbox">
        <input type="checkbox" {{ $checked }}
               class="{{ $filter["field"] }}" 
               id="{{ $filter["field"] }}{{ $option->id }}"
               value="{{ $option->id }}"/>
        <label for="{{ $filter["field"] }}{{ $option->id }}">
            {{ $option->name }}
        </label>
    </div>
@endforeach
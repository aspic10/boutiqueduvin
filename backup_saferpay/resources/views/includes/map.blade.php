@php
$countryMapId = uniqid();
$regionMapId = $countryMapId . "0";
@endphp

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class="maps-container">
    <div class="country-map" id="{{ $countryMapId }}"></div>
    <div class="region-map" id="{{ $regionMapId }}"></div>
</div>
       
<script type="text/javascript">
    google.charts.load("current", {
        packages:["geochart"],
        mapsApiKey: "AIzaSyDMa424ZUpM4MPNCxP44FaJ1loAZCSCHQg"
    });
    google.charts.setOnLoadCallback(drawRegionsMap);

    function drawRegionsMap() {
        // Pays
        var dataCountry = google.visualization.arrayToDataTable([
            ["Pays"],
            ["{{ $country }}"]
        ]);
        
        var optionsCountry = {
            region: "{{ $country }}",
            resolution: "country",
            datalessRegionColor: "transparent",
            enableRegionInteractivity: false,
            geochartVersion: 11,
            backgroundColor: "transparent",
            defaultColor: "#EAEAEA"
        };

        var chartCountry = new google.visualization.GeoChart(document.getElementById("{{ $countryMapId }}"));
        chartCountry.draw(dataCountry, optionsCountry);
        
        // Région
        @if(isset($region))
            var data = google.visualization.arrayToDataTable([
                ["Région"],
                ["{{ $region }}"]
            ]);

            var options = {
                region: "{{ $country }}",
                resolution: "provinces",
                datalessRegionColor: "transparent",
                enableRegionInteractivity: false,
                geochartVersion: 11,
                backgroundColor: "transparent",
                defaultColor: "#DB0A5B"
            };

            var chartRegion = new google.visualization.GeoChart(document.getElementById("{{ $regionMapId }}"));
            chartRegion.draw(data, options);
        @endif
    }
</script>
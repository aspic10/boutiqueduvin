<!-- Diaporama -->
<div id="picture-lightbox">
    <!-- Bouton pour fermer le diaporama -->
    <span class="close-lightbox"><i class="fas fa-times icon"></i></span>
    
    <!-- Boutons pour naviguer entre les photos -->
    <span class="previous-picture"><i class="fas fa-angle-left icon"></i></span>
    <span class="next-picture"><i class="fas fa-angle-right icon"></i></span>
    
    <!-- 
    Eléments qui contiennent les photos
    (deux éléments pour une transition fluide : on affiche chaque fois un des
    deux et le suivant sera affiché quand on clique sur "précédent" / "suivant")
    -->
    <div class="picture-container current right">
        <!-- Photo -->
        <img class="picture"/>
    </div>
    <div class="picture-container next right">
        <img class="picture"/>
    </div>
</div>

<script type="text/javascript">
    // Diaporama
    var lightbox = jQuery("#picture-lightbox");
    
    // Eléments à afficher alternativement si on navigue entre les photos
    var currentPictureContainer = lightbox.find(".picture-container.visible");
    var nextPictureContainer = lightbox.find(".picture-container.hidden");
    
    // Icônes pour fermer le diaporama et naviguer entre les photos
    var previousPictureIcon = lightbox.find(".previous-picture");
    var nextPictureIcon = lightbox.find(".next-picture");
    var closeLightboxIcon = lightbox.find(".close-lightbox");
    
    var previousPicture = null;
    var nextPicture = null;
    
    var container;
    
    /**
     * Afficher une photo dans le diaporama
     */
    function showPicture(currentPicture, cont = null){
        // Elément à afficher
        currentPictureContainer = lightbox.find(".picture-container.current");
        // Elément masqué pour la prochaine photo
        nextPictureContainer = lightbox.find(".picture-container.next");
        
        // Conteneur des photos
        if(cont === null){
            container = currentPicture.parents(".pictures-group");
        }
        
        // Index de la photo affichée
        var pictureId = currentPicture.data("id");
        
        // Index des photos précédente/suivante
        var lastId = container.find(".picture-container").length - 1;
        
        // Une seule image -> ne pas permettre la navigation
        if(lastId === 0){
            previousPictureIcon.hide();
            nextPictureIcon.hide();
        }
        // Plusieurs images -> navigation
        else {
            previousPictureIcon.show();
            nextPictureIcon.show();
            
            previousPictureId = pictureId - 1;
            nextPictureId = pictureId + 1;

            // C'est la première image
            if(pictureId === 0){
                // Elément précédent = dernière photo
                previousPictureId = lastId;
            }

            // C'est la dernière image
            if(pictureId === lastId){
                // Elément suivant = première photo
                nextPictureId = 0;
            }

            previousPicture = container.find(".picture-container[data-id='" + previousPictureId + "']");
            nextPicture = container.find(".picture-container[data-id='" + nextPictureId + "']");
        }
            
        // URL de la photo à afficher
        var url = currentPicture.data("url");
        currentPictureContainer.find(".picture").attr("src", url);
        
        // Délai pour que la transition soit nette
        setTimeout(function(){
            // Afficher la photo
            currentPictureContainer.addClass("visible");
            currentPictureContainer.removeClass("hidden right left");
            
            // Afficher le diaporama
            lightbox.addClass("visible");

            // Empêcher de scroller sur la page
            body.css("overflow", "hidden");
        }, 500);
    }
    
    
    /**
     * Afficher la photo précédente
     */
    function showPreviousPicture(){
        if(previousPicture !== null){
            // Faire entrer la photo suivante depuis la droite
            nextPictureContainer.removeClass("right next")
                .addClass("left current hidden");

            // Faire partir la photo actuelle à gauche
            currentPictureContainer.removeClass("left current visible")
                .addClass("right next");

            // Afficher la photo précédente
            showPicture(previousPicture, container);
        }
    }
    
    
    /**
     * Afficher la photo suivante
     */
    function showNextPicture(){
        if(nextPicture !== null){
            // Faire entrer la photo suivante depuis la gauche
            nextPictureContainer.removeClass("left next")
                .addClass("right current hidden");

            // Faire partir la photo actuelle à droite
            currentPictureContainer.removeClass("right current visible")
                .addClass("left next");

            // Afficher la photo suivante
            showPicture(nextPicture, container);
        }
    }
    
    
    // Clic sur la flèche pour afficher la photo précédente
    previousPictureIcon.click(function(e){
        e.stopPropagation();
        showPreviousPicture();
    });
    
    
    // Clic sur la flèche pour afficher la photo suivante
    nextPictureIcon.click(function(e){
        e.stopPropagation();
        showNextPicture();
    });
    
    // Afficher la photo suivante si on clique sur l'image
    lightbox.find(".picture").click(function(e){
        e.stopPropagation();
        showNextPicture();
    });
    
    // Fermer si on clique sur la croix
    closeLightboxIcon.click(function(e){
        e.stopPropagation();
        closeLightbox();
    });
    
    // Fermer si on clique sur la zone noire (modal)
    lightbox.click(function(){
        closeLightbox();
    });
    
    // Navigation avec le clavier
    jQuery(document).keyup(function(e) {
        // Fermer si on clique sur ESC
        if(e.keyCode === 27) {
            closeLightbox();
        }
        
        // Afficher la photo précédente/suivante si on clique sur les flèches
        if(e.keyCode === 37) {
            showPreviousPicture();
        }
        if(e.keyCode === 39) {
            showNextPicture();
        }
    });
    
    /**
     * Fermer le diaporama photo
     */
    function closeLightbox(){
        body.css("overflow", "auto");
        lightbox.removeClass("visible");
    }
</script>
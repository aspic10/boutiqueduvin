@php
$pictureGridId = uniqid();
@endphp

<!-- Container des images -->
<div class="pictures-grid pictures-group"
     id="{{ $pictureGridId }}"></div>


{{-- Script inclus une seule fois si plusieurs grids affichés sur la même page --}}
@once
    <script type="text/javascript">
        // Eléments qui varient si plusieurs grids affichés
        var pictures = {};
        var pictureGrid = {};
        var minWidth = {};

        // Options du grid
        var margin = 10;
        var containerWidth = 0;
        var nbColumns = 0;
        var margins = 0;
        var columnWidth = 0;
        var columns = [];
        var columnsIds = [];
        var minColumn = 0;
        var bottom = 0;
        var previousMax = 0;

        // Doubles photos
        var lastDouble = 200;
        var lastColumnDouble = null;
        var doubles = [];
        var minColumnsDouble = 2;

        // Redimensionnement de la page (ne pas calculer à chaque fois qu'on change,
        // mais chaque 0.2s pour ne pas lancer de fonction inutilement
        var rtime;
        var timeout = false;
        var delta = 200;
        jQuery(window).resize(function() {
            rtime = new Date();
            if(timeout === false) {
                timeout = true;
                setTimeout(resizeEnd, delta);
            }
        });
        // Fin du redimensionnement de la page
        function resizeEnd() {
            if(new Date() - rtime < delta) {
                setTimeout(resizeEnd, delta);
            } else {
                timeout = false;

                // Placer chaque image
                jQuery(".pictures-grid").each(function(){
                    let id = jQuery(this).attr("id");
                    
                    // Calculer les options du grid
                    setGridOptions(id);
                    
                    jQuery(this).find(".picture-container").each(function(){
                        placePicture(jQuery(this).data("id"), id);
                    });
                });
            }               
        }


        /**
         * Afficher les images
         */
        function displayPictures(id){
            for(var i in pictures[id]){
                var picture = pictures[id][i];

                var urlPicture = picture["filename"];
                var height = picture["height"];
                var width = picture["width"];

                var html = "<div class='picture-container' data-id='" + i + "' "
                            + "data-url='" + urlPicture + "' "
                            + "onclick='showPicture(jQuery(this))' "
                            + "style='width: " + width + "px; height: " + height + "px;'>"
                            + "<div class='picture' "
                                + "style='background-image: url(\"" + urlPicture + "\");'>"
                            + "</div>"
                        + "</div>";

                pictureGrid[id].append(html);

                placePicture(i, id);
            }
        }


        /**
         * Mise en place du grid
         */
        function setGridOptions(id) {
            // Largeur du container
            containerWidth = pictureGrid[id].width();

            // Nombre de colonnes à afficher
            nbColumns = parseInt(containerWidth / minWidth[id]);

            // Largeur des marges
            margins = (nbColumns - 1) * margin;

            // Largeur d'une colonne
            columnWidth = parseInt((containerWidth - margins) / nbColumns);

            // Initialiser la hauteur de chaque colonne
            for(var i = 0; i < nbColumns ; i ++) {
                columns[i] = 0;
                columnsIds[i] = -1;
            }

            // Colonne la plus haute où placer la prochaine image
            minColumn = 0;

            body.css("height", "initial");
            pictureGrid[id].css("height", "initial");
            previousMax = 0;
        }


        /**
         * Placer une image dans le grid
         */
        function placePicture(elementId, id){
            var element = pictureGrid[id].find(".picture-container[data-id='" + elementId + "']")

            // Dimensions actuelles
            var currentWidth = element.width();
            var currentHeight = element.height();

            // Différence de hauteur avec la colonne suivante pour voir si on peut
            // placer la photo sur deux colonnes
            var diffHeight = columns[(minColumn + 1)] - columns[minColumn];

            // Mettre une photo sur deux colonnes si
                // - Il y a plus de deux (par défaut) colonnes affichées
            if(nbColumns > minColumnsDouble 
                // - Ce n'est pas la dernière colonne
                && nbColumns !== (minColumn + 1)
                // - Il n'y a pas de photos doubles depuis 2 photos
                && lastDouble > 1 
                // - La dernière photo double n'était pas dans la même colonne
                && minColumn !== lastColumnDouble
                // - Les deux colonnes sont exactement à la même hauteur
                && (diffHeight === 0 
                    // - OU il y a un écart de moins de 40px entre les deux colonnes
                    // du dessus et il n'y a pas de photo double en dessus
                    || (diffHeight < 50
                        && !doubles.includes(columnsIds[minColumn]) 
                        && !doubles.includes(columnsIds[(minColumn+1)])))){
                lastDouble = 0;
                lastColumnDouble = minColumn;

                // Nouvelles dimensions
                var height = parseInt(currentHeight * (columnWidth * 2 + margin) / currentWidth);
                element.width(columnWidth * 2 + margin);
                element.height(height);

                // Placer au sommet de la colonne la plus haute
                element.css("top", columns[(minColumn + 1)]);

                // Mettre à jour la hauteur de la deuxième colonne
                columns[(minColumn + 1)] += height + margin;
                columns[minColumn] = columns[(minColumn + 1)];

                // Ajouter la photo dans les doubles
                doubles.push(elementId);

                // Modifier la hauteur de l'élément précédent pour qu'il n'y ait pas
                // de vide
                if(diffHeight > 0) {
                    pictureGrid[id].find(".picture-container[data-id='" + columnsIds[minColumn] + "]").height(pictureGrid[id].find(".picture-container[data-id='" + columnsIds[minColumn] + "]").height() + diffHeight);
                }

                // Mettre à jour le dernier ID dans la deuxième colonne
                columnsIds[(minColumn + 1)] = elementId;
            }
            // Une seule colonne
            else {
                // Photos depuis la dernière double
                lastDouble ++;

                // Si ça fait plus de 10 photos qu'il y a eu une double, on peut
                // mettre la prochaine dans la même colonne
                if(lastDouble > 10){
                    lastColumnDouble = null;
                }

                // Nouvelles dimensions
                var height = parseInt(currentHeight * columnWidth / currentWidth);
                element.width(columnWidth);
                element.height(height);

                // Placer au sommet de la colonne la plus haute
                element.css("top", columns[minColumn]);

                // Mettre à jour la hauteur de la colonne
                columns[minColumn] += height + margin;
            }

            // Mettre à jour le dernier ID dans la colonne
            columnsIds[minColumn] = elementId;

            // Mettre à la bonne distance
            var left = minColumn * columnWidth + (minColumn * margin);
            element.css("left", left);

            // Afficher avec un effet
            element.css("transform", "scale(1)");
            element.css("filter", "brightness(1)");

            // Colonnes les plus hautes et basses
            minColumn = 0;
            var min = columns[0];
            var max = columns[0];
            for(var i = 0; i < nbColumns ; i ++) {
                // Trouver la colonne la plus haute pour placer la prochaine image
                if(min > columns[i]){
                    min = columns[i];
                    minColumn = i;
                }
                // Trouver la colonne la plus basse pour agrandir le container
                if(max < columns[i]){
                    max = columns[i];
                }
            }

            // Agrandir le container
            if(previousMax < max){
                pictureGrid[id].height(pictureGrid[id].height() + max - previousMax);
                previousMax = max;
            }
        }
    </script>
@endonce

{{-- Script avec les photos et l'ID de chaque grid --}}
<script type="text/javascript">
    pictures["{{ $pictureGridId }}"] = <?php echo json_encode($pictures)?>;
    
    // Options du grid
    minWidth["{{ $pictureGridId }}"] = <?php echo isset($minWidth) ? $minWidth : 200 ?>;
    
    // Eléments HTML
    pictureGrid["{{ $pictureGridId }}"] = jQuery("#{{ $pictureGridId }}");
    
    document.addEventListener("DOMContentLoaded", function(){
        // Calculer les options du grid
        setGridOptions("{{ $pictureGridId }}");
        
        // Afficher les photos
        displayPictures("{{ $pictureGridId }}");
    });
</script>


@php
    $photo = $producer->photos->first();
    $city = $producer->city;
    $region = $producer->region;
    $countryIsoCode = $region->country->iso_code;
@endphp

<div class="producer-overview column"
     onclick="document.location.href='{{ route("producer", $producer->id) }}'">
    <div class="picture-container">
        @if(!is_null($photo))
            <div class="picture"
                 style="background-image: url('{{ config('app.photo_url') }}producer/{{ $producer->id }}/{{ $photo->name }}')"></div>
        @else
            <div class="picture"
                 style="background-image: url('/img/default-producer.jpg')"></div>
        @endif
    </div>
    <div class="title">
        <div class="name">{{ $producer->name }}</div>
        <div class="location">{{ $city->name }} / {{ $region->name }} ({{ $countryIsoCode }})</div>
    </div>
</div>
@extends('layouts.shop')
@section('headTitle', 'Commandes')

@section('content')
        {{-- Menu --}}
        @include('includes.client-menu', ['current' => 'clientBills'])

        <div class="column three-fourths last">
            <h2 class="no-margin-top">Commandes</h2>

            @if(count($bills) == 0)
                <p>Aucune commande</p>
            @else
                <table class="full-width">
                    <thead>
                        <tr>
                            <th class="center">Numéro</th>
                            <th class="center">Date</th>
                            <th class="center">Statut</th>
                            <th class="right">Total</th>
                            <th class="center">Aperçu</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($bills as $bill)
                            <tr>
                                <td class="center">{{ $bill->id }}</td>
                                <td class="center">{{ $bill->date->format("d.m.Y") }}</td>
                                <td>
                                    @if($bill->bills_status->open == false)
                                        Terminée
                                    @else
                                        En cours de traitement
                                    @endif
                                </td>
                                <td class="right">{{ number_format($bill->getBillTotal(), 2, ".", "'") }}</td>
                                <td class="center">
                                    <a href="{{ route("bill", $bill->id) }}">
                                        <i class="fas fa-search"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $bills->links() }}
            @endif
        </div>
    </div>
@endsection
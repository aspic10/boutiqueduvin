@extends('layouts.shop')
@section('headTitle', 'Suppression du compte')

@section('content')
        {{-- Menu --}}
        @include('includes.client-menu', ['current' => 'clientDelete'])

        <div class="column three-fourths last">
            <h2 class="no-margin-top">Supprimer mon compte</h2>
            <p>
                Si vous souhaitez supprimer votre compte, vous ne pourrez plus 
                consulter vos <span class="colour bold">commandes</span> et n'aurez 
                plus accès à vos <span class="colour bold">articles favoris</span>.
            </p>
            <p>
                Si des <span class="colour bold">bons cadeaux</span> sont liés à 
                votre compte, vous pourrez toujours les utiliser en saisissant leur 
                numéro et code PIN lors d'une prochaine commande. Veillez donc à 
                relever ces informations avant de supprimer votre compte.
            </p>
            <p>
                Votre compte et toutes vos données personnelles (nom, adresse, email 
                et téléphone) seront anonymisés sur notre système. Il ne vous sera 
                <span class="colour bold">plus possible de les récupérer</span>.
            </p>
            <p>
                Veuillez par contre noter que certaines données saisies lors d'anciennes
                commandes sont <span class="colour bold">conservées pour des raisons 
                fiscales ou d'archivage</span>. Elles ne seront en aucun 
                cas utilisées ou transmises pour d'autres motifs.
            </p>

            <div class="buttons-container-flow">
                <input type="button" value="Supprimer mon compte"
                       onclick="jQuery('#dialog-confirm-delete').addClass('visible')"/>
            </div>
        </div>
    </div>
    
    {{-- Dialog de confirmation de suppression --}}
    <div class="dialog" id="dialog-confirm-delete">
        <div class="dialog-container">
            <i class="fas fa-times close-dialog block"
               onclick="jQuery(this).parents('.dialog').removeClass('visible')"></i>
            <div class="dialog-content">
                <h2>Confirmation de suppression</h2>
                <p>Voulez-vous réellement supprimer votre compte ?</p>
                <p>Cette action est irréversible.</p>
                
                <div class="buttons-container-flow">
                    <input type="button" value="Annuler" class="grey"
                           onclick="jQuery('#dialog-confirm-delete').removeClass('visible')"/>
                    <input type="button" value="Supprimer mon compte"
                           onclick="deleteAccount()"/>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        /**
         * Supprimer le compte
         */
        function deleteAccount(){
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            jQuery.ajax({
                type: "POST",
                async: false,
                url: "{{ route('deleteAccount') }}",
                success: function(data){
                    // Vider le localStorage
                    localStorage.clear();
                    
                    // Rediriger vers l'accueil
                    document.location.href = "{{ route('home') }}";
                }
            });
        }
    </script>
@endsection
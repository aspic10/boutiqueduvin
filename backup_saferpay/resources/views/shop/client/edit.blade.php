@extends('layouts.shop')
@section('headTitle', 'Edition des coordonnées')

@section('content')
        {{-- Menu --}}
        @include('includes.client-menu', ['current' => 'account'])

        <div class="column three-fourths last">
            <h2 class="no-margin-top">Modifier mes coordonnées</h2>

            {{-- Adresse --}}
            <form id="address-form" class="form">
                <div class="row">
                    <label for="name">Nom complet *</label>
                    <div>
                        <input id="name" name="name" type="text" required 
                               value="{{ $client->name }}"/>
                        <span class="error">Champ obligatoire</span>
                    </div>
                </div>

                <div class="row">
                    <label for="company">Société</label>
                    <div>
                        <input id="company" name="company"
                               value="{{ $client->company_name }}"/>
                    </div>
                </div>

                <div class="row">
                    <label for="address">Adresse *</label>
                    <div>
                        <input id="address" name="address" type="text" required 
                               value="{{ $client->address }}"/>
                        <span class="error">Champ obligatoire</span>
                    </div>
                </div>

                <div class="row">
                    <label for="postal-code">NPA *</label>
                    <div>
                        <input id="postal-code" name="postal-code" type="text" 
                               required maxlength="4"
                               onkeypress="return onlyInt(event, false)"
                               value="{{ $client->city->postal_code }}"/>
                        <span class="error">NPA invalide</span>
                    </div>
                </div>

                <div class="row">
                    <label for="city">Localité *</label>
                    <div>
                        <input id="city" name="city" type="text" required 
                               value="{{ $client->city->name }}"/>
                        <span class="error">Champ obligatoire</span>
                    </div>
                </div>

                <div class="row">
                    <label for="city">Pays</label>
                    <div>
                        <input type="text" value="Suisse" disabled="">
                    </div>
                </div>

                <div class="row">
                    <label for="email">Email *</label>
                    <div>
                        <input id="email" name="email" type="email" required 
                               value="{{ $client->email }}"/>
                        <span class="error">Email invalide</span>
                    </div>
                </div>

                <div class="row">
                    <label for="phone">Téléphone</label>
                    <div>
                        <input id="phone" name="phone" type="phone" 
                               value="{{ $client->phone }}"/>
                    </div>
                </div>
            </form>

            <div class="buttons-container-flow">
                <a href="{{ route("account") }}">
                    <button class="grey">
                        Annuler
                    </button>
                </a>
                <input type="button" value="Enregistrer les changements"
                       onclick="jQuery('#address-form').submit()"/>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        const emailFormat = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        const postalCodeFormat = /\d{4}/;
        
        
        /**
         * Vérifier les informations et les enregistrer
         */
        jQuery("#address-form").submit(function(e) {
            e.preventDefault();
            
            var errors = 0;
            
            // Vérifier les champs requis et indiquer les erreurs
            jQuery("#address-form").find("input[required], textarea[required]").each(function(){
                if(jQuery(this).val().trim() === ""){
                    errors++;
                    jQuery(this).parents(".row").find("span.error").show();
                    jQuery(this).addClass("error");
                }
                else {
                    jQuery(this).parents(".row").find("span.error").hide();
                    jQuery(this).removeClass("error");
                }
            });
            
            // NPA
            var postalCodeInput = jQuery("#postal-code");

            if(postalCodeFormat.test(postalCodeInput.val().toLowerCase()) === false){
                errors++;
                postalCodeInput.parents(".row").find("span.error").show();
                postalCodeInput.addClass("error");
            }
            else {
                postalCodeInput.parents(".row").find("span.error").hide();
                postalCodeInput.removeClass("error");
            }
            
            // Adresse email
            var emailInput = jQuery("#email");

            if(emailFormat.test(emailInput.val().toLowerCase()) === false){
                errors++;
                emailInput.parents(".row").find("span.error").show();
                emailInput.addClass("error");
            }
            else {
                emailInput.parents(".row").find("span.error").hide();
                emailInput.removeClass("error");
            }
            
            // Pas d'erreur -> enregistrer les infos
            if(errors === 0){
                var data = jQuery("#address-form").serializeArray();
                var address = {};
                
                for(var i in data){
                    address[data[i].name] = data[i].value;
                }
                
                // Mettre à jour les coordonnées du client
                jQuery.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                jQuery.ajax({
                    type: "POST",
                    url: "{{ route('updateClient') }}",
                    data: {
                        address: address
                    },
                    success: function(){
                        // Rediriger vers la page d'accueil du compte
                        document.location.href = "{{ route('account') }}";
                    }
                });
            }
        });
    </script>
@endsection

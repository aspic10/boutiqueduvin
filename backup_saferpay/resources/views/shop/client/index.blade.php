@extends('layouts.shop')
@section('headTitle', 'Mon compte')

@section('content')
        {{-- Menu --}}
        @include('includes.client-menu', ['current' => 'account'])

        <div class="column three-fourths last">
            <h2 class="no-margin-top">Adresse</h2>
            <p>
                {!! $client->getFullAddress() !!}
            </p>

            <h2>Email</h2>
            <p>
                {{ $client->email }}
            </p>

            <h2>Téléphone</h2>
            <p>
                @if(!is_null($client->phone) && $client->phone !== "")
                    {{ $client->phone }}
                @else
                    -
                @endif
            </p>

            <div class="buttons-container-flow">
                <input type="button" value="Modifier mes coordonnées"
                       onclick="document.location.href='{{ route("clientEdit") }}'"/>
                <input type="button" value="Changer mon mot de passe"
                       onclick="document.location.href='{{ route("clientPassword") }}'"/>
            </div>
        </div>
    </div>
@endsection
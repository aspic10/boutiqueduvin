@extends('layouts.shop')

@section('headTitle', 'Contact')

@section('content')
    <h1>Contact et horaires</h1>
    <div class="columns-container">
        <div class="column one-third no-margin-top">
            <h2 class="no-margin-top">Adresse</h2>
            <p>
                Millésime 2012 / F et J Galloni SA
                <br/>
                Route de l'Usine 4
                <br/>
                1907 Saxon (Suisse)
                <br/>
                <a href="tel:+41273062138">027 / 306.21.38</a>
                <br/>
                <a href="mailto:contact@millesime2012.ch">contact@millesime2012.ch</a>
            </p>
                
            <div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2763.519767110648!2d7.197333915851788!3d46.160308194744495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478ec5bbfa730865%3A0xc28ee182a4d0c386!2sRte%20d&#39;Ec%C3%B4ne%20115%2C%201907%20Saxon!5e0!3m2!1sfr!2sch!4v1637943712698!5m2!1sfr!2sch" height="300" style="border:0; width: 100%;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
        <div class="column two-thirds last no-margin-top">
            <h2 class="no-margin-top">Formulaire de contact</h2>
            {{-- Success message --}}
            @if(Session::has("success"))
                <div class="alert success" style="margin-bottom: 10px;">
                    {{Session::get("success")}}
                </div>
            @endif
            
            {{-- Error message --}}
            @if(Session::has("error"))
                <div class="alert error" style="margin-bottom: 10px;">
                    {{Session::get("error")}}
                </div>
            @endif

            <form method="POST" action="{{ route("sendMailContact") }}">
                @csrf

                <div class="row">
                    <label for="name">Nom complet *</label>
                    <div>
                        <input id="name" type="text" class="@error('name') error @enderror"
                               name="name" value="{{ old('name') }}" 
                               required autocomplete="name" autofocus>

                        @error('name')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="email">Email *</label>
                    <div>
                        <input id="email" type="email" class="@error('email') error @enderror"
                               name="email" value="{{ old('email') }}"
                               required autocomplete="email">

                        @error('email')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="phone">Téléphone</label>
                    <div>
                        <input id="phone" type="phone" class="@error('phone') error @enderror"
                               name="phone" value="{{ old('phone') }}"
                               autocomplete="phone">

                        @error('phone')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="message">Message *</label>
                    <div>
                        <textarea id="message" type="message" class="full-width @error('message') error @enderror"
                               name="message" required>{{ old('message') }}</textarea>

                        @error('message')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>
                
                <div class="row">
                    <button type="submit">
                        Envoyer le message
                    </button>
                </div>
                
                {{-- Test robots --}}
                <div class="row" style="display: none;">
                    <label for="fax">Contact par fax</label>
                    <div>
                        <input id="fax" type="checkbox" name="fax" required="true"
                               checked="true">
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <h2>Horaires d'ouverture</h2>
    {!! $hours !!}
    
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("#fax").prop("checked", false);
            jQuery("#fax").prop("required", false);
        });
    </script>
@endsection
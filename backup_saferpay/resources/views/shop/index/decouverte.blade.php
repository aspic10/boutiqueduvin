@extends('layouts.shop')

@section('headTitle', 'Espace Découverte')

@section('content')
    <h1>Espace découverte</h1>
    <p>
        Tous les mois, nous vous proposons une nouvelle sélection de grands vins
        à découvrir. Il s'agit généralement de vins italiens, mais des spécialités
        valaisannes, espagnoles ou internationales viennent parfois agrémenter
        cette sélection.
    </p>
    
    <h2>Sélection actuelle</h2>
    
    @if(is_null($enomatic))
        <p>
            Sélection indisponible actuellement
        </p>
    @else
        <div class="columns-container">
            {{-- Aperçu de chaque vin --}}
            @foreach($enomatic->wines as $wine)
                @include('includes.wine-overview', ["client" => Auth::user(), "onlyVisible" => false])
            @endforeach
        </div>
    @endif
@endsection
@extends('layouts.shop')

@section('headTitle', $producer->name)

@php
$city = $producer->city;
$region = $producer->region;
$country = $region->country;
@endphp

@section('content')
    {{-- Inclure la vue partielle pour le diaporama --}}
    @include('includes.pictures-diaporama')
    
    <h1>{{ $producer->name }}</h1>
    
    {{-- Producteur --}}
        <div class="producer-detail">
            <div class="columns-container">
                {{-- Nom et description --}}
                <div class="column one-half">
                    <div class="list-detail">
                        {{-- Localité --}}
                        <div class="detail">
                            <i class="fas fa-globe-americas icon"
                               title="Localité"></i>
                            {{ $city->name }} / {{ $region->name }} / {{ $country->name }}
                        </div>
                        {{-- Site Internet --}}
                        @if(!is_null($producer->website))
                            <div class="detail">
                                <i class="fas fa-link icon"
                                   title="Site Internet"></i>
                                <a href="{{ $producer->website }}" target="blank">
                                    {{-- rtrim(str_replace(["http://", "https://"], "", $producer->website), "/") --}}
                                    Site Internet
                                </a>
                            </div>
                        @endif
                    </div>
                    <p>
                        {!! str_replace("\n", "</p><p>", $producer->description) !!}
                    </p>

                    {{-- Carte --}}
                    @if(!is_null($region) && !is_null($region->iso_code))
                        @include('includes.map', ['country' => $region->country->iso_code, 'region' => $region->iso_code])
                    @endif
                </div>

                {{-- Photos --}}
                <div class="column one-half">
                    @if(count($producerPhotos) > 0)
                        @include('includes.pictures-grid', ['pictures' => $producerPhotos])
                    @endif
                </div>
            </div>
            
            {{-- Autre vins de la cave --}}
            @if(count($producerWines) > 0) 
                <h2>Vins de la cave</h2>
                <div class="wines-container">
                    {{-- Détail de chaque vin --}}
                    <div class="columns-container">
                        @foreach($producerWines as $wine)
                            @include('includes.wine-overview', ["client" => Auth::user(), "onlyVisible" => true])
                        @endforeach
                    </div>
                    
                    <a href="{{ route("wines", [
                                "producer" => $producer->id
                            ]) }}">
                        <button id="load-more">
                            Voir tous les vins de la cave
                        </button>
                    </a>
                </div>
            @endif
        </div>
@endsection
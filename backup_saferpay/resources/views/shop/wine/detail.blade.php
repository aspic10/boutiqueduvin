@extends('layouts.shop')
@section('headTitle', $wine)

@php
    $client = Auth::user();

    $article = $wine->article;
    $name = $wine->__toString();
    
    $visible = $article->isVisible();
    
    $photo = $article->photos->first();
    $region = $wine->region;
    $varieties = $wine->varieties;
    $distinctions = $wine->distinctions;
    $characteristics = $wine->characteristics;
    $shopPrice = $article->getShopPrice();
    $producer = $article->producer;
    $description = $article->description;
    $favourite = $article->favourite;
    
    // Quantité limitée
    if(!is_null($article->limited_quantity)){
        $soonAvailable = false;
        $maxQuantity = $article->limited_quantity;
    }
    // Quantité non limitée
    else {
        $soonAvailable = $article->soon_available;
        $maxQuantity = $article->quantity;
    }
@endphp

@section('content')
    {{-- Inclure la vue partielle pour le diaporama --}}
    @include('includes.pictures-diaporama')
    
    {{-- Inclure le dialog pour ajouter un article au panier --}}
    @include('includes.dialog.add-to-cart')

    <h1>
        {{-- Favori --}}
        @if(!is_null($client))
            @php
            if($client->articles->contains($article)){
                $class = "yes";
                $title = "Retirer des favoris";
            }
            else {
                $class = "";
                $title = "Ajouter aux favoris";
            }
            @endphp
            
            <div class="favourite {{ $class }}" title="{{ $title }}"
                 data-id="{{ $wine->id }}">
                <i class="fas fa-bookmark"></i>
                <i class="fas fa-star"></i>
            </div>
        @endif
        {{ $name }}
    </h1>
    <div class="wine-detail columns-container">
        @if(!is_null($photo))
            <div class="pictures-group column one-fourth">
                <div class="wine-picture-container"
                     data-id="0" 
                     data-url="{{ config('app.photo_url') }}article/{{ $wine->id }}/{{ $photo->name }}"
                     onclick="showPicture(jQuery(this))">
                    @if($favourite === true)
                        <span class="heart"></span>
                    @endif
                    
                    <img src="{{ config('app.photo_url') }}article/{{ $wine->id }}/{{ $photo->name }}"/>
                </div>
            </div>
            <div class="column three-fourths last">
        @else
            <div class="column">
        @endif
                <div id="price-container">
                    @if($visible === true)
                        <div class="price">
                            {{ number_format($shopPrice, 2, ".", "'") }}
                            /
                            {{ $article->litres }}cl
                        </div>

                        <div class="cart-container">
                            {{-- Article indisponible --}}
                            @if($soonAvailable === false && $maxQuantity == 0)
                                <div class="inform-availability">
                                    <div class="info">
                                        Cet article est actuellement indisponible
                                    </div>

                                    {{-- Informer quand l'article est à nouveau disponible --}}
                                    <p>M'avertir en cas de disponibilité :</p>

                                    {{-- Success message --}}
                                    @if(Session::has("success"))
                                        <div class="alert success">
                                            {{Session::get("success")}}
                                        </div>
                                    @else
                                        {{-- Error message --}}
                                        @if(Session::has("error"))
                                            <div class="alert error" style="margin-bottom: 10px;">
                                                {{Session::get("error")}}
                                            </div>
                                        @endif

                                        <form method="POST" action="{{ route("available") }}">
                                            @csrf

                                            <input type="hidden" name="wine"
                                                   value="{{ $wine->article->code . " : " . $wine}}"/>

                                            <span class="email-container">
                                                <input type="email" name="email" required
                                                       class="@error('email') error @enderror"
                                                       value="{{ old('email') }}"
                                                       autocomplete="email"
                                                       placeholder="Adresse email"/>

                                                @error('email')
                                                    <span class="error">
                                                        {{ $message }}
                                                    </span>
                                                @enderror
                                            </span>
                                            <span class="animated-button" 
                                                  onclick="jQuery(this).parents('form').submit()">
                                                <i class="far fa-paper-plane"></i>
                                                Envoyer
                                            </span>
                                        </form>
                                    @endif
                                </div>
                            {{-- Article disponible ou livrable rapidement --}}
                            @else
                                <input type="number" class="quantity"
                                       @if($soonAvailable == true)
                                           data-max="{{ $maxQuantity }}"
                                       @else
                                           max="{{ $maxQuantity }}"
                                       @endif
                                       onkeypress="return onlyInt(event, false)"
                                       min="1" value="6"/>
                                <span class="animated-button" 
                                      onclick="addMainWineToCart(jQuery(this), {{ $article->id }}, '{{ addslashes($name) }}', {{ $shopPrice }})">
                                    <i class="fas fa-cart-plus"></i>
                                    Ajouter au panier
                                </span>

                                <div class="already-in-cart">
                                    Déjà <span class="quantity-cart"></span> dans le panier
                                </div>
                            @endif
                        </div>
                    @else
                        <div class="cart-container">
                            <div class="inform-availability">
                                <div class="info">
                                    Cet article n'est pas en vente en ligne actuellement
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="list-detail columns-container">
                    <div class="column two-thirds">
                        {{-- Producteur --}}
                        @if(!is_null($producer))
                            <div class="detail">
                                <i class="far fa-user icon"
                                   title="Producteur"></i>
                                <a href="{{ route("producer", $producer->id) }}">
                                    {{ $producer->name }}
                                </a>
                            </div>
                        @endif

                        {{-- Appellation --}}
                        @if(!is_null($wine->appellation))
                            <div class="detail">
                                <i class="fas fa-certificate icon"
                                   title="Appellation"></i>
                                {{ $wine->appellation->name }}
                            </div>
                        @endif

                        {{-- Région --}}
                        @if(!is_null($region))
                            <div class="detail">
                                <i class="fas fa-globe-americas icon"
                                   title="Région"></i>
                                {{ $region->name }} / {{ $region->country->name }}
                            </div>
                        @endif

                        {{-- Millésime --}}
                        @if(!is_null($wine->year))
                            <div class="detail">
                                <i class="far fa-calendar-alt icon"
                                   title="Millésime"></i>
                                {{ $wine->year }}
                            </div>
                        @endif

                        {{-- Couleur --}}
                        <div class="detail">
                            <i class="fas fa-wine-glass-alt icon"
                               title="Couleur"></i>
                            {{ $wine->colour->name }}
                        </div>

                        {{-- Cépages --}}
                        @if(count($varieties) > 0)
                            <div class="detail">
                                <span class="iconify icon" data-icon="vs-grapes"
                                      title="Cépages"></span>
                                <ul>
                                    @foreach($varieties as $variety)
                                        <li>
                                            {{ $variety->name }}
                                            @if(!is_null($variety->pivot->percentage) 
                                                && $variety->pivot->percentage > 0)
                                                {{ $variety->pivot->percentage }}%
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        {{-- Caractéristiques --}}
                        @if(count($characteristics) > 0)
                            <div class="detail">
                                <h3>Caractéristiques</h3>
                                <i class="fas fa-tag icon"></i>
                                <ul>
                                    @foreach($characteristics as $characteristic)
                                        <li>
                                            {{ $characteristic->name }}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        {{-- Vin primé --}}
                        @if(count($distinctions) > 0)
                            <div class="detail">
                                <h3>Distinctions</h3>
                                <i class="fas fa-award icon"></i>
                                <ul>
                                    @foreach($distinctions as $distinction)
                                        <li>
                                            <span class="bold">
                                                {{ $distinction->name }} :
                                            </span>
                                            {{ $distinction->pivot->rating }}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        {{-- Description --}}
                        @if(!is_null($description) && $description != "")
                            <div class="detail">
                                <h3>Quelques infos intéressantes</h3>
                                <i class="far fa-lightbulb icon"
                                   title="Description"></i>
                                {!! nl2br($description) !!}
                            </div>
                        @endif

                        {{-- Coup de coeur --}}
                        @if($wine->favourite == true)
                            <i class="far fa-heart favourite"
                               title="Coup de coeur"></i>
                        @endif

                        {{-- Rareté --}}
                        @if(!is_null($wine->limitedQuantity))
                            <i class="far fa-gem rare"
                               title="Vin rare"></i>
                        @endif

    <!--                    {{-- Promotion --}}
                        @if($wine->favourite == true)
                            <i class="fas fa-percent sale"
                               title="En action"></i>
                        @endif-->
                    </div>
                    <div class="column one-third last">
                        {{-- Carte --}}
                        @if(!is_null($region) && !is_null($region->iso_code))
                            @include('includes.map', ['country' => $region->country->iso_code, 'region' => $region->iso_code])
                        @endif
                    </div>
                </div>
            </div>
        
        {{-- Producteur --}}
        @if(!is_null($producer))
            <div class="producer-detail">
                <div class="columns-container">
                    {{-- Nom et description --}}
                    @if(count($producerPhotos) > 0)
                        <div class="column one-half">
                    @else
                        <div>
                    @endif
                            <h2>{{ $producer->name }}</h2>
                            <p>
                                {!! str_replace("\n", "</p><p>", $producer->description) !!}
                            </p>
                        </div>

                    {{-- Photos --}}
                    @if(count($producerPhotos) > 0)
                        <div class="column one-half last">
                            @include('includes.pictures-grid', ['pictures' => $producerPhotos])
                        </div>
                    @endif
                </div>

                {{-- Autre vins de la cave --}}
                @if(count($producerWines) > 0) 
                    <h3>Autres vins de la cave</h3>
                    <div class="wines-container">
                        {{-- Détail de chaque vin --}}
                        <div class="columns-container">
                            @foreach($producerWines as $wine)
                                @include('includes.wine-overview', ["client" => $client, "onlyVisible" => true])
                            @endforeach
                        </div>

                        <a href="{{ route("wines", [
                            "producer" => $producer->id
                        ]) }}">
                            <button id="load-more">
                                Voir tous les vins de la cave
                            </button>
                        </a>
                    </div>
                @endif
            </div>
        @endif
    </div>
    
    <script type="text/javascript">
        /**
         * Chargement de la page
         */
        document.addEventListener('DOMContentLoaded', function(){
            var divAlreadyInCart = jQuery(".wine-detail .already-in-cart");
            
            // Récupérer le vin dans le localStorage
            var cartItem = cart[{{ $article->id }}] || null;
            quantityCart = 0;
            
            // Vin déjà dans le panier
            if(cartItem !== null){
                quantityCart = parseInt(cartItem.quantity);
            }
            
            // Vin pas encore dans le panier
            if(quantityCart === null){
                quantityCart = 0;
            }
            
            // Pas encore dans le panier -> masquer la quantité déjà présente
            if(quantityCart === 0){
                divAlreadyInCart.hide();
            }
            // Afficher la quantité déjà dans le panier
            else {
                divAlreadyInCart.find(".quantity-cart").html(quantityCart);
                divAlreadyInCart.show();
                
                // Livrable rapidement
                if({{ (int) $soonAvailable }} === 1 && {{ $maxQuantity }} < quantityCart){
                    jQuery(".wine-detail .cart-container").append(soonAvailableSpan.clone());
                }
            }
        });
        
        
        /**
         * Ajouter le vin au panier
         */
        function addMainWineToCart(element, articleId, name, price){
            quantityCart = parseInt(jQuery(".wine-detail .already-in-cart .quantity-cart").html()) || 0;
            
            addToCart(element, articleId, name, price);
        }
    </script>
@endsection
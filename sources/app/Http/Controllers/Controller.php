<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    
    /**
     * Récupérer les infos d'une photo pour l'afficher dans une galerie
     */
    static function getPhotoInfo($filename, $photosArray){
        $size = getimagesize($filename);

        $photosArray[] = array(
            "filename" => $filename,
            "width" => $size[0],
            "height" => $size[1],
        );
        
        return $photosArray;
    }
}
<?php
/**
 * Contrôleur pour le panier et le check-out
 */
namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Wine;
use App\Models\Bill;
use App\Models\BillsArticle;
use App\Models\BillsVat;
use App\Models\BillsTax;
use App\Models\BillsStatus;
use App\Models\ClientsArticle;
use App\Models\Company;
use App\Models\Client;
use App\Models\ClientsType;
use App\Models\ClientsOrder;
use App\Models\ClientsOrdersArticle;
use App\Models\ClientsOrdersStatus;
use App\Models\City;
use App\Models\Tax;
use App\Models\PaymentsTerm;
use App\Models\GiftCard;
use App\Models\SiteText;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CartController extends Controller {
    /**
     * Login
     */
    public function login() {
        $client = Auth::user();

        // Connexion
        if(is_null($client)){
            return view('auth.login', ["guest" => true]);
        }
        // Utilisateur connecté -> choix de la livraison
        elseif($client->hasVerifiedEmail()){
            return redirect()->route('shipping');
        }
        // Vérification de l'email nécessaire
        else {
            return redirect()->route('verification.notice');
        }
    }
    
    
    /**
     * Page de sélection du moyen de paiement
     */
    public function payment(){
        $client = Auth::user();
        
        // Client connecté
        if(!is_null($client)){
            // Récupérer les bons cadeaux du client
            $params = $client->getGiftCards();
        }
        else {
            $params = array();
        }
        
        return view('shop.cart.payment', $params);
    }
    
    
    /**
     * Page de validation de la commande
     */
    public function validateOrder(int $retry = 0){
        // Texte des CGV
        $terms = SiteText::where("label", "cgv")
            ->firstOr(function(){
                return new SiteText();
            });
        
        return view('shop.cart.validate', [
            "terms" => $terms->content,
            "retry" => $retry,
        ]);
    }
    
    
    /**
     * Vérifier le panier d'achat
     */
    public function getCartInfo(Request $request) {
        $cart = $request->post("cart");
        
        $info = array();
        
        $totalCart = 0;
        $nbBottles = 0;
        
        if(!is_null($cart)){
            foreach($cart as $articleId => $cartItem){
                if($cartItem["type"] == "wine"){
                    $wine = Wine::find($articleId);
                    $article = $wine->article;

                    // Article visible
                    if($article->isVisible()){
                        $name = $wine->__toString();
                        $price = $article->getShopPrice();

                        // Quantité limitée
                        if(!is_null($article->limited_quantity)){
                            $soonAvailable = false;
                            $maxQuantity = $article->limited_quantity;
                        }
                        // Quantité non limitée
                        else {
                            $soonAvailable = $article->soon_available;
                            $maxQuantity = $article->quantity;
                        }

                        // Article modifié depuis l'ajout au panier
                        $nameChanged = false;
                        $priceChanged = false;
                        $quantityChanged = false;

                        // Nom modifié
                        if($name !== $cartItem["name"]) {
                            $nameChanged = true;
                            $cartItem["name"] = $name;
                        }

                        // Prix modifié
                        if($price !== floatval($cartItem["price"])) {
                            $priceChanged = true;
                            $cartItem["price"] = $price;
                        }

                        // Nombre en stock insuffisant
                        if($maxQuantity < $cartItem["quantity"]){
                            // Pas livrable rapidement -> adapter la quantité
                            if($soonAvailable === false){
                                $quantityChanged = true;
                                $cartItem["quantity"] = $maxQuantity;
                            }
                        }

                        if($cartItem["quantity"] == 0){
                            unset($cart[$articleId]);
                        }
                        else {
                            $cart[$articleId] = $cartItem;

                            $info[$articleId] = array(
                                "nameChanged" => $nameChanged,
                                "priceChanged" => $priceChanged,
                                "soonAvailable" => $soonAvailable,
                                "quantityChanged" => $quantityChanged,
                                "maxQuantity" => $maxQuantity,
                                "format" => $article->litres . "cl",
                            );

                            // Totaux
                            $totalCart += round($cartItem["quantity"] * $cartItem["price"] * 20) / 20;
                            $nbBottles += $cartItem["quantity"];
                        }
                    }
                    // Supprimer du panier
                    else {
                        unset($cart[$articleId]);
                    }
                }
                elseif($cartItem["type"] == "gift") {
                    $info[$articleId] = array(
                        "nameChanged" => false,
                        "priceChanged" => false,
                        "soonAvailable" => false,
                        "quantityChanged" => false,
                        "maxQuantity" => null,
                        "format" => "-",
                    );

                    // Totaux
                    $totalCart += round($cartItem["quantity"] * $cartItem["price"] * 20) / 20;
                }
            }
        }
        
        return array(
            "cart" => $cart,
            "info" => $info,
            "total" => $totalCart,
            "bottles" => $nbBottles,
        );
    }
    
    
    /**
     * Retourner les frais de livraison
     */
    public function getDeliveryCost(Request $request) {
        $nbBottles = $request->post("nbBottles");
        $total = $request->post("total");
        
        return $this->deliveryCost($nbBottles, $total);
    }
    
    
    /**
     * Calculer les frais de livraison
     */
    private function deliveryCost($nbBottles, $total){
        if($total < 200){
            // Nombre de cartons de 12 bouteilles
            $nbBoxes12 = (int)($nbBottles / 12);
            // Nombre de cartons de 6 bouteilles
            $nbBoxes6 = 0;
            // Bouteilles restantes
            $modulo12 = $nbBottles % 12;

            if($modulo12 > 6){
                $nbBoxes12++;
            }
            elseif($modulo12 > 0){
                $nbBoxes6 = 1;
            }

            $delivery = $nbBoxes12 * 17 + $nbBoxes6 * 12;
        }
        else {
            $delivery = 0;
        }
        
        return $delivery;
    }
    
    
    /**
     * Retourner le montant disponible d'une carte cadeau
     */
    public function getGiftCard(Request $request) {
        $number = $request->post("number");
        $pin = $request->post("pin");
        
        return $this->giftCard($number, $pin);
    }
    
    
    /**
     * Récupérer le montant disponible d'une carte cadeau
     */
    private function giftCard($number, $pin){
        $amount = GiftCard::where([
            ["number", strtolower($number)],
            ["pin", strtolower($pin)],
            ])
            ->value("amount");
        
        return floatval($amount);
    }
    
    
    /**
     * Ajouter la carte au compte client
     */
    public function addGiftCard(Request $request){
        $number = $request->post("number");
        $pin = $request->post("pin");
        
        $giftCard = GiftCard::where([
            ["number", strtolower($number)],
            ["pin", strtolower($pin)],
        ])->first();
        
        if(!is_null($giftCard)){
            // Ajouter la carte au compte client
            $client = Auth::user();
            if(!is_null($client)) {
                $giftCard->client_id = $client->id;
                $giftCard->save();
            }
        }
    }
    
    
    /**
     * Modalité de paiement par défaut
     */
    private function getDefaultPaymentTerm(){
        $paymentTerm = PaymentsTerm::where("name", "like", "Anticipé")
            ->first();
        
        // Créer si introuvable
        if(is_null($paymentTerm)){
            $paymentTerm = new PaymentsTerm();
            $paymentTerm->name = "Anticipé";
            $paymentTerm->days = 0;
            $paymentTerm->blocked = false;
            $paymentTerm->archived = false;
            $paymentTerm->save();
        }
        
        return $paymentTerm;
    }
    
    
    /**
     * Statut de la facture par défaut
     */
    private function getDefaultBillStatus(){
        $name = "Ouverte";
        
        $billStatus = BillsStatus::where("name", "like", $name)
            ->first();
        
        // Créer si introuvable
        if(is_null($billStatus)){
            $billStatus = $this->createBillStatus($name, true);
        }
        
        return $billStatus;
    }
    
    
    /**
     * Créer le statut de la facture s'il n'existe pas
     */
    private function createBillStatus($name, $open){
        $billStatus = new BillsStatus();
        $billStatus->name = $name;
        $billStatus->open = $open;
        $billStatus->save();
        
        return $billStatus;
    }
    
    
    /**
     * Statut de la commande par défaut
     */
    private function getDefaultOrderStatus(){
        $orderStatus = ClientsOrdersStatus::where("name", "like", "Prête pour chargement")
            ->first();
        
        // Créer si introuvable
        if(is_null($orderStatus)){
            $orderStatus = new ClientsOrdersStatus();
            $orderStatus->name = "Prête pour chargement";
            $orderStatus->save();
        }
        
        return $orderStatus;
    }
    
    
    /**
     * Client par défaut
     */
    private function getDefaultCompany(){
        $companyGuest = Company::where([
                ["code", "like", "SHOP-GUEST"],
                ["type", "like", "client"],
            ])
            ->first();

        // Créer si introuvable
        if(is_null($companyGuest)){
            $paymentTerm = $this->getDefaultPaymentTerm();
            
            $companyGuest = new Company();
            $companyGuest->code = "SHOP-GUEST";
            $companyGuest->name = "Invité Shop";
            $companyGuest->type = "client";
            $companyGuest->payment_term_id = $paymentTerm->id;
            $companyGuest->city_id = City::where([
                    ["name", "like", "Saxon"],
                    ["postal_code", "like", "1907"],
                ])
                ->value("id");
            $companyGuest->save();

            $clientGuest = new Client();
            $clientGuest->id = $companyGuest->id;
            $clientGuest->client_type_id = ClientsType::where("name", "like", "Shop")
                ->value("id");
            $clientGuest->visible = false;
            $clientGuest->save();
        }
        
        return $companyGuest;
    }
    
    
    /**
     * Ajouter une commande
     */
    public function addOrder(Request $request) {
        $formPayment = null;
        
        // Nouvelle facture
        $bill = new Bill();
        
        // Nouvelle commande
        $order = new ClientsOrder();
        
        // Modalité de paiement
        $paymentTerm = $this->getDefaultPaymentTerm();
        $bill->payment_term_id = $paymentTerm->id;
        
        // Statut de la facture
        $billStatus = $this->getDefaultBillStatus();
        $bill->bill_status_id = $billStatus->id;
        
        // Statut de la commande
        $orderStatus = $this->getDefaultOrderStatus();
        $order->client_order_status_id = $orderStatus->id;
        
        // Année fiscale pour la table des articles
        $date = new \DateTime();
        $year = (int) $date->format("Y");
        $month = (int) $date->format("n");
        if($month >= 10){
            $year ++;
        }
        
        $bill->date = $date;
        $bill->due_date = $date;
        $bill->year = $year;
        
        $order->date = $date;
        $order->validation_date = $date;
        
        // Erreurs lors de la création de la commande
        $error = false;
        // Page vers laquelle le client est redirigé
        $redirect = "confirm";
        $redirectParams = array();
        
        // Informations du localStorage
        $cart = $request->post("cart");
        $checkout = $request->post("checkout");
        $totalCart = floatval($request->post("totalCart"));
        $totalToPay = floatval($request->post("totalToPay"));
        $nbBottles = (int) $request->post("nbBottles");
        $retryIndicator = (int) $request->post("retryIndicator");
        
        // Adresse
        $addressInfo = $checkout["address"];
        
        // Retrait à Saxon
        $pickup = isset($checkout["pickup"]) ? $checkout["pickup"] : false;
        
        // Informations du client
        $client = Auth::user();

        // Client non connecté
        if(is_null($client)){
            $companyGuest = $this->getDefaultCompany();
            $clientId = $companyGuest->id;
            $clientName = $checkout["client"]["name"];
            $clientEmail = $checkout["client"]["email"];
            $clientPhone = isset($checkout["client"]["phone"]) ? $checkout["client"]["phone"] : "";
        }
        // Client connecté
        else {
            $clientId = $client->id;
            $clientName = $client->name;
            $clientEmail = $client->email;
            $clientPhone = $client->phone;
        }
        
        $bill->client_id = $clientId;
        $order->client_id = $clientId;
        
        // Vérifier le panier sur le serveur (quantités, prix, etc.)
        $cartInfo = $this->getCartInfo($request);
        
        // Panier modifié
        if($cart != $cartInfo["cart"]){
            $error = true;
            $redirect = "validate";
        }
        else {
            // Nombre de bouteilles
            if($nbBottles != $cartInfo["bottles"]){
                $error = true;
                $redirect = "validate";
            }
            else {
                // Calcul des frais de livraison
                if($pickup === "true"){
                    $delivery = 0;
                }
                else {
                    $delivery = $this->deliveryCost($nbBottles, $cartInfo["total"]);
                }
                
                // Total de la commande
                if(round($totalCart, 2) != round($cartInfo["total"] + $delivery, 2)){
                    $error = true;
                    $redirect = "validate";
                }
                else {
                    $remark = "<b>Paiement :</b> ";
                    
                    // Cartes cadeaux
                    $giftCards = isset($checkout["giftCards"]) ? $checkout["giftCards"] : array();
                    $totalToPayCheck = $totalCart;
                    $nbGiftCards = 0;
                    
                    foreach($giftCards as $number => $giftCard){
                        $amount = $this->giftCard($number, $giftCard["pin"]);
                        
                        if($amount != $giftCard["amount"]){
                            $error = true;
                            $redirect = "validate";
                        }
                        else {
                            $totalToPayCheck -= $amount;
                            $nbGiftCards ++;
                        }
                    }
                    
                    if($nbGiftCards > 0){
                        if($nbGiftCards > 1){
                            $remark .= "bons cadeaux";
                        }
                        else {
                            $remark .= "bon cadeau";
                        }
                    }
                    
                    // Solde à payer
                    if($totalToPayCheck < 0){
                        $totalToPayCheck = 0;
                    }
                    
                    if(round($totalToPayCheck, 2) != round($totalToPay, 2)){
                        $error = true;
                        $redirect = "validate";
                    }
                    else {
                        // Adresse de facturation identique
                        $sameAddress = $addressInfo["same-address"];

                        // Adresse
                        $addressName = isset($addressInfo["name"]) ? trim($addressInfo["name"]) : "";
                        $addressCompany = isset($addressInfo["company"]) ? trim($addressInfo["company"]) : "";
                        $addressAddress = isset($addressInfo["address"]) ? trim($addressInfo["address"]) : "";
                        $addressCity = isset($addressInfo["city"]) ? trim($addressInfo["city"]) : "";
                        $addressPostalCode = isset($addressInfo["postal-code"]) ? trim($addressInfo["postal-code"]) : "";
                        $addressPhone = isset($addressInfo["phone"]) ? trim($addressInfo["phone"]) : "";
                        $billingAddress = isset($addressInfo["billing-address"]) ? trim($addressInfo["billing-address"]) : "";

                        $deliveryAddress = "";

                        // Vérifier les champs obligatoires
                        if( // Nom et email du client
                            $clientName === "" || $clientEmail === ""
                            // Pas de retrait à Saxon -> adresse complète obligatoire
                            || ($pickup !== "true" 
                                && ($addressName === "" || $addressAddress === "" || $addressCity === ""
                                    || $addressPostalCode === "" || $addressPhone === ""))
                            // Adresse de facturation différente
                            || ($sameAddress === "false" && $billingAddress === "")
                            ) {
                            // Champ manquant -> retour sur la page de saisie des coordonnées
                            $error = true;
                            $redirect = "shipping";
                        }
                        else {
                            // Adresse de livraison
                            if($pickup !== "true"){
                                if($addressCompany !== ""){
                                    $deliveryAddress .= $addressCompany . "\n";
                                }

                                $deliveryAddress .= $addressName . "\n"
                                    . $addressAddress . "\n"
                                    . $addressPostalCode . " " . $addressCity;
                            }
                            // Retrait à Saxon
                            else {
                                $deliveryAddress = "retrait à Saxon";
                            }
                            
                            // Adresse de facturation identique
                            if($sameAddress === "true"){
                                // Livraison
                                if($pickup !== "true"){
                                    $billingAddress = $deliveryAddress;
                                    $deliveryAddress .= "\n" . $addressPhone;
                                }
                                // Retrait à Saxon
                                else {
                                    $billingAddress = $clientName;
                                }
                            }
                            // Autre adresse de facturation
                            else {
                                if($pickup !== "true"){
                                    $deliveryAddress .= "\n" . $addressPhone;
                                }
                            }
                            
                            // Solde à payer par carte / paiement anticipé
                            if($totalToPay >= 0.05){
                                if($nbGiftCards > 0){
                                    $remark .= " + ";
                                }
                                
                                if($checkout["payment"] === "prepayment"){
                                    $remark .= "paiement anticipé";
                                }
                            }
                            
                            // Informations sur la commande
                            $remark .= "\n<b>Livraison :</b> " . str_replace("\n", " - ", $deliveryAddress);
                            
                            if(isset($checkout["gift"]) && $checkout["gift"] == "true"){
                                $remark .= "\n<b>Cadeau :</b> oui";
                                
                                if(isset($checkout["giftMessage"]) && $checkout["giftMessage"] != ""){
                                    $remark .= "\n<b>Message au destinataire :</b> " . $checkout["giftMessage"];
                                }
                            }
                            
                            if(isset($checkout["remark"])){
                                $remark .= "\n<b>Remarque :</b> " . $checkout["remark"];
                            }
                            
                            $internalRemark = "<b>Coordonnées de l'acheteur :</b> " 
                                . $clientName . " - " . $clientEmail;
                            
                            if(!is_null($clientPhone) && $clientPhone !== ""){
                                $internalRemark .= " - " . $clientPhone;
                            }
                            
                            $bill->address = $billingAddress;
                            $bill->remark = $remark;
                            $bill->internal_remark = $internalRemark;
                            
                            // Enregistrer la facture
                            $bill->save();
                            
                            $redirectParams["billId"] = $bill->id;
                            
                            // Enregistrer la commande
                            $orderRemark = "Facture n° " . $bill->id . "\n" . $remark;
                            
                            $order->remark = $orderRemark;
                            $order->save();
                            
                            $total = 0;
                            $totalBuying = 0;
                            $subtotal = 0;
                            $nbGiftCardsBought = 0;
                            $balanceLabel = "";
                            $balanceAmount = 0;
                            
                            // Ajouter les articles à la facture et à la commande
                            foreach($cart as $articleId => $cartItem){
                                // Vin
                                if($cartItem["type"] == "wine"){
                                    // Informations de l'article
                                    $wine = Wine::find($articleId);
                                    $article = $wine->article;
                                    $price = $cartItem["price"];
                                    $quantity = round($cartItem["quantity"], 3);
                                    $vat = $article->vat;
                                    $vatValue = $vat->value;

                                    // Mettre à jour le stock
                                    $article->quantity = $article->quantity - $quantity;
                                    if(!is_null($article->limited_quantity)){
                                        $article->limited_quantity = $article->limited_quantity - $quantity;
                                    }
                                    $article->save();

                                    // Nouvel article de la commande
                                    $orderArticle = new ClientsOrdersArticle();
                                    $orderArticle->article_id = $article->id;
                                    $orderArticle->client_order_id = $order->id;
                                    $orderArticle->quantity = $quantity;
                                    $orderArticle->inserted_date = $date;
                                    $orderArticle->save();

                                    // Déduire la TVA
                                    $priceNoVat = round($price * 100 / (100 + $vatValue), 2);

                                    // Nouvel article de la facture
                                    $billArticle = new BillsArticle();
                                    $billArticle->setTable("bills_articles_" . $year);
                                    $billArticle->bill_id = $bill->id;
                                    $billArticle->article_id = $article->id;
                                    $billArticle->code = $article->code;
                                    $billArticle->name = $cartItem["name"];
                                    $billArticle->format = $article->format;
                                    $billArticle->quantity = $quantity;
                                    $billArticle->price = $priceNoVat;
                                    $billArticle->vat = $vatValue;
                                    $billArticle->vat_code = $vat->code;
                                    $billArticle->sale = false;
                                    $billArticle->no_discount = false;
                                    $billArticle->buying_price = $article->buying_price;

                                    // Quantité limitée
                                    if(is_null($article->limited_quantity) 
                                        && $article->quantity < $quantity
                                        && $article->soon_available === true){
                                        $billArticle->remark = "Délai de livraison : 2 semaines";
                                    }

                                    // Nombre de cartons
                                    if(!is_null($article->weight)){
                                        $boxes = (int) ($quantity / $article->weight);
                                    }
                                    else {
                                        $boxes = (int) ($quantity / $article->number);
                                    }

                                    $billArticle->boxes = $boxes;

                                    $billArticle->save();

                                    // Prix total de l'article
                                    $articlePrice = round($quantity * $priceNoVat / 0.05) * 0.05;

                                    // Mettre à jour le sous-total avant remise et taxes
                                    $subtotal += $articlePrice;

                                    // TVA
                                    $billVat = BillsVat::where([
                                            ["bill_id", "=", $bill->id],
                                            ["vat", "=", $vatValue],
                                        ])
                                        ->first();

                                    if(is_null($billVat)){
                                        $billVat = new BillsVat();

                                        $billVat->bill_id = $bill->id;
                                        $billVat->vat = $vatValue;
                                        $billVat->amount = $articlePrice;
                                    }
                                    else {
                                        $billVat->amount = $billVat->amount + $articlePrice;
                                    }

                                    $billVat->save();

                                    // Ajouter la TVA
                                    $articlePrice += $articlePrice * $vatValue / 100;

                                    // Mettre à jour le prix total de la facture
                                    $total += $articlePrice;

                                    // Prix d'achat total de l'article
                                    $articleBuyingPrice = $quantity * $article->buying_price;

                                    // Mettre à jour le prix d'achat total de la facture
                                    $totalBuying += $articleBuyingPrice;

                                    // Diminuer les stocks
                                    $article->quantity = round($article->quantity - $quantity, 3);

                                    // Dernière vente de l'article
                                    $article->last_sale_date = $date;

                                    // Ajouter l'article aux favoris du client
                                    if(!is_null($client)){
                                        $clientArticle = ClientsArticle::where([
                                            ["client_id", "=", $clientId],
                                            ["article_id", "=", $articleId],
                                        ])
                                        ->first();

                                        if(is_null($clientArticle)){
                                            $clientArticle = new ClientsArticle();
                                            $clientArticle->client_id = $clientId;
                                            $clientArticle->article_id = $articleId;
                                            $clientArticle->quantity = "";
                                            $clientArticle->save();
                                        }
                                    }
                                }
                                // Bon cadeau
                                elseif($cartItem["type"] == "gift") {
                                    // Quantité > 1 -> plusieurs cartes du même montant
                                    for($i = 0; $i < $cartItem["quantity"]; $i++){
                                        $nbGiftCardsBought++;
                                        
                                        // Créer la carte cadeau
                                        $giftCard = new GiftCard();
                                        $giftCard->amount = $cartItem["price"];
                                        $giftCard->initial_amount = $cartItem["price"];
                                        $giftCard->date = $date;
                                        $giftCard->bill_id = $bill->id;
                                        
                                        // Numéro de la carte (8 chiffres)
                                        do {
                                            $number = mt_rand(10000000, 99999999);
                                            
                                            // Le numéro de carte est unique
                                            if(GiftCard::where('number', $number)->count() == 0){
                                                $giftCard->number = $number;
                                            }
                                        }
                                        while(is_null($giftCard->number));
                                        
                                        // Code PIN (4 caractères)
                                        $pin = "";
                                        
                                        for($j = 0; $j < 4; $j++){
                                            if(rand(0,1) == 0){
                                                // Lettres A-Z
                                                $pin .= chr(rand(65, 90));
                                            }
                                            else {
                                                // Chiffres de 0-9
                                                $pin .= rand(0, 9);
                                            }
                                        }
                                        
                                        $giftCard->pin = $pin;
                                        
                                        $giftCard->save();

                                        // Ajouter au balance_label
                                        if($nbGiftCardsBought > 1){
                                            $balanceLabel .= " / ";
                                        }
                                        
                                        $balanceLabel .= $number . " (" . $cartItem["price"] . ".-)";

                                        // Ajouter au balance_amount
                                        $balanceAmount += $cartItem["price"];
                                    }
                                }
                            }
                            
                            // Mettre à jour les totaux
                            $bill->total = $total;
                            $bill->total_buying = $totalBuying;
                            $bill->subtotal = $subtotal;
                            
                            if($nbGiftCardsBought > 0){
                                $bill->balance_amount = $balanceAmount;
                                
                                if($nbGiftCardsBought > 1){
                                    $balanceLabel = "Bons cadeaux : " . $balanceLabel;
                                }
                                else {
                                    $balanceLabel = "Bon cadeau : " . $balanceLabel;
                                }
                                
                                $bill->balance_label = $balanceLabel;
                                
                                $order->remark = $order->remark . "\n" . $balanceLabel;
                                $order->save();
                            }
                            
                            // Frais de livraison
                            if($delivery > 0){
                                $tax = Tax::where("name", "LIKE", "Frais de livraison")
                                    ->first();

                                if(is_null($tax)){
                                    $tax = new Tax();
                                    $tax->name = "Frais de livraison";
                                    $tax->vat_id = 2;
                                    $tax->price = 1;
                                    $tax->archived = false;
                                    $tax->save();
                                }

                                $taxVatValue = $tax->vat->value;

                                // Déduire la TVA
                                $deliveryNoVat = round($delivery * 100 / (100 + $taxVatValue), 2);

                                // Nouvelle taxe de la facture
                                $billTax = new BillsTax();

                                $billTax->tax_id = $tax->id;
                                $billTax->bill_id = $bill->id;
                                $billTax->quantity = $deliveryNoVat;
                                $billTax->price = 1;
                                $billTax->vat = $taxVatValue;

                                $billTax->save();

                                // TVA
                                $taxBillVat = BillsVat::where([
                                    ["bill_id", "=", $bill->id],
                                    ["vat", "=", $taxVatValue],
                                ])
                                ->first();

                                if(is_null($taxBillVat)){
                                    $taxBillVat = new BillsVat();

                                    $taxBillVat->bill_id = $bill->id;
                                    $taxBillVat->vat = $taxVatValue;
                                    $taxBillVat->amount = $deliveryNoVat;
                                }
                                else {
                                    $taxBillVat->amount = $taxBillVat->amount + $deliveryNoVat;
                                }

                                $taxBillVat->save();

                                // Mettre à jour le total des taxes de la facture
                                $bill->total_taxes = $deliveryNoVat + ($deliveryNoVat * $taxVatValue / 100);
                            }
                                
                            $bill->save();
                            
                            // Utiliser les bons cadeaux
                            foreach($giftCards as $number => $giftCardInfo){
                                $giftCard = GiftCard::where([
                                        ["number", strtolower($number)],
                                        ["pin", strtolower($giftCardInfo["pin"])],
                                    ])
                                    ->first();

                                $amount = $giftCard->amount;
                                $availableAmount = $amount;

                                // Total < carte cadeau -> déduire de la carte le solde
                                if($totalCart < $amount){
                                    $amount = $totalCart;
                                    $totalCart = 0;

                                    $availableAmount -= $amount;
                                }
                                // Utiliser la carte complète
                                else {
                                    $totalCart -= $availableAmount;
                                    $availableAmount = 0;
                                }
                                
                                // Nouveau solde
                                if($amount > 0){
                                    $giftCard->bills()->attach($bill, ["amount" => $amount]);
                                    $giftCard->amount = $availableAmount;
                                    $giftCard->save();
                                }
                            }
                            
                            // Paiement anticipé -> envoyer l'email de confirmation
                            try {
                                Mail::to($clientEmail)->send(new \App\Mail\OrderConfirm($bill));
                            } catch (\Exception $ex) {}
                        }
                    }
                }
            }
        }
        
        return array(
            "error" => $error,
            "redirect" => route($redirect, $redirectParams),
            "formPayment" => $formPayment,
        );
    }
    
    
    /**
     * Statut du paiement par carte
     */
    public function paymentStatus($status, int $billId, int $orderId = 0, int $retry = 0){
        $bill = Bill::find($billId);
        
        // Paiement réussi
        if($status === "success") {
            $url = route("confirm", $billId);
            $icon = "far fa-check-circle";
            $colour = "green";
            $label = "Paiement réussi";
            $clear = true;
        }
        // Paiement échoué ou annulé
        else {
            $clear = false;
            
            // Supprimer la commande
            $order = ClientsOrder::find($orderId);
            $order->delete();
            
            // Remettre les articles en stock
            foreach($bill->articles as $article){
                $billArticle = $article->pivot;
                $quantity = $billArticle->quantity;
                
                $article->quantity = $article->quantity + $quantity;
                if(!is_null($article->limited_quantity)){
                    $article->limited_quantity = $article->limited_quantity + $quantity;
                }
                $article->save();
            }
            
            // Supprimer la facture
            $bill->delete();
            
            // Paiement échoué
            if($status === "fail") {
                $url = route("validate", $retry);
                $icon = "fas fa-ban";
                $colour = "red";
                $label = "Une erreur est survenue";
            }
            // Paiement annulé
            else {
                $url = route("validate");
                $icon = "fas fa-ban";
                $colour = "orange";
                $label = "Paiement annulé";
            }
        }
        
        return view('shop.cart.payment-status', [
            "url" => $url,
            "icon" => $icon,
            "colour" => $colour,
            "label" => $label,
            "clear" => $clear,
        ]);
    }
    
    
    /**
     * Paiement par carte de crédit réussi
     */
    public function paymentNotify(int $billId, $email){
        $bill = Bill::find($billId);
        
        // Email de confirmation
        try {
            Mail::to($email)->send(new \App\Mail\OrderConfirm($bill, false));
        } catch (\Exception $ex) {}
        
        // Changer le statut de la facture et indiquer que le paiement est reçu
        $billStatus = $this->getDefaultBillStatus();
        $bill->bill_status_id = $billStatus->id;
        $bill->internal_remark = $bill->internal_remark . "\nPaiement reçu à " 
            . date("H\hi");
                            
        // Enregistrer la facture
        $bill->save();
    }
}
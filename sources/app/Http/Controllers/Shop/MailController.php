<?php
/**
 * Contrôleur pour l'envoi d'emails
 */
namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Rules\ReCaptcha;

class MailController extends Controller {
    private $to;
    
    public function __construct(){
        $this->to = config("mail.to");
    }   
    
    /**
     * Contact -> envoi de l'email
     */
    public function contact(Request $request){
        // Validation
        $this->validate($request, [
            "name" => "required",
            "email" => "required|email",
            "message" => "required",
        ]);
        
        try {
            // Vérification anti-spam :
            // - case fax cachée ne doit pas être cochée
            // - au moins trois mots dans le message
            // - expéditeur bloqué
            if(!$request->fax 
                && str_word_count($request->message) > 2
                && $request->name != "Roberttix"){
                // Envoi de l'email
                Mail::to($this->to)
                    ->send(new \App\Mail\Contact($request->name, $request->email, $request->phone, $request->message));

                // Envoi de la copie à l'expéditeur
                Mail::to($request->email)
                    ->send(new \App\Mail\ContactConfirm($request->message));
            }
            
            return back()->with("success", "Votre message nous est bien parvenu !");
        } catch (\Exception $ex) {
            return back()->with("error", "Une erreur est survenue. Veuillez vérifier les informations saisies.")
                ->withInput();
        }
    }
    
    
    /**
     * Informer de la disponibilité d'un vin
     */
    public function available(Request $request){
        // Validation
        $this->validate($request, [
            "email" => "required|email",
        ]);
        
        try {
            // Envoi de l'email
            Mail::to($this->to)
                ->send(new \App\Mail\Available($request->email, $request->wine));
            
            return back()->with("success", "Demande transmise !");
        } catch (\Exception $ex) {
            return back()->with("error", "Une erreur est survenue")
                ->withInput();
        }
    }
}
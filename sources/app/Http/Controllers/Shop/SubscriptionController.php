<?php
/**
 * Contrôleur pour les abonnements
 */
namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Wine;

class SubscriptionController extends Controller {
    public function index() {
        $wines = Wine::take(4)
            ->get();
        return view('shop.subscription.index', ["wines" => $wines]);
    }
}
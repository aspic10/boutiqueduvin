<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable {
    use Queueable,
        SerializesModels;

    // Coordonnées
    public $name;
    public $email;
    public $phone;
    
    // Message
    public $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $phone, $content) {
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject("Contact Shop")
            ->replyTo($this->email)
            ->view('mail.contact');
    }
}
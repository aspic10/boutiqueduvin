<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Bill;

class OrderConfirm extends Mailable {
    use Queueable,
        SerializesModels;

    // Facture
    public $bill;
    
    // Paiement anticipé
    public $prepayment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Bill $bill, bool $prepayment = true) {
        $this->bill = $bill;
        $this->prepayment = $prepayment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject("Confirmation de commande")
            ->view('mail.order-confirm');
    }
}
<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Article
 * 
 * @property int $id
 * @property int $category_id
 * @property int $vat_id
 * @property int $container_id
 * @property string $code
 * @property string $name
 * @property bool $no_discount
 * @property string $format
 * @property float $quantity
 * @property bool $price_list
 * @property string $description
 * @property float $weight
 * @property int $number
 * @property string $type
 * @property float $buying_price
 * @property float $price
 * @property int $brand_id
 * @property Carbon $last_sale_date
 * @property bool $archived
 * @property float $alcohol_volume
 * @property int $location_id
 * @property string $remark
 * @property int $rank
 * @property float $litres
 * @property bool $shop
 * @property int $limited_quantity
 * @property bool $soon_available
 * @property bool $favourite
 * @property int $producer_id
 * @property float $increment
 * @property bool $visible
 * 
 * @property Category $category
 * @property Brand $brand
 * @property Producer $producer
 * @property Vat $vat
 * @property Container $container
 * @property Location $location
 * @property Collection|Packaging[] $packagings
 * @property Collection|Remark[] $remarks
 * @property Collection|Tax[] $taxes
 * @property Collection|Bill[] $bills
 * @property Collection|Client[] $clients
 * @property Collection|Client[] $clients_prices
 * @property Collection|ClientsOrder[] $clients_orders
 * @property Drink $drink
 * @property Collection|InventoriesArticle[] $inventories_articles
 * @property Collection|Photo[] $photos
 * @property Collection|Price[] $prices
 * @property Collection|Sale[] $sales
 * @property Collection|Supplier[] $suppliers
 * @property Collection|SuppliersBill[] $suppliers_bills
 * @property Collection|SuppliersOrder[] $suppliers_orders
 * @property Wine $wine
 *
 * @package App\Models
 */
class Article extends Model
{
	protected $table = 'articles';
	public $timestamps = false;

	protected $casts = [
		'category_id' => 'int',
		'vat_id' => 'int',
		'container_id' => 'int',
		'no_discount' => 'bool',
		'quantity' => 'float',
		'price_list' => 'bool',
		'weight' => 'float',
		'number' => 'int',
		'buying_price' => 'float',
		'price' => 'float',
		'brand_id' => 'int',
		'archived' => 'bool',
		'alcohol_volume' => 'float',
		'location_id' => 'int',
		'rank' => 'int',
		'litres' => 'float',
		'shop' => 'bool',
		'limited_quantity' => 'int',
		'soon_available' => 'bool',
		'favourite' => 'bool',
		'producer_id' => 'int',
		'increment' => 'float',
		'visible' => 'bool'
	];

	protected $dates = [
		'last_sale_date'
	];

	protected $fillable = [
		'category_id',
		'vat_id',
		'container_id',
		'code',
		'name',
		'no_discount',
		'format',
		'quantity',
		'price_list',
		'description',
		'weight',
		'number',
		'type',
		'buying_price',
		'price',
		'brand_id',
		'last_sale_date',
		'archived',
		'alcohol_volume',
		'location_id',
		'remark',
		'rank',
		'litres',
		'shop',
		'limited_quantity',
		'soon_available',
		'favourite',
		'producer_id',
		'increment',
		'visible'
	];

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function brand()
	{
		return $this->belongsTo(Brand::class);
	}

	public function producer()
	{
		return $this->belongsTo(Producer::class);
	}

	public function vat()
	{
		return $this->belongsTo(Vat::class);
	}

	public function container()
	{
		return $this->belongsTo(Container::class);
	}

	public function location()
	{
		return $this->belongsTo(Location::class);
	}

	public function packagings()
	{
		return $this->belongsToMany(Packaging::class, 'articles_packagings');
	}

	public function remarks()
	{
		return $this->belongsToMany(Remark::class, 'articles_remarks');
	}

	public function taxes()
	{
		return $this->belongsToMany(Tax::class, 'articles_taxes')
					->withPivot('quantity');
	}

	public function bills()
	{
		return $this->belongsToMany(Bill::class, 'bills_articles_2020')
					->withPivot('id', 'code', 'name', 'format', 'quantity', 'price', 'vat', 'vat_code', 'sale', 'no_discount', 'remark', 'boxes', 'last', 'buying_price');
	}

	public function clients()
	{
            return $this->belongsToMany(Client::class, 'clients_articles')
                ->withPivot('rank', 'quantity');
	}
        
	public function clients_prices()
	{
		return $this->belongsToMany(Client::class, 'clients_articles_prices')
					->withPivot('id', 'from', 'to', 'price', 'fixed');
	}

	public function clients_orders()
	{
		return $this->belongsToMany(ClientsOrder::class, 'clients_orders_articles', 'article_id', 'client_order_id')
					->withPivot('quantity', 'quantity_delivered', 'inserted_date');
	}

	public function drink()
	{
		return $this->hasOne(Drink::class, 'id');
	}

	public function inventories_articles()
	{
		return $this->hasMany(InventoriesArticle::class);
	}

	public function photos()
	{
		return $this->hasMany(Photo::class);
	}

	public function prices()
	{
		return $this->hasMany(Price::class);
	}

	public function sales()
	{
		return $this->belongsToMany(Sale::class, 'sales_articles')
					->withPivot('sale_category_id', 'price', 'rank', 'name', 'photo');
	}

	public function suppliers()
	{
		return $this->belongsToMany(Supplier::class, 'suppliers_articles')
					->withPivot('id', 'price', 'reference', 'supplier_article_category_id', 'rank', 'suggested_price', 'refund');
	}

	public function suppliers_bills()
	{
		return $this->belongsToMany(SuppliersBill::class, 'suppliers_bills_articles', 'article_id', 'supplier_bill_id')
					->withPivot('id', 'reference', 'quantity', 'price', 'vat', 'sale', 'remark');
	}

	public function suppliers_orders()
	{
		return $this->belongsToMany(SuppliersOrder::class, 'suppliers_orders_articles', 'article_id', 'supplier_order_id')
					->withPivot('quantity', 'quantity_received');
	}

	public function wine()
	{
		return $this->hasOne(Wine::class, 'id', 'id');
	}
        
        
    /**************************************************************************/
    /* FUNCTIONS                                                              */
    /**************************************************************************/
    /**
     * To string
     */
    public function __toString() {
        $name = $this->name;
        
        // Volume d'alcool
        if(!is_null($this->alcoholVolume) && $this->alcoholVolume > 0){
            $name .= " " . $this->alcoholVolume . "°";
        }
        
        // Ajouter le nom de la marque
        if(!is_null($this->brand)){
            // Marque avant le nom de l'article
            if($this->brand->after == false){
                $toString = $this->brand . " " . $name;
            }
            // Marque après le nom de l'article
            else {
                $toString = $name . " " . $this->brand;
            }
        }
        // Pas de marque
        else {
            $toString = $name;
        }
        
        return str_replace(array("\r", "\n"), '', $toString);
    }
    
    
    /**
     * Récupérer le prix de vente sur le Shop
     */
    public function getShopPrice() {
        $shopPrice = null;
        
        foreach($this->prices as $price){
            $clientType = $price->clients_type;
            
            if($clientType->name == "Shop"){
                $shopPrice = $price->price;
                break;
            }
        }
        
        return $shopPrice;
    }
    
    
    /**
     * Article visible sur le Shop ou non
     */
    public function isVisible(){
        $visible = true;
        
        // Case "Vendu sur le Shop"
        if($this->shop == false
            // Pas de prix Shop
            || is_null($this->getShopPrice())
            // Article archivé
            || $this->archived == true){
            $visible = false;
        }
        
        return $visible;
    }
}

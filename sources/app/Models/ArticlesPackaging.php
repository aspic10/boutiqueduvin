<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticlesPackaging
 * 
 * @property int $article_id
 * @property int $packaging_id
 * 
 * @property Packaging $packaging
 * @property Article $article
 *
 * @package App\Models
 */
class ArticlesPackaging extends Model
{
	protected $table = 'articles_packagings';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'article_id' => 'int',
		'packaging_id' => 'int'
	];

	public function packaging()
	{
		return $this->belongsTo(Packaging::class);
	}

	public function article()
	{
		return $this->belongsTo(Article::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticlesRemark
 * 
 * @property int $article_id
 * @property int $remark_id
 * 
 * @property Article $article
 * @property Remark $remark
 *
 * @package App\Models
 */
class ArticlesRemark extends Model
{
	protected $table = 'articles_remarks';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'article_id' => 'int',
		'remark_id' => 'int'
	];

	public function article()
	{
		return $this->belongsTo(Article::class);
	}

	public function remark()
	{
		return $this->belongsTo(Remark::class);
	}
}

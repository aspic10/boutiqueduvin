<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BillsArticle
 * 
 * @property int $id
 * @property int $bill_id
 * @property int $article_id
 * @property string $code
 * @property string $name
 * @property string $format
 * @property float $quantity
 * @property float $price
 * @property float $vat
 * @property string $vat_code
 * @property bool $sale
 * @property bool $no_discount
 * @property string $remark
 * @property int $boxes
 * @property bool $last
 * @property float $buying_price
 * 
 * @property Bill $bill
 * @property Article $article
 *
 * @package App\Models
 */
class BillsArticle extends Model
{
	protected $table = 'bills_articles';
	public $timestamps = false;

	protected $casts = [
		'bill_id' => 'int',
		'article_id' => 'int',
		'quantity' => 'float',
		'price' => 'float',
		'vat' => 'float',
		'sale' => 'bool',
		'no_discount' => 'bool',
		'boxes' => 'int',
		'last' => 'bool',
		'buying_price' => 'float'
	];

	protected $fillable = [
		'bill_id',
		'article_id',
		'code',
		'name',
		'format',
		'quantity',
		'price',
		'vat',
		'vat_code',
		'sale',
		'no_discount',
		'remark',
		'boxes',
		'last',
		'buying_price'
	];

	public function bill()
	{
		return $this->belongsTo(Bill::class);
	}

	public function article()
	{
		return $this->belongsTo(Article::class);
	}
}

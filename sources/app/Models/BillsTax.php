<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BillsTax
 * 
 * @property int $bill_id
 * @property int $tax_id
 * @property float $price
 * @property float $quantity
 * @property float $vat
 * 
 * @property Bill $bill
 * @property Tax $tax
 *
 * @package App\Models
 */
class BillsTax extends Model
{
	protected $table = 'bills_taxes';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'bill_id' => 'int',
		'tax_id' => 'int',
		'price' => 'float',
		'quantity' => 'float',
		'vat' => 'float'
	];

	protected $fillable = [
		'price',
		'quantity',
		'vat'
	];

	public function bill()
	{
		return $this->belongsTo(Bill::class);
	}

	public function tax()
	{
		return $this->belongsTo(Tax::class);
	}
}

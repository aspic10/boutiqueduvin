<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 * 
 * @property int $id
 * @property string $name
 * @property int $after
 * 
 * @property Collection|Article[] $articles
 *
 * @package App\Models
 */
class Brand extends Model
{
	protected $table = 'brands';
	public $timestamps = false;

	protected $casts = [
		'after' => 'int'
	];

	protected $fillable = [
		'name',
		'after'
	];

	public function articles()
	{
		return $this->hasMany(Article::class);
	}
}

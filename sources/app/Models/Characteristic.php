<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Characteristic
 * 
 * @property int $id
 * @property string $name
 * 
 * @property Collection|Wine[] $wines
 *
 * @package App\Models
 */
class Characteristic extends Model
{
	protected $table = 'characteristics';
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function wines()
	{
		return $this->belongsToMany(Wine::class, 'wines_characteristics');
	}
}

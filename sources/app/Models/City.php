<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class City
 * 
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property string $postal_code
 * 
 * @property Country $country
 * @property Collection|Company[] $companies
 * @property Collection|Producer[] $producers
 *
 * @package App\Models
 */
class City extends Model
{
	protected $table = 'cities';
	public $timestamps = false;

	protected $casts = [
		'country_id' => 'int'
	];

	protected $fillable = [
		'country_id',
		'name',
		'postal_code'
	];

	public function country()
	{
		return $this->belongsTo(Country::class);
	}

	public function companies()
	{
		return $this->hasMany(Company::class);
	}

	public function producers()
	{
		return $this->hasMany(Producer::class);
	}
        
    /**************************************************************************/
    /* FUNCTIONS                                                              */
    /**************************************************************************/
    /**
     * Nom complet
     */
    public function __toString() {
        $result = $this->postal_code . " " . $this->name;
        
        // Indiquer le pays si ce n'est pas la Suisse
        if($this->country->iso_code != "CH"){
            $result .= " (" . $this->country->iso_code . ")";
        }
        
        return $result;
    }
}

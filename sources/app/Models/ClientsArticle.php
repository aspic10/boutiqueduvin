<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientsArticle
 * 
 * @property int $client_id
 * @property int $article_id
 * @property int $rank
 * @property string $quantity
 * 
 * @property Client $client
 * @property Article $article
 *
 * @package App\Models
 */
class ClientsArticle extends Model
{
	protected $table = 'clients_articles';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'client_id' => 'int',
		'article_id' => 'int',
		'rank' => 'int'
	];

	protected $fillable = [
		'rank',
		'quantity'
	];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function article()
	{
		return $this->belongsTo(Article::class);
	}
}

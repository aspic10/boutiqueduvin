<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientsDay
 * 
 * @property int $client_id
 * @property int $day_id
 * @property string $hours
 * 
 * @property Client $client
 * @property Day $day
 *
 * @package App\Models
 */
class ClientsDay extends Model
{
	protected $table = 'clients_days';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'client_id' => 'int',
		'day_id' => 'int'
	];

	protected $fillable = [
		'hours'
	];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function day()
	{
		return $this->belongsTo(Day::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientsOrder
 * 
 * @property int $id
 * @property int $client_id
 * @property int $client_order_status_id
 * @property Carbon $date
 * @property string $remark
 * @property bool $modified
 * @property Carbon $validation_date
 * @property int $user_id
 * 
 * @property Client $client
 * @property ClientsOrdersStatus $clients_orders_status
 * @property User $user
 * @property Collection|Bill[] $bills
 * @property Collection|Article[] $articles
 *
 * @package App\Models
 */
class ClientsOrder extends Model
{
	protected $table = 'clients_orders';
	public $timestamps = false;

	protected $casts = [
		'client_id' => 'int',
		'client_order_status_id' => 'int',
		'modified' => 'bool',
		'user_id' => 'int'
	];

	protected $dates = [
		'date',
		'validation_date'
	];

	protected $fillable = [
		'client_id',
		'client_order_status_id',
		'date',
		'remark',
		'modified',
		'validation_date',
		'user_id'
	];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function clients_orders_status()
	{
		return $this->belongsTo(ClientsOrdersStatus::class, 'client_order_status_id');
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function bills()
	{
		return $this->hasMany(Bill::class, 'client_order_id');
	}

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'clients_orders_articles', 'client_order_id')
					->withPivot('quantity', 'quantity_delivered', 'inserted_date');
	}
}

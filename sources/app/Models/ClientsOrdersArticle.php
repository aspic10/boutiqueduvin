<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientsOrdersArticle
 * 
 * @property int $client_order_id
 * @property int $article_id
 * @property float $quantity
 * @property float $quantity_delivered
 * @property Carbon $inserted_date
 * 
 * @property Article $article
 * @property ClientsOrder $clients_order
 *
 * @package App\Models
 */
class ClientsOrdersArticle extends Model
{
	protected $table = 'clients_orders_articles';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'client_order_id' => 'int',
		'article_id' => 'int',
		'quantity' => 'float',
		'quantity_delivered' => 'float'
	];

	protected $dates = [
		'inserted_date'
	];

	protected $fillable = [
		'quantity',
		'quantity_delivered',
		'inserted_date'
	];

	public function article()
	{
		return $this->belongsTo(Article::class);
	}

	public function clients_order()
	{
		return $this->belongsTo(ClientsOrder::class, 'client_order_id');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientsOrdersStatus
 * 
 * @property int $id
 * @property string $name
 * 
 * @property Collection|ClientsOrder[] $clients_orders
 *
 * @package App\Models
 */
class ClientsOrdersStatus extends Model
{
	protected $table = 'clients_orders_status';
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function clients_orders()
	{
		return $this->hasMany(ClientsOrder::class, 'client_order_status_id');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientsUser
 * 
 * @property int $client_id
 * @property int $user_id
 * 
 * @property Client $client
 * @property User $user
 *
 * @package App\Models
 */
class ClientsUser extends Model
{
	protected $table = 'clients_users';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'client_id' => 'int',
		'user_id' => 'int'
	];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}

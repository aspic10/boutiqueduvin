<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Colour
 * 
 * @property int $id
 * @property string $name
 * @property string $colour
 * 
 * @property Collection|Wine[] $wines
 *
 * @package App\Models
 */
class Colour extends Model
{
	protected $table = 'colours';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'colour'
	];

	public function wines()
	{
		return $this->hasMany(Wine::class);
	}
}

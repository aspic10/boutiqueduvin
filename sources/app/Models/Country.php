<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 * 
 * @property int $id
 * @property string $name
 * @property string $iso_code
 * 
 * @property Collection|City[] $cities
 * @property Collection|Region[] $regions
 *
 * @package App\Models
 */
class Country extends Model
{
	protected $table = 'countries';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'iso_code'
	];

	public function cities()
	{
		return $this->hasMany(City::class);
	}

	public function regions()
	{
		return $this->hasMany(Region::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Day
 * 
 * @property int $id
 * @property string $name
 * @property int $number
 * 
 * @property Collection|Client[] $clients
 *
 * @package App\Models
 */
class Day extends Model
{
	protected $table = 'days';
	public $timestamps = false;

	protected $casts = [
		'number' => 'int'
	];

	protected $fillable = [
		'name',
		'number'
	];

	public function clients()
	{
		return $this->belongsToMany(Client::class, 'clients_days')
					->withPivot('hours');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Distinction
 * 
 * @property int $id
 * @property string $name
 * @property string $example
 * 
 * @property Collection|Wine[] $wines
 *
 * @package App\Models
 */
class Distinction extends Model
{
	protected $table = 'distinctions';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'example'
	];

	public function wines()
	{
		return $this->belongsToMany(Wine::class, 'wines_distinctions')
					->withPivot('rating');
	}
}

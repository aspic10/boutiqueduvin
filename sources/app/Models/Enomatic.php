<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Enomatic
 * 
 * @property int $id
 * @property Carbon $from
 * @property Carbon $to
 * 
 * @property Collection|Wine[] $wines
 *
 * @package App\Models
 */
class Enomatic extends Model
{
	protected $table = 'enomatics';
	public $timestamps = false;

	protected $dates = [
		'from',
		'to'
	];

	protected $fillable = [
		'from',
		'to',
	];

	public function wines()
	{
		return $this->belongsToMany(Wine::class, 'enomatics_wines')
					->withPivot('rank');
	}
}

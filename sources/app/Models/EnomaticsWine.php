<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EnomaticsWine
 * 
 * @property int $enomatic_id
 * @property int $wine_id
 * @property int $rank
 * 
 * @property Wine $wine
 * @property Enomatic $enomatic
 *
 * @package App\Models
 */
class EnomaticsWine extends Model
{
	protected $table = 'enomatics_wines';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'enomatic_id' => 'int',
		'wine_id' => 'int',
		'rank' => 'int'
	];

	protected $fillable = [
		'rank'
	];

	public function wine()
	{
		return $this->belongsTo(Wine::class);
	}

	public function enomatic()
	{
		return $this->belongsTo(Enomatic::class);
	}
}

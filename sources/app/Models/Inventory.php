<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Inventory
 * 
 * @property int $id
 * @property string $name
 * @property Carbon $date
 * @property Carbon $applied_date
 * 
 * @property Collection|InventoriesArticle[] $inventories_articles
 *
 * @package App\Models
 */
class Inventory extends Model
{
	protected $table = 'inventories';
	public $timestamps = false;

	protected $dates = [
		'date',
		'applied_date'
	];

	protected $fillable = [
		'name',
		'date',
		'applied_date'
	];

	public function inventories_articles()
	{
		return $this->hasMany(InventoriesArticle::class);
	}
}

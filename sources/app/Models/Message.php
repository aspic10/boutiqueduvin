<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Message
 * 
 * @property int $id
 * @property int $sender_id
 * @property int $recipient_id
 * @property string $subject
 * @property string $content
 * @property Carbon $date
 * @property bool $read
 * @property int $priority
 * 
 * @property User $user
 *
 * @package App\Models
 */
class Message extends Model
{
	protected $table = 'messages';
	public $timestamps = false;

	protected $casts = [
		'sender_id' => 'int',
		'recipient_id' => 'int',
		'read' => 'bool',
		'priority' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'sender_id',
		'recipient_id',
		'subject',
		'content',
		'date',
		'read',
		'priority'
	];

	public function user()
	{
		return $this->belongsTo(User::class, 'sender_id');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Packaging
 * 
 * @property int $id
 * @property string $name
 * @property float $price
 * @property float $quantity
 * @property bool $archived
 * 
 * @property Collection|Article[] $articles
 * @property Collection|Bill[] $bills
 * @property Collection|Client[] $clients
 *
 * @package App\Models
 */
class Packaging extends Model
{
	protected $table = 'packagings';
	public $timestamps = false;

	protected $casts = [
		'price' => 'float',
		'quantity' => 'float',
		'archived' => 'bool'
	];

	protected $fillable = [
		'name',
		'price',
		'quantity',
		'archived'
	];

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'articles_packagings');
	}

	public function bills()
	{
		return $this->belongsToMany(Bill::class, 'bills_packagings')
					->withPivot('price', 'quantity', 'quantity_returned');
	}

	public function clients()
	{
		return $this->belongsToMany(Client::class, 'clients_packagings')
					->withPivot('quantity');
	}
}

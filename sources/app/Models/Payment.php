<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Payment
 * 
 * @property int $id
 * @property int $bill_id
 * @property int $payment_type_id
 * @property Carbon $date
 * @property float $amount
 * 
 * @property Bill $bill
 * @property PaymentsType $payments_type
 *
 * @package App\Models
 */
class Payment extends Model
{
	protected $table = 'payments';
	public $timestamps = false;

	protected $casts = [
		'bill_id' => 'int',
		'payment_type_id' => 'int',
		'amount' => 'float'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'bill_id',
		'payment_type_id',
		'date',
		'amount'
	];

	public function bill()
	{
		return $this->belongsTo(Bill::class);
	}

	public function payments_type()
	{
		return $this->belongsTo(PaymentsType::class, 'payment_type_id');
	}
}

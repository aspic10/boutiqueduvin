<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentsType
 * 
 * @property int $id
 * @property string $name
 * @property bool $archived
 * 
 * @property Collection|Payment[] $payments
 * @property Collection|SuppliersPayment[] $suppliers_payments
 *
 * @package App\Models
 */
class PaymentsType extends Model
{
	protected $table = 'payments_types';
	public $timestamps = false;

	protected $casts = [
		'archived' => 'bool'
	];

	protected $fillable = [
		'name',
		'archived'
	];

	public function payments()
	{
		return $this->hasMany(Payment::class, 'payment_type_id');
	}

	public function suppliers_payments()
	{
		return $this->hasMany(SuppliersPayment::class, 'payment_type_id');
	}
}

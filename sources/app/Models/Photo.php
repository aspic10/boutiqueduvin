<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Photo
 * 
 * @property int $id
 * @property int $article_id
 * @property string $name
 * @property int $rank
 * @property int $extension_id
 * @property int $producer_id
 * 
 * @property Article $article
 * @property Extension $extension
 * @property Producer $producer
 *
 * @package App\Models
 */
class Photo extends Model
{
	protected $table = 'photos';
	public $timestamps = false;

	protected $casts = [
		'article_id' => 'int',
		'rank' => 'int',
		'extension_id' => 'int',
		'producer_id' => 'int'
	];

	protected $fillable = [
		'article_id',
		'name',
		'rank',
		'extension_id',
		'producer_id'
	];

	public function article()
	{
		return $this->belongsTo(Article::class);
	}

	public function extension()
	{
		return $this->belongsTo(Extension::class);
	}

	public function producer()
	{
		return $this->belongsTo(Producer::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Price
 * 
 * @property int $article_id
 * @property int $client_type_id
 * @property float $price
 * 
 * @property Article $article
 * @property ClientsType $clients_type
 *
 * @package App\Models
 */
class Price extends Model
{
	protected $table = 'prices';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'article_id' => 'int',
		'client_type_id' => 'int',
		'price' => 'float'
	];

	protected $fillable = [
		'price'
	];

	public function article()
	{
		return $this->belongsTo(Article::class);
	}

	public function clients_type()
	{
		return $this->belongsTo(ClientsType::class, 'client_type_id');
	}
}

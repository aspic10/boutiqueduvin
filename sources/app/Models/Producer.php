<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Producer
 * 
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string $website
 * @property string $description
 * 
 * @property City $city
 * @property Region $region
 * @property Collection|Article[] $articles
 * @property Collection|Photo[] $photos
 *
 * @package App\Models
 */
class Producer extends Model
{
	protected $table = 'producers';
	public $timestamps = false;

	protected $casts = [
		'city_id' => 'int',
		'region_id' => 'int'
	];

	protected $fillable = [
		'city_id',
		'region_id',
		'name',
		'website',
		'description'
	];

	public function city()
	{
		return $this->belongsTo(City::class);
	}
        
	public function region()
	{
		return $this->belongsTo(Region::class);
	}

	public function articles()
	{
		return $this->hasMany(Article::class);
	}

	public function photos()
	{
		return $this->hasMany(Photo::class)->orderBy("rank");
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RefundsLevel
 * 
 * @property int $id
 * @property int $refund_type_id
 * @property float $min
 * @property float $max
 * @property float $discount
 * 
 * @property RefundsType $refunds_type
 *
 * @package App\Models
 */
class RefundsLevel extends Model
{
	protected $table = 'refunds_levels';
	public $timestamps = false;

	protected $casts = [
		'refund_type_id' => 'int',
		'min' => 'float',
		'max' => 'float',
		'discount' => 'float'
	];

	protected $fillable = [
		'refund_type_id',
		'min',
		'max',
		'discount'
	];

	public function refunds_type()
	{
		return $this->belongsTo(RefundsType::class, 'refund_type_id');
	}
}

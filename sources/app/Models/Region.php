<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Region
 * 
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property string $iso_code
 * 
 * @property Country $country
 * @property Collection|Wine[] $wines
 *
 * @package App\Models
 */
class Region extends Model
{
	protected $table = 'regions';
	public $timestamps = false;

	protected $casts = [
		'country_id' => 'int'
	];

	protected $fillable = [
		'country_id',
		'name',
		'iso_code'
	];

	public function country()
	{
		return $this->belongsTo(Country::class);
	}

	public function wines()
	{
		return $this->hasMany(Wine::class);
	}
}

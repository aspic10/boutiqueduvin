<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RemindersType
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property float $interest
 * @property float $fee
 * @property string $title
 * @property int $rank
 * 
 * @property Collection|Reminder[] $reminders
 *
 * @package App\Models
 */
class RemindersType extends Model
{
	protected $table = 'reminders_types';
	public $timestamps = false;

	protected $casts = [
		'interest' => 'float',
		'fee' => 'float',
		'rank' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'interest',
		'fee',
		'title',
		'rank'
	];

	public function reminders()
	{
		return $this->hasMany(Reminder::class, 'reminder_type_id');
	}
}

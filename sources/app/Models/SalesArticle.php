<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SalesArticle
 * 
 * @property int $sale_id
 * @property int $article_id
 * @property int $sale_category_id
 * @property float $price
 * @property int $rank
 * @property string $name
 * @property bool $photo
 * 
 * @property Sale $sale
 * @property SalesCategory $sales_category
 * @property Article $article
 *
 * @package App\Models
 */
class SalesArticle extends Model
{
	protected $table = 'sales_articles';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'sale_id' => 'int',
		'article_id' => 'int',
		'sale_category_id' => 'int',
		'price' => 'float',
		'rank' => 'int',
		'photo' => 'bool'
	];

	protected $fillable = [
		'sale_category_id',
		'price',
		'rank',
		'name',
		'photo'
	];

	public function sale()
	{
		return $this->belongsTo(Sale::class);
	}

	public function sales_category()
	{
		return $this->belongsTo(SalesCategory::class, 'sale_category_id');
	}

	public function article()
	{
		return $this->belongsTo(Article::class);
	}
}

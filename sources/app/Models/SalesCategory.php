<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SalesCategory
 * 
 * @property int $id
 * @property string $name
 * @property string $colour
 * @property int $rank
 * 
 * @property Collection|SalesArticle[] $sales_articles
 *
 * @package App\Models
 */
class SalesCategory extends Model
{
	protected $table = 'sales_categories';
	public $timestamps = false;

	protected $casts = [
		'rank' => 'int'
	];

	protected $fillable = [
		'name',
		'colour',
		'rank'
	];

	public function sales_articles()
	{
		return $this->hasMany(SalesArticle::class, 'sale_category_id');
	}
}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ShopSlide
 * 
 * @property int $id
 * @property int $article_id
 * @property string $title
 * @property int $rank
 * 
 * @property Collection|ShopSlide[] $slides
 * 
 * @property Article $article
 *
 * @package App\Models
 */
class ShopSlide extends Model {
	protected $table = 'shop_slides';
	public $timestamps = false;

        protected $casts = [
		'article_id' => 'int',
		'rank' => 'int'
	];
        
	protected $fillable = [
		'article_id',
		'title',
		'rank'
	];
        
	public function article()
	{
		return $this->belongsTo(Article::class);
	}
}

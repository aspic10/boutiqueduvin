<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Supplier
 * 
 * @property int $id
 * @property string $reference
 * @property bool $merchandise
 * @property string $vat_number
 * 
 * @property Company $company
 * @property Collection|Article[] $articles
 * @property Collection|SuppliersBill[] $suppliers_bills
 * @property Collection|Client[] $clients
 * @property Collection|SuppliersOrder[] $suppliers_orders
 * @property Collection|SuppliersSale[] $suppliers_sales
 *
 * @package App\Models
 */
class Supplier extends Company
{
	protected $table = 'suppliers';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'merchandise' => 'bool'
	];

	protected $fillable = [
		'reference',
		'merchandise',
		'vat_number'
	];
        
        /**************************************************************************/
        /* CONSTRUCTOR                                                            */
        /**************************************************************************/
        public function __construct() {
            // Initialisation du parent
            parent::__construct();
        }

	public function company()
	{
		return $this->belongsTo(Company::class, 'id');
	}

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'suppliers_articles')
					->withPivot('id', 'price', 'reference', 'supplier_article_category_id', 'rank', 'suggested_price', 'refund');
	}

	public function suppliers_bills()
	{
		return $this->hasMany(SuppliersBill::class);
	}

	public function clients()
	{
		return $this->belongsToMany(Client::class, 'suppliers_clients');
	}

	public function suppliers_orders()
	{
		return $this->hasMany(SuppliersOrder::class);
	}

	public function suppliers_sales()
	{
		return $this->hasMany(SuppliersSale::class);
	}
}

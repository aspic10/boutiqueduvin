<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersArticlesSchedule
 * 
 * @property int $id
 * @property int $supplier_article_id
 * @property Carbon $date
 * @property float $supplier_buying_price
 * @property float $price
 * @property float $suggested_price
 * 
 * @property SuppliersArticle $suppliers_article
 *
 * @package App\Models
 */
class SuppliersArticlesSchedule extends Model
{
	protected $table = 'suppliers_articles_schedules';
	public $timestamps = false;

	protected $casts = [
		'supplier_article_id' => 'int',
		'supplier_buying_price' => 'float',
		'price' => 'float',
		'suggested_price' => 'float'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'supplier_article_id',
		'date',
		'supplier_buying_price',
		'price',
		'suggested_price'
	];

	public function suppliers_article()
	{
		return $this->belongsTo(SuppliersArticle::class, 'supplier_article_id');
	}
}

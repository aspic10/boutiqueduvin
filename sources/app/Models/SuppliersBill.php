<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersBill
 * 
 * @property int $id
 * @property int $supplier_order_id
 * @property int $bill_status_id
 * @property int $supplier_id
 * @property int $payment_term_id
 * @property Carbon $date
 * @property Carbon $due_date
 * @property bool $merchandise
 * @property float $discount
 * @property string $remark
 * @property float $total
 * @property float $total_taxes
 * @property float $subtotal
 * @property string $bill_number
 * @property string $delivery_number
 * 
 * @property SuppliersOrder $suppliers_order
 * @property PaymentsTerm $payments_term
 * @property Supplier $supplier
 * @property BillsStatus $bills_status
 * @property Collection|Article[] $articles
 * @property Collection|SuppliersBillsTax[] $suppliers_bills_taxes
 * @property Collection|SuppliersBillsVat[] $suppliers_bills_vats
 * @property Collection|SuppliersPayment[] $suppliers_payments
 *
 * @package App\Models
 */
class SuppliersBill extends Model
{
	protected $table = 'suppliers_bills';
	public $timestamps = false;

	protected $casts = [
		'supplier_order_id' => 'int',
		'bill_status_id' => 'int',
		'supplier_id' => 'int',
		'payment_term_id' => 'int',
		'merchandise' => 'bool',
		'discount' => 'float',
		'total' => 'float',
		'total_taxes' => 'float',
		'subtotal' => 'float'
	];

	protected $dates = [
		'date',
		'due_date'
	];

	protected $fillable = [
		'supplier_order_id',
		'bill_status_id',
		'supplier_id',
		'payment_term_id',
		'date',
		'due_date',
		'merchandise',
		'discount',
		'remark',
		'total',
		'total_taxes',
		'subtotal',
		'bill_number',
		'delivery_number'
	];

	public function suppliers_order()
	{
		return $this->belongsTo(SuppliersOrder::class, 'supplier_order_id');
	}

	public function payments_term()
	{
		return $this->belongsTo(PaymentsTerm::class, 'payment_term_id');
	}

	public function supplier()
	{
		return $this->belongsTo(Supplier::class);
	}

	public function bills_status()
	{
		return $this->belongsTo(BillsStatus::class, 'bill_status_id');
	}

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'suppliers_bills_articles', 'supplier_bill_id')
					->withPivot('id', 'reference', 'quantity', 'price', 'vat', 'sale', 'remark');
	}

	public function suppliers_bills_taxes()
	{
		return $this->hasMany(SuppliersBillsTax::class, 'supplier_bill_id');
	}

	public function suppliers_bills_vats()
	{
		return $this->hasMany(SuppliersBillsVat::class, 'supplier_bill_id');
	}

	public function suppliers_payments()
	{
		return $this->hasMany(SuppliersPayment::class, 'supplier_bill_id');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersBillsArticle
 * 
 * @property int $id
 * @property int $supplier_bill_id
 * @property int $article_id
 * @property string $reference
 * @property float $quantity
 * @property float $price
 * @property float $vat
 * @property bool $sale
 * @property string $remark
 * 
 * @property Article $article
 * @property SuppliersBill $suppliers_bill
 *
 * @package App\Models
 */
class SuppliersBillsArticle extends Model
{
	protected $table = 'suppliers_bills_articles';
	public $timestamps = false;

	protected $casts = [
		'supplier_bill_id' => 'int',
		'article_id' => 'int',
		'quantity' => 'float',
		'price' => 'float',
		'vat' => 'float',
		'sale' => 'bool'
	];

	protected $fillable = [
		'supplier_bill_id',
		'article_id',
		'reference',
		'quantity',
		'price',
		'vat',
		'sale',
		'remark'
	];

	public function article()
	{
		return $this->belongsTo(Article::class);
	}

	public function suppliers_bill()
	{
		return $this->belongsTo(SuppliersBill::class, 'supplier_bill_id');
	}
}

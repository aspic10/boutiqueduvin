<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersClient
 * 
 * @property int $supplier_id
 * @property int $client_id
 * 
 * @property Client $client
 * @property Supplier $supplier
 *
 * @package App\Models
 */
class SuppliersClient extends Model
{
	protected $table = 'suppliers_clients';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'supplier_id' => 'int',
		'client_id' => 'int'
	];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function supplier()
	{
		return $this->belongsTo(Supplier::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersOrdersArticle
 * 
 * @property int $supplier_order_id
 * @property int $article_id
 * @property float $quantity
 * @property float $quantity_received
 * 
 * @property SuppliersOrder $suppliers_order
 * @property Article $article
 *
 * @package App\Models
 */
class SuppliersOrdersArticle extends Model
{
	protected $table = 'suppliers_orders_articles';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'supplier_order_id' => 'int',
		'article_id' => 'int',
		'quantity' => 'float',
		'quantity_received' => 'float'
	];

	protected $fillable = [
		'quantity',
		'quantity_received'
	];

	public function suppliers_order()
	{
		return $this->belongsTo(SuppliersOrder::class, 'supplier_order_id');
	}

	public function article()
	{
		return $this->belongsTo(Article::class);
	}
}

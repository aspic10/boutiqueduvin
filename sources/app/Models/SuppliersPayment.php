<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SuppliersPayment
 * 
 * @property int $id
 * @property int $supplier_bill_id
 * @property int $payment_type_id
 * @property Carbon $date
 * @property float $amount
 * 
 * @property SuppliersBill $suppliers_bill
 * @property PaymentsType $payments_type
 *
 * @package App\Models
 */
class SuppliersPayment extends Model
{
	protected $table = 'suppliers_payments';
	public $timestamps = false;

	protected $casts = [
		'supplier_bill_id' => 'int',
		'payment_type_id' => 'int',
		'amount' => 'float'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'supplier_bill_id',
		'payment_type_id',
		'date',
		'amount'
	];

	public function suppliers_bill()
	{
		return $this->belongsTo(SuppliersBill::class, 'supplier_bill_id');
	}

	public function payments_type()
	{
		return $this->belongsTo(PaymentsType::class, 'payment_type_id');
	}
}

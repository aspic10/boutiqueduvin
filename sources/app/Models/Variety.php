<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Variety
 * 
 * @property int $id
 * @property string $name
 * 
 * @property Collection|Wine[] $wines
 *
 * @package App\Models
 */
class Variety extends Model
{
	protected $table = 'varieties';
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

        public function wines()
	{
		return $this->belongsToMany(Wine::class, 'wines_varieties')
					->withPivot('percentage');
	}
}

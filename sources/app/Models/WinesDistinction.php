<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WinesDistinction
 * 
 * @property int $wine_id
 * @property int $distinction_id
 * @property string $rating
 * 
 * @property Wine $wine
 * @property Distinction $distinction
 *
 * @package App\Models
 */
class WinesDistinction extends Model
{
	protected $table = 'wines_distinctions';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'wine_id' => 'int',
		'distinction_id' => 'int'
	];

	protected $fillable = [
		'rating'
	];

	public function wine()
	{
		return $this->belongsTo(Wine::class);
	}

	public function distinction()
	{
		return $this->belongsTo(Distinction::class);
	}
}

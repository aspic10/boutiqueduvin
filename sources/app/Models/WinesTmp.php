<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WinesTmp
 * 
 * @property int $id
 * @property string $code
 * @property string $name
 * @property float $price
 * @property float $buying_price
 * @property bool $no_discount
 * @property string $format
 * @property float $quantity
 * @property bool $price_list
 * @property string $description
 * @property int $number
 * @property float $vat
 * @property string $packagings
 * @property string $remarks
 * @property string $prices
 * @property int $year
 * @property bool $wine_price_list
 * @property string $code_regie
 * @property string $colour
 * @property string $region
 * @property string $varieties
 * @property string $supplier
 * @property string $reference
 * @property string $taxes
 *
 * @package App\Models
 */
class WinesTmp extends Model
{
	protected $table = 'wines_tmp';
	public $timestamps = false;

	protected $casts = [
		'price' => 'float',
		'buying_price' => 'float',
		'no_discount' => 'bool',
		'quantity' => 'float',
		'price_list' => 'bool',
		'number' => 'int',
		'vat' => 'float',
		'year' => 'int',
		'wine_price_list' => 'bool'
	];

	protected $fillable = [
		'code',
		'name',
		'price',
		'buying_price',
		'no_discount',
		'format',
		'quantity',
		'price_list',
		'description',
		'number',
		'vat',
		'packagings',
		'remarks',
		'prices',
		'year',
		'wine_price_list',
		'code_regie',
		'colour',
		'region',
		'varieties',
		'supplier',
		'reference',
		'taxes'
	];
}

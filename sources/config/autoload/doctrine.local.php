<?php
use Doctrine\DBAL\Driver\PDOMySql\Driver as PDOMySQLDriver;

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => PDOMySQLDriver::class,
                'params' => [
                    'host'     => 'localhost',                    
                    'user'     => 'root',
                    'password' => 'root',
                    'dbname'   => 'galloni',
                    'driverOptions' => [
                        1002 => 'SET NAMES `UTF8`',
                    ],
                ]
            ],            
        ],        
    ],
];

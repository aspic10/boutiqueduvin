<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    
    /* Mot de passe oublié */
    'reset_password' => 'Reset password',
    'send_password_link' => 'Send Password Reset Link',
    'password_request' => 'You are receiving this email because we received a password reset request for your account.',
    'password_expire' => 'This password reset link will expire in :count minutes.',
    'password_no_action' => 'If you did not request a password reset, no further action is required.',
    'confirm_password' => 'Confirm Password',
    'confirm_password_info' => 'Please confirm your password before continuing.',
    
    /* Email de vérification */
    'verify' => 'Verify Your Email Address',
    'link_sent' => 'A fresh verification link has been sent to :email',
    'check_email' => 'Before proceeding, please check your email for a verification link.',
    'link_not_received' => 'If you did not receive the email check your spam folder or',
    'new_link' => 'to request another link',
    'click' => 'click here',
    'click_button' => 'Please click the button below to verify your email address',
    'no_action' => 'If you did not create an account, no further action is required.',
];

@extends('layouts.shop')
@section('headTitle', 'Connexion')

@section('content')
    {{-- Flux du panier --}}
    @isset($guest)
        @include('includes.cart.flow', ['current' => 'cart/login'])
    @endisset
    
    <div class="columns-container">
        <div class="column one-half">
            <h1>Connexion</h1>

            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="row">
                    <label for="email">Adresse email</label>

                    <div>
                        <input id="email" type="email" class="@error('email') @enderror" 
                               name="email" value="{{ old('email') }}" 
                               required autocomplete="email" autofocus>

                        @error('email')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="password">Mot de passe</label>

                    <div>
                        <input id="password" type="password" class="@error('password') @enderror" 
                               name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div>
                        <button type="submit">
                            Connexion
                        </button>

                        @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">
                                Mot de passe oublié ?
                            </a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
        <div class="column one-half">
            <h3>Vous n'avez pas encore de compte ?</h3>

            <a href="{{ route('register') }}">
                <button>
                    Créer un compte
                </button>
            </a>
        </div>
    </div>
    
    {{-- Depuis le panier, ajouter la navigation --}}
    @isset($guest)
        <div class="buttons-container-flow">
            <a href="{{ route('cart') }}">
                <button class="grey">
                    Retour au panier
                </button>
            </a>
            
            <a href="{{ route('shipping') }}">
                <button>
                    Continuer en tant qu'invité(e)
                </button>
            </a>
        </div>
    @endisset
@endsection
@extends('layouts.shop')
@section('headTitle', 'Nouveau mot de passe')

@section('content')
    <h1>{{ __('auth.reset_password') }}</h1>

    <div class="card-body">
        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="row">
                <label for="email">Adresse email</label>

                <div>
                    <input id="email" type="email" class="@error('email') @enderror" 
                           name="email" value="{{ old('email') }}" 
                           required autocomplete="email" autofocus>

                    @error('email')
                        <span class="error">
                            {{ $message }}
                        </span>
                    @enderror
                </div>
            </div>

            <div class="row">
                <label for="password">Mot de passe</label>

                <div>
                    <input id="password" type="password" class="@error('password') @enderror" 
                           name="password" required autocomplete="current-password">

                    @error('password')
                        <span class="error">
                            {{ $message }}
                        </span>
                    @enderror
                </div>
            </div>

            <div class="row">
                <label for="password-confirm">Confirmez le mot de passe</label>
                <div>
                    <input id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required autocomplete="new-password">
                </div>
            </div>

            <div class="row">
                <button type="submit">
                    {{ __('auth.reset_password') }}
                </button>
            </div>
        </form>
    </div>
@endsection
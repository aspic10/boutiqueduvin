@php
$checked = "";
if($filter["checked"] === true){
    $checked = "checked";
}
@endphp
<div class="checkbox">
    <input type="checkbox" {{ $checked }} id="{{ $filter["field"] }}"/>
    <label for="{{ $filter["field"] }}">{{ $filter["label"] }}</label>
</div>
@foreach($filter["colours"] as $colour)
    @php
    $class = "";
    if($filter["colour"] == $colour->id){
        $class = "selected";
    }
    @endphp
    <div class="colour {{ $class }}"
         title="{{ $colour->name }}"
         data-id="{{ $colour->id }}"
         style="background-color: {{ $colour->colour }}"></div>
@endforeach
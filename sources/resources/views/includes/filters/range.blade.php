<div id="{{ $filter["field"] }}-container">
    <div id="{{ $filter["field"] }}-values">
        CHF
        <span id="min-{{ $filter["field"] }}">{{ $filter["min"] }}</span>.00
        -
        <span id="max-{{ $filter["field"] }}">{{ $filter["max"] }}</span>.00
    </div>

    <div id="{{ $filter["field"] }}-range"></div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // Prix
        jQuery("#{{ $filter['field'] }}-range").slider({
            range: true,
            min: {{ $filter["minRange"] }},
            max: {{ $filter["maxRange"] }},
            values: [{{ $filter["min"] }}, {{ $filter["max"] }}],
            step: 1,
            slide: function(event, ui) {
                jQuery("#min-{{ $filter['field'] }}").html(ui.values[0]);
                jQuery("#max-{{ $filter['field'] }}").html(ui.values[1]);
            }
        });
    });
</script>
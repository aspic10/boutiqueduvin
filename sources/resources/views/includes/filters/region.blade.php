@php
    if(!is_null($filter["selectedCountry"])){
        $displayRegions = "block";
    }
    else {
        $displayRegions = "none";
    }
@endphp

<select id="sel-region" style="display: {{ $displayRegions }};">
    <option value="0">Régions</option>

    @foreach($filter["options"] as $option)
        @php
        $selected = "";
        if($filter["selectedValue"] == $option->id){
            $selected = "selected";
        }

        $displayRegion = "none";
        if($filter["selectedCountry"] == $option->country_id){
            $displayRegion = "block";
        }
        @endphp

        <option value="{{ $option->id }}" {{ $selected }}
                data-country="{{ $option->country_id }}"
                style="display: {{ $displayRegion }};">
            {{ $option->name }}
        </option>
    @endforeach
</select>
<select id="sel-{{ $filter["field"] }}">
    <option value="0">{{ $filter["emptyOption"] }}</option>

    @foreach($filter["options"] as $option)
        @php
        $selected = "";
        if($filter["selectedValue"] == $option->id){
            $selected = "selected";
        }
        @endphp

        <option value="{{ $option->id }}" {{ $selected }}>
            {{ $option->name }}
        </option>
    @endforeach
</select>
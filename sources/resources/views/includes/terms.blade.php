<h2>Champ d'application</h2>
<p>Les conditions générales de vente s'appliquent à toutes les commandes passées sur la boutique en ligne de Millésime 2012 / F et J Galloni SA.</p>

<h2>Assortiment</h2>
<p>L’offre est valable tant qu’elle apparaît dans la boutique en ligne, et ce dans la limite des stocks disponibles.</p>
<p>Des modifications de prix et d'assortiment sont possibles en tout temps.</p>
<p>Les photos et informations concernant les produits (cépages, descriptions, etc.) sont indicatives et non contractuelles. Les indications spécifiques sur l’emballage du produit sont déterminantes.</p>
<p>Si un millésime est épuisé, le millésime suivant du vin commandé sera automatiquement envoyé si celui-ci est de qualité et prix équivalents.</p>

<h2>Prix</h2>
<p>Tous les prix sont indiqués en francs suisses (CHF) et incluent la TVA.</p>
<p>Les prix indiqués dans la boutique en ligne au moment de la commande font foi.</p>
<p>Les frais de livraison ne sont pas inclus dans le prix. Ils sont calculés séparément et sont indiqués dans le récapitulatif de la commande.</p>

<h2>Commande</h2>
<p>Lorsqu'une commande est passée, un contrat de vente est conclu entre le client et Millésime 2012 / F et J Galloni SA.</p>
<p>Après transmission de la commande, le client reçoit automatiquement un email de confirmation.</p>
<p>Une commande transmise ne peut plus être modifiée ou annulée par le client.</p>
<p>Millésime 2012 / F et J Galloni SA peut, sans justification, refuser tout ou partie de la commande. Dans ce cas, le client est informé et les paiements éventuellement déjà effectués sont remboursés.</p>

<h3>Boissons alcoolisées</h3>
<p>Millésime 2012 / F et J Galloni SA ne vend pas d'alcool aux personnes de moins de 18 ans. Les clients qui achètent des boissons alcoolisées confirment avoir 18 ans révolus.</p>

<h2>Paiement</h2>
<p>Les paiements sont effectués par virement bancaire anticipé ou avec des bons cadeaux.</p>
<p>Les commandes seront automatiquement supprimées si leur paiement n'est pas reçu dans les 15 jours.</p>

<h2>Livraison</h2>
<p>Les livraisons ne sont effectuées qu'en Suisse et dans la Principauté du Liechtenstein.</p>
<p>La livraison est assurée par Planzer ou la Poste. Le client peut également venir chercher sa commande directement à l'œnothèque Millésime 2012.</p>

<h3>Frais de livraison</h3>
<p>Les frais de port sont calculés selon les tarifs de Planzer et la Poste. Ils varient selon le nombre de bouteilles commandé. Ils sont offerts en cas de commande supérieure à CHF 200.- (valeur de la marchandise) ou lors d'un retrait à l'œnothèque.</p>

<h3>Délais de livraison</h3>
<p>En général, les commandes sont préparées et envoyées dans les 3 jours ouvrés après réception du paiement. Certains articles nécessitent un délai plus long (environ deux semaines) et sont signalés dans le récapitulatif de la commande.</p>
<p>Le délai de livraison est toutefois indicatif et sans engagement.</p>
<p>Des retards de livraison ne donnent pas droit à résilier le contrat de vente ou à des prétentions en dommages-intérêts de quelque nature que ce soit. Si un délai de livraison plus long est nécessaire ou si ce délai est prolongé, le client est informé.</p>

<h2>Retours et réclamations</h2>
<p>Si des articles sont livrés avec des dommages visibles au niveau de l'emballage ou du contenu, le client doit immédiatement en aviser le livreur et refuser la réception. Tous les dommages liés au transport doivent en outre être immédiatement signalés à Millésime 2012 / F et J Galloni SA.</p>
<p>Toute réclamation doit parvenir au vendeur dans les 5 jours après réception de la commande.</p>
<p>Les frais de port pour les retours sont à la charge du client.</p>

<h3>Bouteilles avec goût de bouchon</h3>
<p>Suite à la décision de l'Assemblée générale Suisse des Négociants en Vins du 10 juin 1988 et selon l'art. 197 et suivants du Code des obligations :</p>
<ul>
    <li>les vins étrangers mis en bouteille en Suisse sont remplacés durant une année à partir de la date de livraison. Les bouteilles doivent être rendues avec le bouchon d’origine et aux trois-quarts pleines.</li>
    <li>les vins étrangers mis d'origine ne sont pas remplacés.</li>
</ul>

<h2>Politique de confidentialité et protection des données</h2>
<p>
    En aucun cas vos informations ne seront communiquées à des tiers. 
    Les renseignements collectés lors d'une commande sont ceux fournis par vos
    soins dans les formulaires. Ils seront utilisés uniquement par 
    Millésime 2012/F et J Galloni pour acheminer la commande, à des fins de 
    facturation, d'information et pour vous contacter.
</p>

<h2>Droit applicable et for juridique</h2>
<p>Les présentes CGV sont régies par le droit suisse. Le for juridique est établi à Saxon, Suisse.</p>

<h2>Contact</h2>
<p>
    Millésime 2012 / F et J Galloni SA
    <br/>
    Route de l'Usine 4
    <br/>
    1907 Saxon (Suisse)
    <br/>
    027 / 306.21.38
    <br/>
    contact@millesime2012.ch
</p>
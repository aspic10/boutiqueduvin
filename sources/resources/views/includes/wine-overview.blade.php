@php
    if(isset($wine)){
        $article = $wine->article;
    }
    elseif(isset($article)){
        $wine = $article->wine;
    }
    
    $photo = $article->photos->first();
    
    // Quantité limitée
    if(!is_null($article->limited_quantity)){
        $soonAvailable = false;
        $maxQuantity = $article->limited_quantity;
    }
    // Quantité non limitée
    else {
        $soonAvailable = $article->soon_available;
        $maxQuantity = $article->quantity;
    }
@endphp

@if($article->isVisible() || $onlyVisible === false)
    <a href="{{ route("wine", $wine->id) }}"
       class="wine-overview column">
        <div class="title"><div>{{ $wine }}</div></div>
        <div class="picture-container">
            @if(!is_null($photo))
                <img src="{{ config('app.photo_url') }}article/{{ $wine->id }}/{{ $photo->name }}"/>
            @else
                <img src="/img/logo.png"/>
            @endif
        </div>
        <div class="special">
            {{-- Vin primé --}}
            @if(count($wine->distinctions) > 0)
                <i class="fas fa-award distinction"
                   title="Vin primé"></i>
            @endif

            {{-- Coup de coeur --}}
            @if($wine->favourite == true)
                <i class="far fa-heart favourite"
                   title="Coup de coeur"></i>
            @endif

            {{-- Rareté --}}
            @if(!is_null($wine->limitedQuantity))
                <i class="far fa-gem rare"
                   title="Vin rare"></i>
            @endif

            {{-- Appellation --}}
            @if(!is_null($wine->appellation))
                <i class="fas fa-certificate"
                   title="{{ $wine->appellation->name }}"></i>
            @endif
        </div>
        
        @if($onlyVisible === true)
            <div class="price">
                {{ number_format($article->getShopPrice(), 2, ".", "'") }}
                /
                {{ $wine->litres }}cl
            </div>

            {{-- Article indisponible --}}
            @if($soonAvailable === false && $maxQuantity == 0)
                <span class="unavailable">Actuellement indisponible</span>
            {{-- Article disponible ou livrable rapidement --}}
            @else
                <i class="fas fa-shopping-cart cart"
                   onclick="openAddToCartDialog(event, {{ $wine->id }}, '{{ addslashes($wine) }}', {{ $article->getShopPrice() }}, {{ $wine->litres }}, {{ (int) $soonAvailable }}, {{ $maxQuantity }});"></i>
            @endif
    <!--        {{-- Promotion --}}
            @if($wine->favourite == true)
                <i class="fas fa-certificate sale"
                   title="En action"></i>
            @endif-->
        @endif

        {{-- Favori --}}
        @if(!is_null($client) && $onlyVisible === true)
            @php
            if($client->articles->contains($article)){
                $class = "yes";
                $title = "Retirer des favoris";
            }
            else {
                $class = "";
                $title = "Ajouter aux favoris";
            }
            @endphp
            
            <div class="favourite {{ $class }}" title="{{ $title }}"
                 data-id="{{ $wine->id }}">
                <i class="fas fa-bookmark"></i>
                <i class="fas fa-star"></i>
            </div>
        @endif
    </a>
@endif
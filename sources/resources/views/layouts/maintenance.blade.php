<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Millésime 2012</title>
        
        <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        {{-- Fonts --}}
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,500&display=swap" rel="stylesheet">
        
        {{-- Style --}}
        <style>
            body {
                font-family: "Poppins", sans-serif;
                font-weight: 300;
                color: #5A5A5A;
                background: #FAFAFA;
                width: 100vw;
                height: 100vh;
                margin: 0;
                user-select: none;
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            
            #logo {
                max-width: 300px;
            }

            h1 {
                position: relative;
                font-weight: 500;
                text-shadow: 1px 1px 2px #BABABA;
                text-transform: uppercase;
            }
        </style>
    </head>
    <body>
        <div id="app"></div>
        
        {{-- Logo --}}
        <img src="{{ asset('img/logo-full-shadow.png') }}"
             alt="Millésime 2012" id="logo"/>
        
        {{-- Contenu principal --}}
        <div id="main-container">
            <h1>Site en maintenance</h1>
            <p>Merci de revenir dans quelques minutes</p>
        </div>
    </body>
</html>
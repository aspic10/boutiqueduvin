<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('headTitle')</title>
        
        <link rel="icon" href="{{ URL::asset('favicon.ico') }}" type="image/x-icon"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        {{-- Fonts --}}
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,500&display=swap" rel="stylesheet">
        <link href="{{ asset('fonts/alpaca/alpaca.css') }}" rel="stylesheet">
        <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
        
        {{-- Styles compilés --}}
        <link type="text/css" href="{{ asset('css/shop.css') }}" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="{{ mix('css/app.css') }}">
    </head>
    <body>
        <div id="app"></div>
        
        {{-- Scripts compilés --}}
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/cart.js') }}"></script>
        
        {{-- En-tête --}}
        <div id="main-header" role="navigation">
            {{-- Logo --}}
            <div id="logo" onclick="document.location.href='{{ route("home") }}'">
                <span class="o"></span>
                <span class="o"></span>
                <span class="o"></span>
                <span class="o"></span>
                <span class="o"></span>
                <span class="b"></span>
                <img src="/img/logo-millesime.png"/>
            </div>
            
            {{-- Menu utilisateur et panier --}}
            <div id="user-menu">
                {{-- Utilisateur --}}
                <div class="user-menu-item">
                    <i class="fas @guest fa-user @else fa-user-check @endguest main-icon"
                       onclick="document.location.href='{{ route("account") }}'"></i>
                    
                    <div class="user-menu-info">
                        {{-- Utilisateur pas connecté -> lien vers le login --}}
                        @guest
                            <ul>
                                <li>
                                    <a href="{{ route("login")}}">
                                        Connexion
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route("register")}}">
                                        Créer un compte
                                    </a>
                                </li>
                            </ul>

                        @else
                            {{-- Utilisateur connecté -> lien vers son compte --}}
                            <h3>{{ Auth::user() }}</h3>
                            <ul>
                                <li>
                                    <a href="{{ route("account") }}">
                                        Mon compte
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                        Déconnexion
                                    </a>
                                </li>
                            </ul>

                            <form id="logout-form" action="{{ route('logout') }}" 
                                  method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endguest
                    </div>
                </div>
                
                {{-- Panier --}}
                <div class="user-menu-item">
                    <i class="fas fa-shopping-cart main-icon"
                       onclick="document.location.href='{{ route("cart") }}'"></i>
                    <span id="nb-articles-cart"
                          onclick="document.location.href='{{ route("cart") }}'"></span>
                    
                    {{-- Aperçu sauf depuis la page du panier --}}
                    @if(Request::url() !== route("cart"))
                        <div class="user-menu-info">
                            <h3>Aperçu du panier</h3>
                            <div id="cart-overview">
                                
                                <table class="table-full-width">
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3" class="nb-bottles"></td>
                                            <th colspan="2" class="right bold total"></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="right" style="padding: 0 3px;">
                                    <i>Sous-total en CHF hors frais de port</i>
                                </div>
                            </div>
                            <div class="buttons-container" id="cart-buttons">
                                <input type="button" class="grey" value="Vider le panier"
                                       onclick="emptyCart()"/>
                                <input type="button" value="Voir le panier"
                                       onclick="document.location.href='{{ route("cart")}}'" />
                            </div>
                            <div class="empty-cart">
                                Le panier est vide
                            </div>
                        </div>
                    @endif
                </div>
                <div class="user-menu-item" id="icon-menu">
                    <i class="fas fa-bars main-icon"></i>
                </div>
            </div>

            {{-- Eléments du menu --}}
            <?php
            $menu = array(
                "Accueil" => array(
                    "route" => "home",
                    "submenu" => array(),
                ),
                "L'œnothèque" => array(
                    "route" => "presentation",
                    "submenu" => array(
                        "Présentation" => "presentation",
                        "Espace Découverte" => "decouverte",
                    ),
                ),
                "Shop" => array(
                    "route" => "wines",
                    "submenu" => array(
                        "Vins" => "wines",
                        "Cadeaux" => "gift",
                        "CGV" => "terms",
                    ),
                ),
                "Producteurs" => array(
                    "route" => "producers",
                    "submenu" => array(),
                ),
                "Contact" => array(
                    "route" => "contact",
                    "submenu" => array(
                        "Contact et horaires" => "contact",
                        "Newsletter" => "newsletter",
                    ),
                ),
            );
            ?>

            {{-- Menu --}}
            <nav id="menu">
                <div id="icon-menu-close">
                    <i class="fas fa-times"></i>
                </div>
                <ul>
                    {{-- Afficher le menu --}}
                    @foreach($menu as $label => $menuInfo)
                        <li>
                            <a href="{{ route($menuInfo["route"])}}">
                                {!! $label !!}
                            </a>
                            
                            @if(count($menuInfo["submenu"]) > 0)
                                <div class="submenu">
                                    @foreach($menuInfo["submenu"] as $label => $route)
                                        <a href="{{ route($route)}}">
                                            {!! $label !!}
                                        </a>
                                    @endforeach
                                </div>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </nav>
            
            {{-- Remonter au sommet --}}
            <span id="return-to-top"
                  onclick="jQuery('html, body').animate({scrollTop: '0'});"></span>
        </div>

        {{-- Contenu principal --}}
        <div id="main-container">
            @yield('content')
        </div>

        {{-- Footer --}}
        <div id="main-footer">
            <div id="footer-content">
                <img src="{{ asset('img/logo-pink.png') }}"
                     alt="Millésime 2012"/>
                Millésime 2012 - F et J Galloni SA
            </div>
        </div>
        
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        
        </script>
        <!--End of Tawk.to Script-->
        
        
        <script type="text/javascript">
            var mainHeader = jQuery("#main-header");
            var menu = jQuery("#menu");
            var menuTop = 0;
            var body = jQuery("body");
            var returnToTop = jQuery("#return-to-top");

            
            /**
             * Chargement de la page
             */
            document.addEventListener('DOMContentLoaded', function(){
                menuTop = menu.offset().top;
                setColumnsWidth();
            });
            
            /**
             * Redimensionnement de la fenêtre
             */
            jQuery(window).on("resize", function(){
                setColumnsWidth();
            });
            
            /**
             * Scroll
             */
            jQuery(window).on("scroll", function(){
                // Afficher le petit header
                if(this.scrollY > menuTop){
                    mainHeader.addClass("fixed");
                }
                // Sommet de la page -> afficher le grand header
                else {
                    mainHeader.removeClass("fixed");
                }
                
                // Afficher le bouton pour revenir vers le haut
                if(this.scrollY > 0){
                    returnToTop.addClass("visible");
                }
                // Masquer le bouton pour revenir vers le haut
                else {
                    returnToTop.removeClass("visible");
                }
            });
            
            /**
             * Largeur des colonnes
             */
            function setColumnsWidth(){
                jQuery(".columns-container").each(function(){
                    let columnContainer = jQuery(this);
                    let firstColumn = columnContainer.find(".column").first();
                    let maxWidth = firstColumn.css("max-width");
                    
                    // Ne resizer que les colonnes qui ont une max-width précise
                    // en pixels (400px)
                    if(maxWidth.replace("px", "") === (parseFloat(maxWidth) + "")){
                        // Reset max width (sinon la première colonne peut être plus large que les autres)
                        columnContainer.find(".column").css("max-width", maxWidth);
                        
                        // Attribuer la largeur de la première colonne comme
                        // largeur maximale des autres colonnes
                        columnContainer.find(".column").not(":first").css("max-width", firstColumn.outerWidth());
                    }
                });
            }
            
            
            /**
             * Mettre en forme un float
             */
            function formatFloat(float){
                var localStringOptions = {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2
                };
                
                return parseFloat(float).toLocaleString("de-CH", localStringOptions);
            }
            
            
            /**
             * N'autoriser que les entiers
             */
            function onlyInt(e, negative){
                var charCode = e.charCode;

                if((charCode < 48 || charCode > 57) && charCode !== 13){
                    if(negative === true && charCode === 45){
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return true;
                }
            }
            
            
            // Ouvrir/fermer le menu (mobile)
            jQuery("#icon-menu, #icon-menu-close").click(function(){
                jQuery("#menu").toggleClass("active");
                
                if(jQuery("#menu").hasClass("active")){
                    Tawk_API.hideWidget();
                }
                else {
                    Tawk_API.showWidget();
                }
            });
            
            
            /**
             * Dialog
             */
            jQuery(".dialog").click(function(){
                jQuery(this).removeClass("visible");
            });
            jQuery(".dialog-content").click(function(e){
                e.stopPropagation();
            });
            
            
            /**
             * Ajouter / Retirer un vin des favoris
             */
            jQuery(".favourite").click(function(e){
                e.stopPropagation();
                e.preventDefault();
                
                var element = jQuery(this);
                var articleId = element.data("id");

                jQuery.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                jQuery.ajax({
                    type: "POST",
                    url: "{{ route('setFavourite') }}",
                    data: {
                        articleId: articleId
                    },
                    success: function(favourite){
                        if(favourite === "1"){
                            element.attr("title", "Retirer des favoris");
                            element.addClass("yes");
                        }
                        else {
                            // Depuis la page des vins favoris du client
                            // -> enlever l'article
                            @if(Route::currentRouteName() === "clientWines")
                                element.parents(".wine-overview").remove();
                            // Depuis les autres pages -> possibilité de recocher
                            @else
                                element.attr("title", "Ajouter aux favoris");
                                element.removeClass("yes");
                            @endif
                        }
                    }
                });
                
                // Empêche la redirection du <a>
                return false;
            });
            
            
            /**
             * Tawk.to
             */
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/615d6ba1d326717cb68500d6/1fhaeorjq';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
            })();

            /* Customize the widget as soon as the widget is loaded */
            Tawk_API.onLoad = function(){
                setTimeout(function(){
                    // Changer l'emplacement
                    jQuery("iframe[title='chat widget']").eq(0).css({
                        'right': '10px',
                        'bottom': '10px'
                    });
                }, 100);
            };
        </script>
    </body>
</html>

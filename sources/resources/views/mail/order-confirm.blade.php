<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="color-scheme" content="light"/>
        <meta name="supported-color-schemes" content="light"/>
    </head>
    <body>
        <style>
            /* Base */
            body,
            body *:not(html):not(style):not(br):not(tr):not(code) {
                box-sizing: border-box;
                font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,
                    'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';
                position: relative;
            }

            body {
                -webkit-text-size-adjust: none;
                height: 100%;
                line-height: 1.4;
                margin: 0;
                padding: 0;
                width: 100% !important;
            }

            a {
                color: #DB0A5B;
                text-decoration: none;
            }

            a img {
                border: none;
            }

            /* Typography */
            h1 {
                margin-top: 0;
            }

            img {
                max-width: 100%;
            }

            /* Layout */
            .wrapper {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 100%;
                background-color: #F2F2F2;
                width: 100%;
            }

            .content {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 100%;
                width: 100%;
            }

            /* Header */
            .header {
                padding: 25px 0;
                text-align: center;
            }

            /* Logo */
            .logo {
                max-height: 100px;
                max-width: 300px;
            }

            /* Body */

            .body {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 100%;
                margin: 0;
                padding: 0;
                width: 100%;
            }

            .inner-body {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 800px;
                background-color: #ffffff;
                border-color: #e8e5ef;
                border-radius: 2px;
                border-width: 1px;
                box-shadow: 0 2px 0 rgba(0, 0, 150, 0.025), 2px 4px 0 rgba(0, 0, 150, 0.015);
                margin: 0 auto;
                padding: 20px;
                width: 800px;
            }

            /* Footer */
            .footer {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 100%;
                text-align: center;
                width: 100%;
                padding: 10px 0;
            }

            .footer p {
                color: #b0adc5;
                font-size: 12px;
                text-align: center;
            }

            /* Custom */
            .full-width {
                width: 100%;
            }
            .right {
                text-align: right;
            }
            .center {
                text-align: center;
            }
            .total {
                font-size: 1.2em;
                color: #DB0A5B;
            }
            .color {
                color: #DB0A5B;
            }

            @media only screen and (max-width: 800px) {
                .inner-body {
                    width: 100% !important;
                }
            }
        </style>

        <table class="wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td align="center">
                    <table class="content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                        <tr>
                            <td class="header">
                                <img src="{{ asset('img/logo-email.png') }}" class="logo" alt="{{ config('app.name') }}">
                            </td>
                        </tr>

                        <!-- Email Body -->
                        <tr>
                            <td class="body" width="100%" cellpadding="0" cellspacing="0">
                                <table class="inner-body" align="center" width="800" cellpadding="0" cellspacing="0" role="presentation">
                                    <tr>
                                        <td>
                                            @php
                                            $articles = $bill->articles;
                                            $giftCards = $bill->gift_cards;
                                            $giftCardsBought = $bill->gift_cards_bought;
                                            @endphp

                                            <h1>Merci pour votre commande !</h1>

                                            <h2>Récapitulatif de la commande <span class="color">{{ $bill->id }}</span></h2>
                                            {{-- Contenu du panier --}}
                                            <div>
                                                <table width="100%" class="full-width">
                                                    <thead>
                                                        <tr>
                                                            <th>Quantité</th>
                                                            <th>Désignation</th>
                                                            <th>Format</th>
                                                            <th>Prix</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                        $totalPrice = 0;
                                                        @endphp

                                                        {{-- Articles --}}
                                                        @foreach($articles as $article)
                                                            @php
                                                            $billArticle = $article->pivot;
                                                            $quantity = $billArticle->quantity;
                                                            $price = round(($billArticle->price + $billArticle->price * $billArticle->vat / 100) / 0.05) * 0.05;
                                                            $articlePrice = $price * $quantity;
                                                            $totalPrice += $articlePrice;
                                                            @endphp

                                                            <tr>
                                                                <td class="center">{{ $quantity }}</td>
                                                                <td>{{ $billArticle->name }}</td>
                                                                <td class="center">{{ $billArticle->format }}</td>
                                                                <td class="right">{{ number_format($price, 2, ".", "'") }}</td>
                                                                <td class="right">{{ number_format($articlePrice, 2, ".", "'") }}</td>
                                                            </tr>
                                                        @endforeach
                                                        
                                                        {{-- Bons cadeaux --}}
                                                        @foreach($giftCardsBought as $giftCard)
                                                            @php
                                                            $price = $giftCard->initial_amount;
                                                            $totalPrice += $price;
                                                            @endphp

                                                            <tr>
                                                                <td class="center">1</td>
                                                                <td>Bon cadeau</td>
                                                                <td class="center">-</td>
                                                                <td class="right">{{ number_format($price, 2, ".", "'") }}</td>
                                                                <td class="right">{{ number_format($price, 2, ".", "'") }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th colspan="4" class="right">Sous-total</th>
                                                            <th class="right">{{ number_format($totalPrice, 2, ".", "'") }}</th>
                                                        </tr>
                                                        <tr>
                                                            {{-- Frais de port --}}
                                                            @php
                                                            $delivery = round($bill->total_taxes / 0.05) * 0.05;
                                                            $totalPrice += $delivery;
                                                            @endphp

                                                            <th colspan="4" class="right">Frais de port</th>
                                                            <th class="right">{{ number_format($delivery, 2, ".", "'") }}</th>
                                                        </tr>

                                                        <tr class="total">
                                                            <th colspan="4" class="right">Total en CHF</th>
                                                            <th class="right">{{ number_format($totalPrice, 2, ".", "'") }}</th>
                                                        </tr>

                                                        @foreach($giftCards as $giftCard)
                                                            @php
                                                            $giftCardBill = $giftCard->pivot;
                                                            $amount = $giftCardBill->amount;
                                                            $totalPrice -= $amount;
                                                            @endphp
                                                            @if($amount > 0)
                                                                <tr>
                                                                    <td colspan="4" class="right">
                                                                        - Bon {{ $giftCard->number }}
                                                                    </td>
                                                                    <td class="right">
                                                                        -{{ number_format($amount, 2, ".", "'") }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach

                                                        @if(count($giftCards) > 0)
                                                        <tr>
                                                            <th colspan="4" class="right">Solde à payer en CHF</th>
                                                            <th class="right">{{ number_format($totalPrice, 2, ".", "'") }}</th>
                                                        </tr>
                                                        @endif
                                                    </tfoot>
                                                </table>
                                            </div>

                                            <h3>Informations</h3>
                                            <p>{!! nl2br($bill->remark) !!}</p>

                                            <h3>Adresse de facturation</h3>
                                            <p>{!! nl2br($bill->address) !!}</p>

                                            @if($prepayment === true && $totalPrice >= 0.05)
                                            <h2>Informations de paiement</h2>
                                            <ul>
                                                <li>
                                                    <b>Banque :</b>
                                                    Raiffeisen Martigny et Région
                                                </li>
                                                <li>
                                                    <b>Destinataire :</b>
                                                    F et J Galloni SA - Route de l'Usine 4 - 1907 Saxon
                                                </li>
                                                <li>
                                                    <b>IBAN :</b>
                                                    CH87 8080 8006 4672 6780 1
                                                </li>
                                                <li>
                                                    <b>Référence :</b>
                                                    Facture {{ $bill->id }}
                                                </li>
                                                <li>
                                                    <b>Montant :</b>
                                                    CHF {{ number_format($totalPrice, 2, ".", "'") }}
                                                </li>
                                                <li>
                                                    <b>Délai de paiement :</b>
                                                    15 jours
                                                </li>
                                            </ul>
                                            @endif

                                            <p>Avec nos remerciements et nos meilleures salutations,</p>

                                            <p>Millésime 2012</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="footer" align="center" width="800" cellpadding="0" cellspacing="0" role="presentation">
                                    <tr>
                                        <td align="center">
                                            <p>
                                                Millésime 2012 / F et J Galloni SA
                                                <br/>
                                                Route de l'Usine 4
                                                <br/>
                                                1907 Saxon
                                                <br/>
                                                <a href="{{ config('app.url') }}">
                                                    {{ config('app.url') }}
                                                </a>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
@extends('layouts.shop')
@section('headTitle', 'Paiement')

@php
$client = Auth::user();
@endphp

@section('content')
    {{-- Flux du panier --}}
    @include('includes.cart.flow', ['current' => 'confirm'])
    
    <h1><i class="fas fa-check colour"></i> Commande transmise !</h1>
    
    <p>
        La commande <a href="{{ route("bill", Request::route('billId')) }}" class="bold">{{ Request::route('billId') }}</a> nous
        est bien parvenue et nous vous en remercions !
    </p>
    
    <p>
        Une confirmation contenant tous les détails vous a été envoyée par email.
        Pensez à vérifier le dossier spam si vous ne la trouvez pas dans votre 
        boîte de réception.
    </p>
    <p>
        En cas de paiement anticipé, les coordonnées bancaires figurent également
        dans l'email.
    </p>
    @auth
        <p>
            Vous pouvez consulter le détail et le statut de toutes vos commandes
            depuis <a href="{{ route("clientBills") }}" class="bold">votre compte</a>.
        </p>
    @endauth
    <p>
        Pour toute question concernant votre commande, contactez-nous en
        mentionnant le numéro de commande <span class="colour">{{ Request::route('billId') }}</span>.
    </p>
    
    <div class="buttons-container-flow">
        <a href="{{ route("home") }}">
            <button>
                Retour à l'accueil
            </button>
        </a>
    </div>
@endsection

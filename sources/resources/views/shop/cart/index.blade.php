@extends('layouts.shop')

@section('headTitle', 'Panier')

@section('content')
    {{-- Flux du panier --}}
    @include('includes.cart.flow', ['current' => 'cart'])

    <h1>Panier d'achat</h1>
    
    {{-- Articles modifiés --}}
    <p class="alert warning hidden" id="warning-modifications">
        Certains articles ont été modifiés depuis leur ajout au panier. Veuillez
        vérifier le récapitulatif ci-dessous.
    </p>
    
    <p class="alert info">
        Les frais de port sont offerts dès 200.- ou si vous souhaitez retirer la 
        marchandise à Saxon.
    </p>
    
    {{-- Contenu du panier --}}
    @include('includes.cart.overview', ['page' => 'cart/index'])
        
    <div class="buttons-container-flow" id="cart-buttons">
        <input type="button" class="grey" value="Vider le panier"
               onclick="emptyCart()"/>
        <a href="{{ route("cart/login")}}">
            <button>
                Continuer
            </button>
        </a>
    </div>
    
    <div class="empty-cart">
        Le panier est vide
        
        <div class="buttons-container-flow">
            <input type="button" class="grey" value="Poursuivre les achats"
                   onclick="document.location.href='{{ route("wines")}}'"/>
        </div>
    </div>
    
    <script type="text/javascript">
        var checkboxPickup = jQuery("#pickup");
        
        
        /**
         * Changer la quantité d'un article
         */
        function udpateItem(element, price){
            var tr = element.parents("tr");
            var articleId = tr.data("id");
            var type = tr.data("type");
            var tdTotal = tr.find(".total");
            var tdQuantity = element.parents("td");
            var tdName = tr.find(".name");
            var previousTotal = parseFloat(tdTotal.html().replaceAll('’', '')).toFixed(2);
            var newQuantity = parseInt(element.val());
            var previousQuantity = element.data("quantity");
            var max = parseInt(element.attr("max"));
            var maxSoon = parseInt(element.data("max"));
            
            if(isNaN(newQuantity)){
                newQuantity = previousQuantity;
                element.val(newQuantity);
            }
            
            if(isNaN(max)){
                max = null;
            }
            
            if(isNaN(maxSoon)){
                maxSoon = null;
            }
            
            tdName.find(".soon").remove();
            tdQuantity.find("span").remove();
            
            // Commande supérieure au stock disponible
            if(max !== null && max < newQuantity){
                newQuantity = max;
                element.val(newQuantity);
                tdQuantity.append(quantityChangedSpan.clone());
            }
            else {
                // Commande supérieure au stock disponible mais livrable rapidement
                if(maxSoon !== null && maxSoon < newQuantity){
                    tdName.append(soonAvailableSpan.clone());
                }
            }
            
            if(type === "wine"){
                nbBottles -= previousQuantity;
            }
            
            totalCart -= previousTotal;
            
            // Supprimer l'article
            if(newQuantity <= 0){
                // Supprimer la ligne de l'aperçu du panier
                tr.remove();

                // Mettre à jour l'aperçu du panier et le localStorage
                removeFromCartOverview(articleId);
            }
            // Mettre à jour la quantité
            else {
                var newTotal = Math.round(newQuantity * price * 20) / 20;
                tdTotal.html(formatFloat(newTotal));
                if(type === "wine"){
                    nbBottles += newQuantity;
                }
                totalCart += newTotal;
                element.data("quantity", newQuantity);
                
                // Sauver le panier
                cart[articleId].quantity = newQuantity;
                localStorage.setItem("cart", JSON.stringify(cart));
                
                // Mettre à jour l'aperçu du panier
                addItemCartOverview(articleId);
            }
            
            totalCart = parseFloat(totalCart.toFixed(2));
            
            // Mettre à jour le total
            setTotalCart();
        }
        
        
        /**
         * Supprimer un article du panier 
         */
        function removeFromCart(articleId){
            var tr = cartBody.find("tr[data-id='" + articleId + "']");
            var tdTotal = tr.find(".total");
            var previousTotal = parseFloat(tdTotal.html());
            var previousQuantity = tr.find(".quantity").data("quantity");
            
            var cartItem = cart[articleId];
            var type = cartItem.type;
            
            if(type === "wine"){
                nbBottles -= previousQuantity;
            }
            
            totalCart -= previousTotal;
        
            // Supprimer la ligne de l'aperçu du panier
            tr.remove();
            
            // Mettre à jour l'aperçu du panier et le localStorage
            removeFromCartOverview(articleId);

            // Mettre à jour le total du panier
            setTotalCart();
        }
        
        
        /**
         * Retrait à Saxon
         */
        checkboxPickup.click(function(){
            checkout["pickup"] = checkboxPickup.is(":checked");
            localStorage.setItem("checkout", JSON.stringify(checkout));
            setTotalCart();
        });
    </script>
@endsection
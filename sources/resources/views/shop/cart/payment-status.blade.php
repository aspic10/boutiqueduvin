<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        {{-- Fonts --}}
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,500&display=swap" rel="stylesheet">
        <link href="{{ asset('fonts/alpaca/alpaca.css') }}" rel="stylesheet">
        <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
        
        {{-- Styles compilés --}}
        <link type="text/css" href="{{ asset('css/shop.css') }}" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="{{ mix('css/app.css') }}">
    </head>
    <body>
        <div class="center payment-status">
            <div class="bold">{{ $label }}</div>
            <i class="{{ $icon }} {{ $colour }}"></i>
        </div>
        
        <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function(){
                @if($clear === true)
                    // Vider le panier
                    localStorage.removeItem("cart");

                    var checkout = JSON.parse(localStorage.getItem("checkout") || null) || {};
                    checkout.remark = "";
                    checkout.giftMessage = "";
                    checkout.gift = false;
                    localStorage.setItem("checkout", JSON.stringify(checkout));
                @endif
                
                top.location.href = "{{ $url }}";
            });
        </script>
    </body>
</html>
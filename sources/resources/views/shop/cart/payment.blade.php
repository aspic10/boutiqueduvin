@extends('layouts.shop')
@section('headTitle', 'Paiement')

@php
$client = Auth::user();
@endphp

@section('content')
    {{-- Flux du panier --}}
    @include('includes.cart.flow', ['current' => 'payment'])
    
    <h1>Paiement</h1>
    
    {{-- Invité --}}
    @if(is_null($client))
        <div class="alert info">
            Vous commandez en tant qu'invité(e). Souhaitez-vous vous <a href="{{ route('cart/login') }}">connecter
            ou créer un compte</a> ?
        </div>
    @endif

    <h2>Moyen de paiement</h2>

    <div class="form" id="guest-form">
        <div class="row">
            <input type="radio" name="payment" class="payment" 
                   value="prepayment" id="prepayment"/>
            <label for="prepayment">Paiement anticipé</label>
            <div class="info">
                Vous recevrez nos coordonnées bancaires par email après
                avoir confirmé la commande.
                <br/>
                Nous enverrons la marchandise une fois le paiement reçu.
                <br/>
                La commande sera annulée si le virement n'est pas effectué dans les 15 jours.
            </div>
        </div>
    </div>
    
    <h3>Utiliser un bon cadeau</h3>
    
    @if(!is_null($client))
        <h4>Bons sur mon compte</h4>
        
        <div id="gift-cards-client">
            {{-- Détail de chaque bon cadeau --}}
            @foreach($validGiftCards as $giftCard)
                <div class="gift-card" data-number="{{ $giftCard->number }}"
                    ><i class="fas fa-gift"></i
                    ><span class="bold">Bon {{ $giftCard->number }} :</span>
                    {{ number_format($giftCard->amount, 2, ".", "'") }} CHF
                    <i class="fas fa-times" onclick="showGiftCard('{{ $giftCard->number }}', '{{ $giftCard->pin }}')"></i>
                </div>
            @endforeach
        </div>
    @endif
    
    <h4>Bons appliqués à cette commande</h4>
    <div id="gift-cards" class="empty"></div>
    
    <h4>Ajouter un bon</h4>
    {{-- Formulaire d'ajout d'un bon cadeau --}}
    @include('includes.add-gift-card')
    
    <div class="buttons-container-flow">
        <a href="{{ route("cart") }}">
            <button class="grey">
                Retour au panier
            </button>
        </a>
        <a href="{{ route("validate") }}">
            <button>
                Continuer
            </button>
        </a>
    </div>
    
    <script type="text/javascript">
        var giftCardsContainer = jQuery("#gift-cards");
        var giftCardsClientContainer = jQuery("#gift-cards-client");
        var icon = jQuery("<i>").addClass("fas fa-gift");
        var close = jQuery("<i>").addClass("fas fa-times");
        
        var payment = checkout["payment"] || "credit-card";
        var giftCards = checkout["giftCards"] || {};
        
        document.addEventListener('DOMContentLoaded', function(){
            if(giftCardsClientContainer.find(".gift-card").length === 0){
                giftCardsClientContainer.addClass("empty");
            }
            
            // Vérifier que l'utilisateur ne s'est pas dé/connecté entre temps
            var guest = localStorage.getItem("guest") || null;

            // Utilisateur non connecté
            @guest
                if(guest !== "true"){
                    document.location.href = "{{ route('shipping') }}";
                }
            // Utilisateur connecté
            @else
                if(guest !== "false"){
                    document.location.href = "{{ route('shipping') }}";
                }
            @endguest
            
            // Sélectionner l'option selon le localStorage
            jQuery(".payment[value='" + payment + "']").prop("checked", true);
            jQuery(".payment[value='" + payment + "']").parents(".row").find(".info").show();
            checkout["payment"] = payment;
            localStorage.setItem("checkout", JSON.stringify(checkout));
            
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            // Afficher les bons cadeaux
            for(var number in giftCards){
                showGiftCard(number, giftCards[number].pin);
            }
        });
        
        
        /**
         * Choix d'un moyen de paiement
         */
        jQuery(".payment").on("click", function(){
            jQuery(".form .info").hide();
            payment = jQuery(this).val();
            jQuery(this).parents(".row").find(".info").show();
            checkout["payment"] = payment;
            localStorage.setItem("checkout", JSON.stringify(checkout));
        });
        
        
        /**
         * Supprimer un bon cadeau
         */
        function removeGiftCard(number, pin){
            var giftCard = jQuery(".gift-card[data-number='" + number + "']");
            
            // Supprimer des bons appliqués à la commande
            giftCard.removeClass("applied");
            giftCard.find(".fa-times").remove();
            giftCard.append(close.clone().attr("onclick", "showGiftCard('" + number + "', '" + pin + "')"));
            
            // Ajouter aux bons du client
            giftCardsClientContainer.append(giftCard);
            giftCardsClientContainer.removeClass("empty");
            
            // Enlever du localStorage
            delete giftCards[number];
            checkout["giftCards"] = giftCards;
            localStorage.setItem("checkout", JSON.stringify(checkout));
            
            if(giftCardsContainer.find(".gift-card").length === 0){
                giftCardsContainer.addClass("empty");
            }
        }
        
        
        /**
         * Afficher un bon cadeau
         */
        function showGiftCard(number, pin){
            // Vérifier le montant disponible du bon cadeau
            jQuery.ajax({
                type: "POST",
                async: false,
                url: "{{ route('getGiftCard') }}",
                data: {
                    number: number,
                    pin: pin
                },
                success: function(availableAmount){
                    let giftCard = jQuery(".gift-card[data-number='" + number + "']");

                    if(availableAmount > 0){
                        // Ajouter dans le localStorage
                        giftCards[number] = {
                            pin: pin,
                            amount: availableAmount
                        };
                        
                        // Le bon fait partie des bons du client
                        if(giftCard.length > 0){
                            // Supprimer des bons du client
                            giftCard.addClass("applied");
                            giftCard.find(".fa-times").remove();
                            giftCard.append(close.clone().attr("onclick", "removeGiftCard('" + number + "', '" + pin + "')"));

                            // Ajouter aux bons de la commande
                            giftCardsContainer.append(giftCard);
                        }
                        // Nouveau bon depuis le formulaire
                        else {
                            // Ajouter le bon
                            let giftCard = jQuery("<div>").addClass("gift-card applied")
                                .attr("data-number", number)
                                .html("<span class='bold'>Bon " + number + " :</span> " + formatFloat(availableAmount) + " CHF ")
                                .prepend(icon.clone())
                                .append(close.clone().attr("onclick", "removeGiftCard('" + number + "', '" + pin + "')"));

                            giftCardsContainer.append(giftCard);
                        }
                        
                        giftCardsContainer.removeClass("empty");
                    }
                    // Bon introuvable ou solde < 0
                    else {
                        giftCard.remove();
                        delete giftCards[number];
                    }

                    // Sauver dans le localStorage
                    checkout["giftCards"] = giftCards;
                    localStorage.setItem("checkout", JSON.stringify(checkout));
                    
                    if(giftCardsClientContainer.find(".gift-card").length === 0){
                        giftCardsClientContainer.addClass("empty");
                    }
                }
            });
        }
    </script>
@endsection

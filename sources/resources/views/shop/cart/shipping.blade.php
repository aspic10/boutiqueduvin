@extends('layouts.shop')
@section('headTitle', 'Coordonnées')

@php
$client = Auth::user();
@endphp

@section('content')
    {{-- Flux du panier --}}
    @include('includes.cart.flow', ['current' => 'shipping'])
    
    <h1>Coordonnées</h1>
    
    {{-- Invité --}}
    @guest
        <div class="alert info">
            Vous commandez en tant qu'invité(e). Souhaitez-vous vous <a href="{{ route('cart/login') }}">connecter
            ou créer un compte</a> ?
        </div>
    
        <h2>Informations personnelles</h2>
        <div class="form" id="guest-form">
            <div class="row">
                <label for="client-name">Nom complet *</label>
                <div>
                    <input id="client-name" type="text" required/>
                    <span class="error">Champ obligatoire</span>
                </div>
            </div>
            <div class="row">
                <label for="client-email">Email *</label>
                <div>
                    <input id="client-email" type="email" required/>
                    <span class="error">Email invalide</span>
                </div>
            </div>
            <div class="row">
                <label for="client-phone">Téléphone</label>
                <div>
                    <input id="client-phone" type="phone"/>
                </div>
            </div>
        </div>
    @endguest

    {{-- Adresse de livraison --}}
    <div class="columns-container">
        <div class="column one-half">
            <h2>Adresse de livraison</h2>
            <div class="form">
                <div class="row">
                    <input type="checkbox" id="pickup"/>
                    <label for="pickup">Je viens chercher la marchandise à Saxon</label>
                </div>
            </div>

            <div class="form" id="address-form">
                <div class="row">
                    <label for="name">Nom complet *</label>
                    <div>
                        <input id="name" type="text" required/>
                        <span class="error">Champ obligatoire</span>
                    </div>
                </div>

                <div class="row">
                    <label for="company">Société</label>
                    <div>
                        <input id="company"/>
                    </div>
                </div>

                <div class="row">
                    <label for="address">Adresse *</label>
                    <div>
                        <input id="address" type="text" required/>
                        <span class="error">Champ obligatoire</span>
                    </div>
                </div>

                <div class="row">
                    <label for="postal-code">NPA *</label>
                    <div>
                        <input id="postal-code" type="text" required
                               onkeypress="return onlyInt(event, false)"
                               maxlength="4"/>
                        <span class="error">NPA invalide</span>
                    </div>
                </div>

                <div class="row">
                    <label for="city">Localité *</label>
                    <div>
                        <input id="city" type="text" required/>
                        <span class="error">Champ obligatoire</span>
                    </div>
                </div>

                <div class="row">
                    <label for="city">Pays</label>
                    <div>
                        <input type="text" value="Suisse" disabled="">
                    </div>
                </div>

                <div class="row">
                    <label for="phone">Téléphone *</label>
                    <div>
                        <input id="phone" type="phone" required/>
                        <span class="error">Champ obligatoire</span>
                    </div>
                </div>

                @auth
                    <div class="row" style="margin-top: 5px; margin-bottom: 10px;">
                        <input type="button" class="grey" onclick="updateClient()"
                               disabled="disabled" id="save-info"
                               value="Enregistrer l'adresse dans mon compte"/>
                    </div>
                    <div id="info-saved" class="pop alert success">
                        Vos coordonnées ont été mises à jour
                    </div>
                @endauth
            </div>
        </div>

        {{-- Adresse de facturation --}}
        <div class="column one-half">
            <h2>Adresse de facturation</h2>
            <div class="form">
                <div class="row">
                    <input type="checkbox" id="same-address"/>
                    <label for="same-address">Adresse de facturation identique</label>
                </div>
            </div>

            <div id="billing-form" class="form">
                <div class="row">
                    <label for="lastname">Adresse de facturation *</label>
                    <div>
                        <textarea id="billing-address" required></textarea>
                        <span class="error">Champ obligatoire</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="buttons-container-flow">
        <a href="{{ route("cart") }}">
            <button class="grey">
                Retour au panier
            </button>
        </a>
        <input type="button" value="Continuer"
               onclick="checkForm()"/>
    </div>
    
    <script type="text/javascript">
        var address;
        var client;
        const emailFormat = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        const postalCodeFormat = /\d{4}/;
        
        // Invité
        var guestNameInput = jQuery("#name-guest");
        var guestForm = jQuery("#guest-form");
        
        // Livraison
        var addressForm = jQuery("#address-form");
        var pickupCheckbox = jQuery("#pickup");
        var saveInfo = jQuery("#save-info");
        var infoSavedDiv = jQuery("#info-saved");
        
        // Facturation
        var billingForm = jQuery("#billing-form");
        var sameAddressCheckbox = jQuery("#same-address");
        
        document.addEventListener('DOMContentLoaded', function(){
            var guest = localStorage.getItem("guest") || null;

            // Utilisateur non connecté
            @guest
                if(guest !== "true"){
                    localStorage.removeItem("checkout");
                    checkout = {};
                }

                guest = "true";

            // Utilisateur connecté
            @else
                if(guest !== "false"){
                    localStorage.removeItem("checkout");
                    checkout = {};
                }
                
                client = checkout["client"] || {};
                
                client.name = "{{ $client->name }}";
                client.email = "{{ $client->email }}";
                client.phone = "{{ $client->phone }}";
                
                checkout["client"] = client;
                localStorage.setItem("checkout", JSON.stringify(checkout));

                guest = "false";
            @endguest

            localStorage.setItem("guest", guest);
            
            address = checkout["address"] || null;
            client = checkout["client"] || {};
            
            // Adresse pas encore sauvée dans le localStorage
            if(address === null) {
                address = {
                    "same-address": true
                };
                
                // Connecté -> récupérer les infos du client
                @auth
                    address.name = "{{ $client->name }}";
                    address.company = "{{ $client->company_name }}";
                    address.address = "{{ $client->address }}";
                    address["postal-code"] = "{{ $client->city->postal_code }}";
                    address.city = "{{ $client->city->name }}";
                    address.phone = "{{ $client->phone }}";
                @endauth
            }
            
            // Remplir les champs selon le compte utilisateur ou le localStorage
            for(var key in address){
                jQuery("#" + key).val(address[key]);
            }
            // Remplir les champs selon le compte utilisateur ou le localStorage
            for(var key in client){
                jQuery("#client-" + key).val(client[key]);
            }
            
            // Retrait à Saxon
            if(checkout["pickup"] === true) {
                pickupCheckbox.prop("checked", true);
                addressForm.hide();
            }
            
            // Même adresse de facturation
            sameAddressCheckbox.prop("checked", address["same-address"]);
            if(address["same-address"] === true){
                billingForm.hide();
            }
            
            save();
        });
        
        
        /**
         * Modification d'un champ -> sauver dans le localStorage
         */
        jQuery("#address-form input:not(:checkbox), #address-form textarea, #billing-form input:not(:checkbox), #billing-form textarea").on("change", function(){
            var id = jQuery(this).attr("id");
            address[id] = jQuery(this).val().trim();
            save();
        });
        
        
        /**
         * Modification des informations du client -> sauver dans le localStorage
         */
        jQuery("#guest-form input").on("change", function(){
            var id = jQuery(this).attr("id").replace("client-", "");
            client[id] = jQuery(this).val().trim();
            checkout["client"] = client;
            localStorage.setItem("checkout", JSON.stringify(checkout));
        });
        
        
        /**
         * Retrait à Saxon
         */
        pickupCheckbox.on("click", function(){
            if(jQuery(this).is(":checked")){
                addressForm.hide();
            }
            else {
                addressForm.show();
            }
            
            checkout["pickup"] = jQuery(this).is(":checked");
            localStorage.setItem("checkout", JSON.stringify(checkout));
        });
        
        
        /**
         * Même adresse de facturation
         */
        sameAddressCheckbox.on("click", function(){
            if(jQuery(this).is(":checked")){
                billingForm.hide();
            }
            else {
                billingForm.show();
            }
            
            address["same-address"] = jQuery(this).is(":checked");
            save();
        });
        
        
        /**
         * Vérifier les informations avant de passer à la page suivante
         */
        function checkForm(){
            var errors = 0;
            
            // Formulaire invité
            if(guestForm.length){
                errors += checkRequiredInput(guestForm);
                
                // Adresse email
                var emailInput = jQuery("#client-email");
                
                if(emailFormat.test(emailInput.val().toLowerCase()) === false){
                    errors++;
                    emailInput.parents(".row").find("span.error").show();
                    emailInput.addClass("error");
                }
                else {
                    emailInput.parents(".row").find("span.error").hide();
                    emailInput.removeClass("error");
                }
            }
            
            // Adresse de livraison
            if(!pickupCheckbox.is(":checked")){
                errors += checkRequiredInput(addressForm);
                
                // NPA
                var postalCodeInput = jQuery("#postal-code");
                
                if(postalCodeFormat.test(postalCodeInput.val().toLowerCase()) === false){
                    errors++;
                    postalCodeInput.parents(".row").find("span.error").show();
                    postalCodeInput.addClass("error");
                }
                else {
                    postalCodeInput.parents(".row").find("span.error").hide();
                    postalCodeInput.removeClass("error");
                }
            }
            
            // Adresse de facturation
            if(!sameAddressCheckbox.is(":checked")){
                errors += checkRequiredInput(billingForm);
            }
            
            if(errors === 0){
                document.location.href = "{{ route('payment') }}";
            }
        }
        
        
        /**
         * Vérifier les champs requis et indiquer les erreurs
         */
        function checkRequiredInput(form){
            var errors = 0;
            form.find("input[required], textarea[required]").each(function(){
                if(jQuery(this).val().trim() === ""){
                    errors++;
                    jQuery(this).parents(".row").find("span.error").show();
                    jQuery(this).addClass("error");
                }
                else {
                    jQuery(this).parents(".row").find("span.error").hide();
                    jQuery(this).removeClass("error");
                }
            });
            
            return errors;
        }
        
        /**
         * Sauver dans le localStorage
         */
        function save(){
            checkout["address"] = address;
            localStorage.setItem("checkout", JSON.stringify(checkout));
            
            @auth
                if(address.name != "{{ $client->name }}"
                    || address.company != "{{ $client->company_name }}"
                    || address.address != "{{ $client->address }}"
                    || address["postal-code"] != "{{ $client->city->postal_code }}"
                    || address.city != "{{ $client->city->name }}"
                    || address.phone != "{{ $client->phone }}"){
                    saveInfo.prop("disabled", false);
                }
                else {
                    saveInfo.prop("disabled", true);
                }
            @endauth
        }
        
        
        /**
         * Mettre à jour les coordonnées du client
         */
        function updateClient(){
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            jQuery.ajax({
                type: "POST",
                url: "{{ route('updateClient') }}",
                data: {
                    address: address
                },
                success: function(){
                    // Mettre à jour les infos du client
                    client.name = address.name;
                    client.phone = address.phone;
                    checkout["client"] = client;
                    localStorage.setItem("checkout", JSON.stringify(checkout));
                    
                    // Désactiver le bouton
                    saveInfo.prop("disabled", true);
                    
                    // Message pour indiquer que ça a été sauvé
                    infoSavedDiv.addClass("visible");
                    
                    setTimeout(function(){
                        infoSavedDiv.removeClass("visible");
                    }, 3000);
                }
            });
        }
    </script>
@endsection

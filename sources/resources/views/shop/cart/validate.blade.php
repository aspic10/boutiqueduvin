@extends('layouts.shop')
@section('headTitle', 'Validation')

@php
$client = Auth::user();
@endphp

@section('content')
    {{-- Flux du panier --}}
    @include('includes.cart.flow', ['current' => 'validate'])
    
    <h1>Valider la commande</h1>
    
    {{-- Articles modifiés --}}
    <p class="alert warning hidden" id="warning-modifications">
        Certains articles ont été modifiés depuis leur ajout au panier. Veuillez
        vérifier le récapitulatif ci-dessous.
    </p>
    
    {{-- Invité --}}
    @if(is_null($client))
        <p class="alert info">
            Vous commandez en tant qu'invité(e). Souhaitez-vous vous <a href="{{ route('cart/login') }}">connecter
            ou créer un compte</a> ?
        </p>
    @endif
    
    <div class="columns-container">
        <div class="column two-thirds" style="padding-right: 10px;">
            <h2 class="no-margin-top">Récapitulatif de la commande</h2>
            {{-- Contenu du panier --}}
            @include('includes.cart.overview', ['page' => 'validate'])

            <h3>Remarque</h3>
            <textarea class="full-width" id="remark"></textarea>

            <h3>Cadeau</h3>
            <input type="checkbox" id="gift"/>
            <label for="gift">
                Cette commande est un cadeau
            </label>
            <div id="gift-message-container">
                <div class="bold">Ecrire un message au destinataire :</div>
                <textarea class="full-width" id="gift-message"></textarea>
            </div>
        </div>

        <div class="column one-third last">
            <h3 class="no-margin-top">Informations personnelles</h3>
            <div class="list-detail">
                <div class="detail">
                    <i class="fas fa-user icon"></i>
                    <span id="client-name"></span>
                </div>
                <div class="detail">
                    <i class="fas fa-envelope icon"></i>
                    <span id="client-email"></span>
                </div>
                <div class="detail">
                    <i class="fas fa-phone icon"></i>
                    <span id="client-phone"></span>
                </div>
            </div>

            <h3>Adresse de livraison</h3>
            {{-- Retrait à Saxon --}}
            <div id="pickup" class="list-detail">
                <div class="detail">
                    <i class="fas fa-dolly icon"></i>
                    Retrait de la marchandise à Saxon :
                    <p>
                        Œnothèque Millésime 2012
                        <br/>
                        F et J Galloni SA
                        <br/>
                        Route de l'Usine 4
                        <br/>
                        1907 Saxon
                    </p>
                </div>
                <div class="detail">
                    <i class="fas fa-map-marker-alt icon"></i>
                    <a href="https://goo.gl/maps/iZXcQMP95Fbzye727" target="blank">
                        Voir sur Google Maps
                    </a>
                </div>
            </div>

            {{-- Adresse de livraison précisée par le client --}}
            <div id="delivery" class="list-detail">
                <div class="detail">
                    <i class="fas fa-map-marker-alt icon"></i>
                    <span id="address"></span>
                </div>
                <div class="detail">
                    <i class="fas fa-phone icon"></i>
                    <span id="phone"></span>
                </div>
            </div>

            <h3>Adresse de facturation</h3>
            <div class="list-detail">
                <div class="detail">
                    <i class="fas fa-file-alt icon"></i>
                    <span id="billing-address"></span>
                </div>
            </div>

            <h3>Paiement</h3>
            <div class="list-detail">
                <div class="detail" id="payment-container">
                    <i class="far fa-credit-card icon"></i>
                    <span id="payment"></span>
                </div>
            </div>
        </div>
    </div>
    
    {{-- Conditions --}}
    <h3>Conditions</h3>
    <div id="terms-container">
        <input type="checkbox" id="age"/>
        <label for="age">
            Je confirme avoir <span class="colour bold">18 ans révolus</span>
        </label>
        <br/>
        
        <input type="checkbox" id="terms"/>
        <label>
            J'accepte les <span class="colour bold pointer" onclick="dialogTerms.addClass('visible')">Conditions générales de vente</span>
        </label>
    </div>
    
    <div class="buttons-container-flow">
        <a href="{{ route("cart") }}">
            <button class="grey">
                Retour au panier
            </button>
        </a>
        <input type="button" value="Envoyer la commande" onclick="addOrder()"/>
    </div>
    
    {{-- Dialog pour le paiement --}}
    <div class="dialog" id="dialog-payment">
        <div class="dialog-container">
            <div class="dialog-content"></div>
        </div>
    </div>
    
    {{-- Dialog pour les erreurs --}}
    <div class="dialog" id="dialog-error">
        <div class="dialog-container">
            <div class="dialog-content">
                <h2>Une erreur est survenue</h2>
                <p class="center">
                    <a id="error-redirect-url">
                        <button>Recharger la page</button>
                    </a>
                </p>
            </div>
        </div>
    </div>
    
    {{-- Dialog pour les CGV --}}
    <div class="dialog" id="dialog-terms">
        <div class="dialog-container">
            <i class="fas fa-times close-dialog block"
               onclick="jQuery(this).parents('.dialog').removeClass('visible')"></i>
            <div class="dialog-content">
                <h1>Conditions générales de vente</h1>
                {!! $terms !!}
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        var dialogPayment = jQuery("#dialog-payment");
        var dialogError = jQuery("#dialog-error");
        var dialogTerms = jQuery("#dialog-terms");
        
        var termsContainer = jQuery("#terms-container");
        
        var address = checkout.address || {};
        var client = checkout.client || {};
        
        var giftCards = checkout["giftCards"] || {};
        var totalCart;
        var totalToPay;
        var retryIndicator = {{ $retry }};
        
        var deliveryAddress = "";
        var billingAddress;
        
        document.addEventListener('DOMContentLoaded', function(){
            // Vérifier que l'utilisateur ne s'est pas dé/connecté entre temps
            var guest = localStorage.getItem("guest") || null;

            // Utilisateur non connecté
            @guest
                if(guest !== "true"){
                    document.location.href = "{{ route('shipping') }}";
                }
            // Utilisateur connecté
            @else
                if(guest !== "false"){
                    document.location.href = "{{ route('shipping') }}";
                }
            @endguest
            
            // Si le panier est vide -> retour à l'accueil
            if(cart === null){
                document.location.href = "{{ route('home') }}";
            }
            else {
                // Retrait à Saxon
                var pickup = checkout.pickup || false;
                
                // Adresse de facturation identique
                var sameAddress = address["same-address"];
                
                // Informations du client
                var clientName = (client.name || "").trim();
                var clientEmail = (client.email || "").trim();
                var clientPhone = (client.phone || "-").trim();
                
                // Adresse
                var addressName = (address.name || "").trim();
                var addressCompany = (address.company || "").trim();
                var addressAddress = (address.address || "").trim();
                var addressCity = (address.city || "").trim();
                var addressPostalCode = (address["postal-code"] || "").trim();
                var addressPhone = (address.phone || "").trim();
                var billingAddress = (address["billing-address"] || "").trim().replace(/(\r\n|\n|\r)/gm, "<br/>");
                
                // Remarque
                var remark = (checkout.remark || "").trim();
                
                // Cadeau
                var gift = checkout.gift || false;
                var giftMessage = (checkout.giftMessage || "").trim();
                
                // Vérifier les champs obligatoires
                if( // Nom et email du client
                    clientName === "" || clientEmail === ""
                    // Pas de retrait à Saxon -> adresse complète obligatoire
                    || (pickup === false 
                        && (addressName === "" || addressAddress === "" || addressCity === ""
                            || addressPostalCode === "" || addressPhone === ""))
                    // Adresse de facturation différente
                    || (sameAddress === false && billingAddress === "")
                    ) {
                    // Champ manquant -> retour sur la page de saisie des coordonnées
                    document.location.href = "{{ route('shipping') }}";
                }
                else {
                    jQuery("#client-name").html(clientName);
                    jQuery("#client-email").html(client.email);
                    jQuery("#client-phone").html(clientPhone);

                    // Adresse de livraison
                    if(pickup === false){
                        if(addressCompany !== ""){
                            deliveryAddress += addressCompany + "<br/>";
                        }
                        
                        deliveryAddress += addressName + "<br/>";
                        deliveryAddress += addressAddress + "<br/>";
                        deliveryAddress += addressPostalCode + " " + addressCity;

                        jQuery("#address").html(deliveryAddress);
                        jQuery("#phone").html(addressPhone);

                        jQuery("#pickup").hide();
                        jQuery("#delivery").show();
                    }
                    // Retrait à Saxon
                    else {
                        jQuery("#pickup").show();
                        jQuery("#delivery").hide();

                        deliveryAddress = "Retrait à Saxon";
                    }

                    // Adresse de facturation
                    if(address["same-address"] === true){
                        jQuery("#billing-address").html(deliveryAddress);
                    }
                    else {
                        jQuery("#billing-address").html(billingAddress);
                    }
                    
                    jQuery("#remark").html(remark);
                    
                    // Cadeau
                    if(gift === true){
                        jQuery("#gift").prop("checked", true);
                        jQuery("#gift-message-container").show();
                        jQuery("#gift-message").html(giftMessage);
                    }
                }
            }
        });
        
        
        /**
         * Changement de la remarque
         */
        jQuery("#remark").change(function(){
            checkout.remark = jQuery(this).val().trim();
            localStorage.setItem("checkout", JSON.stringify(checkout));
        });
        
        
        /**
         * Cadeau
         */
        jQuery("#gift").change(function(){
            var gift = jQuery(this).is(":checked");
            checkout.gift = gift;
            localStorage.setItem("checkout", JSON.stringify(checkout));
            
            if(gift === true){
                jQuery("#gift-message-container").show();
            }
            else {
                jQuery("#gift-message-container").hide();
            }
        });
        
        
        /**
         * Message pour le cadeau
         */
        jQuery("#gift-message").change(function(){
            checkout.giftMessage = jQuery(this).val().trim();
            localStorage.setItem("checkout", JSON.stringify(checkout));
        });
        
        
        /**
         * Montant à payer
         */
        function setTotalPay(total){
            totalCart = total;
            
            var nbGiftCards = 0;
            var giftCardsTotal = 0;
            
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            // Afficher les bons cadeaux
            for(var number in giftCards){
                // Vérifier le montant disponible de la carte cadeau
                jQuery.ajax({
                    type: "POST",
                    async: false,
                    url: "{{ route('getGiftCard') }}",
                    data: {
                        number: number,
                        pin: giftCards[number].pin
                    },
                    success: function(availableAmount){
                        if(availableAmount > 0){
                            // Montant disponible
                            giftCards[number].amount = availableAmount;

                            let amount = availableAmount;

                            // Total < carte cadeau -> déduire de la carte le solde
                            if(total < availableAmount){
                                amount = total;
                                total = 0;
                                
                                availableAmount -= amount;
                            }
                            // Utiliser la carte complète
                            else {
                                total -= availableAmount;
                                availableAmount = 0;
                            }
                            
                            // Afficher la carte sous la commande
                            let tr = giftCardTemplate.clone().show();
                            tr.find(".number").html(number);
                            tr.find(".amount").html(formatFloat(amount));
                            tr.find(".balance").html(formatFloat(availableAmount));
                            
                            giftCardTemplate.after(tr);
                            
                            nbGiftCards++;
                            giftCardsTotal += parseFloat(amount);
                        }
                        // Carte introuvable
                        else {
                            delete giftCards[number];
                        }
                        
                        // Sauver dans le localStorage
                        checkout["giftCards"] = giftCards;
                        localStorage.setItem("checkout", JSON.stringify(checkout));
                    }
                });
            }
            
            var payment = "";
            
            if(nbGiftCards > 0){
                balanceTr.find(".balance").html(formatFloat(total));
                balanceTr.show();
                
                if(nbGiftCards > 1){
                    payment = "Bons cadeaux";
                }
                else {
                    payment = "Bon cadeau";
                }
                
                payment += " : <span class='bold'>CHF " + formatFloat(giftCardsTotal) + "</span>";
            }
            
            if(total >= 0.05){
                if(nbGiftCards > 0){
                    payment += "<br/>";
                }
                
                // Moyen de paiement
                var paymentType = checkout.payment;
                if(paymentType === "prepayment"){
                    payment += "Paiement anticipé";
                }
            }

            jQuery("#payment").html(payment);
            
            totalToPay = total;
        }
        
        
        /**
         * Envoyer la commande
         */
        function addOrder(){
            termsContainer.removeClass("error");
            
            // Vérifier que les conditions sont acceptées
            if(!(jQuery("#age").is(":checked") && jQuery("#terms").is(":checked"))){
                termsContainer.addClass("error");
            }
            else {
                jQuery.ajax({
                    type: "POST",
                    async: false,
                    url: "{{ route('addOrder') }}",
                    data: {
                        cart: cart,
                        checkout: checkout,
                        totalCart: totalCart,
                        totalToPay: totalToPay,
                        nbBottles: nbBottles,
                        retryIndicator: retryIndicator
                    },
                    success: function(data){
                        if(data.error === false){
                            // Paiement par carte cadeau ou anticipé
                            // Vider le panier
                            localStorage.removeItem("cart");

                            checkout.remark = "";
                            checkout.giftMessage = "";
                            checkout.gift = false;
                            localStorage.setItem("checkout", JSON.stringify(checkout));

                            // Rediriger sur la page de confirmation
                            document.location.href = data.redirect;
                        }
                        else {
                            dialogError.addClass("visible");
                            dialogError.unbind();
                            jQuery("#error-redirect-url").attr("href", data.redirect);
                        }
                    }
                });
            }
        }
        
        
        /**
         * Ouvrir le dialog de paiement
         */
        function openDialogPayment(e){
            e.stopPropagation();
            e.preventDefault();

            // Afficher le dialog
            dialogPayment.addClass("visible");
            dialogPayment.unbind();

            // Empêche la redirection du <a>
            return false;
        }
    </script>
@endsection

@extends('layouts.shop')
@section('headTitle', 'Commande ' . $bill->id)

@section('content')
        {{-- Menu --}}
        @include('includes.client-menu', ['current' => 'clientBills'])

        <div class="column three-fourths last">
            <h2 class="no-margin-top">Commande {{ $bill->id }}</h2>

            <div class="list-detail">
                <div class="detail">
                    <i class="far fa-calendar-alt icon"
                       title="Date"></i>
                    {{ $bill->date->format("d.m.Y") }}
                </div>
                <div class="detail">
                    @if($bill->bills_status->open == false)
                        <i class="fas fa-check icon" title="Statut"></i>
                        Commande terminée
                    @else
                        <i class="fas fa-hourglass-half icon" title="Statut"></i>
                        Commande en cours de traitement
                    @endif
                </div>
                <div class="detail">
                    <i class="fas fa-file-alt icon" title="Adresse de facturation"></i>
                    {!! nl2br($bill->address) !!}
                </div>
                <div class="detail">
                    <i class="fas fa-info-circle icon" title="Remarque"></i>
                    {!! nl2br($bill->remark) !!}
                </div>
            </div>

            <h3>Détail de la commande</h3>

            <table class="full-width">
                <thead>
                    <tr>
                        <th class="center">Quantité</th>
                        <th>Désignation</th>
                        <th class="center">Format</th>
                        <th class="right">Prix</th>
                        <th class="right">Total</th>
                        <th class="center">Aperçu</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $totalPrice = 0;
                    @endphp

                    @foreach($bill->articles as $article)
                        @php
                        $billArticle = $article->pivot;
                        $quantity = $billArticle->quantity;
                        $price = round(($billArticle->price + $billArticle->price * $billArticle->vat / 100) / 0.05) * 0.05;
                        $articlePrice = $price * $quantity;
                        $totalPrice += $articlePrice;
                        @endphp

                        <tr>
                            <td class="center">{{ $quantity }}</td>
                            <td>{{ $billArticle->name }}</td>
                            <td class="center">{{ $billArticle->format }}</td>
                            <td class="right">{{ number_format($price, 2, ".", "'") }}</td>
                            <td class="right bold">{{ number_format($articlePrice, 2, ".", "'") }}</td>
                            <td class="center">
                                <a href="{{ route("wine", $article->id) }}">
                                    <i class="fas fa-search"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    
                    @foreach($bill->gift_cards_bought as $giftCard)
                        @php
                        $price = $giftCard->initial_amount;
                        $totalPrice += $price;
                        @endphp

                        <tr>
                            <td class="center">1</td>
                            <td>Bon cadeau</td>
                            <td class="center">-</td>
                            <td class="right">{{ number_format($price, 2, ".", "'") }}</td>
                            <td class="right bold">{{ number_format($price, 2, ".", "'") }}</td>
                            <td class="center"></td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="4" class="right">Sous-total</th>
                        <th class="right">{{ number_format($totalPrice, 2, ".", "'") }}</th>
                        <th/>
                    </tr>
                    <tr>
                        @php
                        $deliveryPrice = round($bill->total_taxes / 0.05) * 0.05;
                        $totalPrice += $deliveryPrice;
                        @endphp
                        <th colspan="4" class="right">Frais de port</th>
                        <th class="right">{{ number_format($deliveryPrice, 2, ".", "'") }}</th>
                        <th/>
                    </tr>
                    <tr class="total">
                        <th colspan="4" class="right">Total en CHF</th>
                        <th class="right">{{ number_format($totalPrice, 2, ".", "'") }}</th>
                        <th/>
                    </tr>
                </tfoot>
            </table>

            <div class="buttons-container-flow">
                <a href="{{ route("clientBills") }}">
                    <button>
                        Retour à la liste des commandes
                    </button>
                </a>
            </div>
        </div>
    </div>
@endsection
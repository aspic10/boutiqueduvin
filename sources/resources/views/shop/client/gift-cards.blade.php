@extends('layouts.shop')
@section('headTitle', 'Bons cadeaux')

@section('content')
        {{-- Menu --}}
        @include('includes.client-menu', ['current' => 'clientGiftCards'])

        <div class="column three-fourths last">
            <h2 class="no-margin-top">Bons cadeaux</h2>

            @if(count($validGiftCards) == 0)
                <p>Vous n'avez aucun bon cadeau actuellement</p>
            @else
                <p>
                    <span class="bold">Montant total disponible : <span class="colour">CHF {{ $availableAmount }}</span></span>
                </p>
                <div class="columns-container">
                    {{-- Détail de chaque bon cadeau --}}
                    @foreach($validGiftCards as $giftCard)
                        <div class="gift-card-overview column one-fourth">
                            <div class="title bold">
                                N° {{ $giftCard->number }}
                            </div>
                            <div class="amount bold colour">
                                CHF {{ number_format($giftCard->amount, 2, ".", "'") }}
                            </div>
                            <div class="initial">
                                Initial : CHF {{ number_format($giftCard->initial_amount, 2, ".", "'") }}
                            </div>
                            <div class="pin">
                                PIN : {{ $giftCard->pin }}
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

            <div class="buttons-container-flow">
                <input type="button" value="Ajouter un bon cadeau"
                       onclick="jQuery('#dialog-add-gift-card').addClass('visible')"/>
            </div>
            
            @if(count($usedGiftCards) > 0)
                <h3>Bons cadeaux utilisés</h3>
                <div class="columns-container">
                    {{-- Détail de chaque bon cadeau --}}
                    @foreach($usedGiftCards as $giftCard)
                        <div class="gift-card-overview column one-fourth used">
                            <div class="title bold">
                                N° {{ $giftCard->number }}
                            </div>
                            <div class="used bold">
                                CHF {{ number_format($giftCard->initial_amount, 2, ".", "'") }}
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
        
        {{-- Formulaire d'ajout d'un bon cadeau --}}
        <div class="dialog" id="dialog-add-gift-card">
            <div class="dialog-container">
                <i class="fas fa-times close-dialog"
                   onclick="jQuery(this).parents('.dialog').removeClass('visible')"></i>
                <div class="dialog-content">
                    <h2 class="title">Ajouter un bon cadeau</h2>
                    @include('includes.add-gift-card')
                </div>
            </div>
        </div>
        
        <script type="text/javascript">
            var giftCards = {};
        </script>
    </div>
@endsection
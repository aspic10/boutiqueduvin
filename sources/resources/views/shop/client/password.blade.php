@extends('layouts.shop')
@section('headTitle', 'Nouveau mot de passe')

@section('content')
        {{-- Menu --}}
        @include('includes.client-menu', ['current' => 'account'])

        <div class="column three-fourths last">
            <h2 class="no-margin-top">Changer mon mot de passe</h2>

            @if(session()->has("success"))
                <div id="info-saved" class="alert success">
                    Mot de passe enregistré
                </div>
            @endif

            <form id="password-form" method="POST" action="{{ route('changePassword') }}">
                @csrf

                <div class="row">
                    <label for="current_password">Mot de passe actuel</label>

                    <div>
                        <input id="current_password" type="password" 
                               class="@error('current_password') error @enderror" 
                               name="current_password" autofocus required/>

                        @error('current_password')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="new_password">Nouveau mot de passe</label>

                    <div>
                        <input id="new_password" type="password" class="@error('new_password') error @enderror" 
                               name="new_password" required/>

                        @error('new_password')
                            <span class="error">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <label for="new_password_confirmation">Confirmez le mot de passe</label>
                    <div>
                        <input id="new_password_confirmation" type="password" class="form-control"
                               name="new_password_confirmation" required/>
                    </div>
                </div>
            </form>

            <div class="buttons-container-flow">
                <a href="{{ route("account") }}">
                    <button class="grey">
                        Annuler
                    </button>
                </a>
                <input type="button" value="Enregistrer le mot de passe"
                       onclick="jQuery('#password-form').submit()"/>
            </div>
        </div>
    </div>
@endsection
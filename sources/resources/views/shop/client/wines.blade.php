@extends('layouts.shop')
@section('headTitle', 'Mon compte')

@section('content')
        {{-- Menu --}}
        @include('includes.client-menu', ['current' => 'clientWines'])

        <div class="column three-fourths last">
            <h2 class="no-margin-top">Vins favoris</h2>

            @if(count($articles) == 0)
                <p>Aucun article favori</p>
            @else
                {{-- Inclure le dialog pour ajouter un article au panier --}}
                @include('includes.dialog.add-to-cart')

                <div class="columns-container">
                    {{-- Détail de chaque vin --}}
                    @foreach($articles as $article)
                        @include('includes.wine-overview', ["client" => Auth::user(), "onlyVisible" => true])
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endsection
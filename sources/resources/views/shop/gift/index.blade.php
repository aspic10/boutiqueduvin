@extends('layouts.shop')

@section('headTitle', 'Cadeaux')

@section('content')
    <h1>Bons cadeaux</h1>
    <p>
        Les bons peuvent être envoyés par poste à la personne de votre choix. 
        Dans ce cas, merci de préciser son adresse de livraison lors de la 
        finalisation de votre commande. Un message personnel peut également être
        ajouté à la dernière étape de la commande.
    </p>
    
    <div class="columns-container">
        @foreach($amounts as $amount)
            <div class="gift-card-overview column one-fourth" data-amount="{{$amount}}">
                <div class="title bold">
                    BON CADEAU
                </div>
                <div class="amount bold colour">
                    CHF {{ number_format($amount, 2, ".", "'") }}
                </div>
                <div class="already-in-cart">
                    Déjà <span class="quantity-cart"></span> dans le panier
                </div>
                <div class="cart">
                    <i class="fas fa-shopping-cart" onclick="addGiftToCart(jQuery(this), {{$amount}});"></i>
                </div>
            </div>
        @endforeach
    </div>
    
    <script type="text/javascript">
        /**
         * Chargement de la page
         */
        document.addEventListener('DOMContentLoaded', function(){
            // Quantité déjà dans le panier
            for(var articleId in cart){
                var cartItem = cart[articleId];
                var type = cartItem.type;
                
                if(type === "gift"){
                    var quantity = parseInt(cartItem.quantity);
                    var price = cartItem.price;
                    var divAlreadyInCart = jQuery(".gift-card-overview[data-amount='" + price + "'] .already-in-cart");

                    divAlreadyInCart.find(".quantity-cart").html(quantity);
                    divAlreadyInCart.show();
                }
            }
        });
    </script>
@endsection
@extends('layouts.shop')

@section('headTitle', 'Bienvenue')

@section('content')
    {{-- Texte d'introduction --}}
    <h1>Bienvenue !</h1>
    {!! $text !!}

    {{-- Slider --}}
    @if(count($slides) > 0)
        <div id="slider">
            @foreach($slides as $slide)
                @php
                $article = $slide->article;
                $wine = $article->wine;
                $winePhoto = $article->photos()->first();
                $producer = $article->producer;
                if(!is_null($producer)){
                    $producerPhoto = $producer->photos()->first();
                }
                else {
                    $producerPhoto = null;
                }
                $region = $wine->region;
                $varieties = $wine->varieties;
                @endphp
                
                <div class="slide"
                     onclick="document.location.href='{{ route("wine", $wine->id) }}'">
                    <div class="content-container">
                        @if(!is_null($winePhoto))
                            <div class="picture-container">
                                <img src="{{ config('app.photo_url') }}article/{{ $wine->id }}/{{ $winePhoto->name }}"/>
                            </div>
                            <span class="diagonal"></span>
                        @endif

                        <div class="text-container">
                            <div>
                                <h1>{{ $slide->title }}</h1>
                                <h2>{{ $wine }}</h2>
                                <div class="wine-detail">
                                    {{-- Producteur --}}
                                    @if(!is_null($producer))
                                        <div class="detail">
                                            <i class="far fa-user icon"
                                               title="Producteur"></i>
                                            {{ $producer->name }}
                                        </div>
                                    @endif

                                    {{-- Région --}}
                                    @if(!is_null($region))
                                        <div class="detail">
                                            <i class="fas fa-globe-americas icon"
                                               title="Région"></i>
                                            {{ $region->name }} / {{ $region->country->name }}
                                        </div>
                                    @endif

                                    {{-- Cépages --}}
                                    @if(count($varieties) > 0)
                                        <div class="detail">
                                            <span class="iconify icon" data-icon="vs-grapes"
                                                  title="Cépages"></span>
                                            <div>
                                                <ul>
                                                    @foreach($varieties as $variety)
                                                        <li>
                                                            {{ $variety->name }}
                                                            @if(!is_null($variety->pivot->percentage) 
                                                                && $variety->pivot->percentage > 0)
                                                                {{ $variety->pivot->percentage }}%
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    @php
                    if(!is_null($producerPhoto)) {
                        $producerPhotoSrc = config('app.photo_url') . "producer/" . $producer->id . "/" . $producerPhoto->name;
                    }
                    else {
                        $producerPhotoSrc = "/img/default-producer.jpg";
                    }
                    @endphp

                    <div class="background-container">
                        <div class="picture" 
                             style="background-image: url('{{ $producerPhotoSrc }}')"></div>
                    </div>
                </div>
            @endforeach
            
            {{-- Navigation entre les slides --}}
            <div class="navigate">
                @foreach($slides as $slide)
                    <span></span>
                @endforeach
            </div>
        </div>
    @endif
    
    {{-- Informations --}}
    <div class="home-info-container columns-container">
        @foreach($icons as $icon)
            <div class="column home-info">
                <i class="{{ $icon->icon }} icon"></i>
                <h2>{{ $icon->title }}</h2>
                <p>{{ $icon->description }}</p>
            </div>
        @endforeach
    </div>
    
    <script type="text/javascript">
        // Conteneur principal du slider
        var slider = jQuery("#slider");
        // Navigation entre les slides
        var navigate = jQuery("#slider .navigate");
        // Nombre de slides
        var nbSlides = slider.find(".slide").length;
        // Index de la prochaine slide à afficher
        var nextSlide = 0;
        // Animation du slider
        var sliderInterval;
        
        jQuery(document).ready(function(){
            // Afficher la première slide
            setSlide(0);
            
            if(nbSlides > 1){
                // Démarrer le slider
                startSlider();
            }
        });
        
        // Mettre le slider en pause quand on survole
        slider.mouseenter(function(){
            stopSlider();
        });
        
        // Relancer le slider quand la souris quitte le conteneur
        slider.mouseleave(function(){
            if(nbSlides > 1){
                startSlider();
            }
        });
        
        // Navigation entre les slides avec les petits ronds
        navigate.find("span").click(function(e){
            e.preventDefault();
            stopSlider();
            setSlide(jQuery(this).index());
        });
        
        
        /**
         * Afficher une slide
         */
        function setSlide(index){
            nextSlide = index;
            
            // Faire sortir la slide courante
            slider.find(".slide.active").removeClass("active").addClass("out");
            
            // Faire entrer la prochaine slide
            slider.find(".slide").eq(nextSlide).addClass("active");
            
            // Actualiser la navigation
            navigate.find("span.active").removeClass("active");
            navigate.find("span").eq(nextSlide).addClass("active");
            
            // Réinitialiser la slide précédente pour qu'elle soit prête à être
            // réinsérée
            setTimeout(function(){
                slider.find(".slide").removeClass("out");
            }, 1000);
            
            // Prochaine slide à afficher
            nextSlide ++;

            if(nextSlide === nbSlides){
                nextSlide = 0;
            }
        }
        
        
        /**
         * Démarrer le slider
         */
        function startSlider(){
            // Arrêter le slider (évite les exécutions multiples)
            stopSlider();
            
            // Changer de slide toutes les 4 secondes
            sliderInterval = setInterval(function(){
                setSlide(nextSlide);
            }, 4000);
        }
        
        
        /**
         * Arrêter le slider
         */
        function stopSlider(){
            clearInterval(sliderInterval);
        }
    </script>
@endsection
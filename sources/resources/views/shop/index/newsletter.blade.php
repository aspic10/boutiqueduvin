@extends('layouts.shop')

@section('headTitle', 'Newsletter')

@section('content')
    <h1>Newsletter</h1>
    
    <p>
        Abonnez-vous à la newsletter et restez informés de toutes les nouveautés et
        promotions. Il est possible de vous désabonner à tout moment.
    </p>
    
    <!-- Begin MailChimp Signup Form -->
    <div id="mc_embed_signup">
        <form action="//millesime2012.us14.list-manage.com/subscribe/post?u=0aa23ead63a9b29f0bf670e26&amp;id=822434b34c" 
              method="post" id="mc-embedded-subscribe-form" 
              name="mc-embedded-subscribe-form" class="validate" 
              target="_blank" novalidate>
            <p>
                <label for="mce-FNAME">Prénom :</label>
                <input type="text" id="mce-FNAME" name="FNAME"/>
            </p>
            <p>
                <label for="mce-LNAME">Nom : </label>
                <input type="text" id="mce-LNAME" name="LNAME"/>
            </p>
            <p>
                <label for="mce-EMAIL">Adresse email : </label>
                <input type="email" id="mce-EMAIL" name="EMAIL" required="true"/>
            </p>
            
            <h3>Choix des newsletters :</h3>
            
            <p>
                <input type="checkbox" value="1" name="group[2589][1]" 
                       id="mce-group[2589]-2589-0" style="width: 20px;"/>
                Quinzaines
                <br/>
                <input type="checkbox" value="2" name="group[2589][2]" 
                       id="mce-group[2589]-2589-1" style="width: 20px;"/>
                Espace Découverte
                <br/>
                <input type="checkbox" value="4" name="group[2589][4]" 
                       id="mce-group[2589]-2589-2" style="width: 20px;"/>
                Nouveautés, dégustations, promotions
            </p>

            <!-- real people should not fill this in and expect good things 
            - do not remove this or risk form bot signups-->
            <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
            </div>
            <div style="position: absolute; left: -5000px;" aria-hidden="true">
                <input type="text" name="b_0aa23ead63a9b29f0bf670e26_822434b34c" 
                       tabindex="-1" value="">
            </div>
            
            <h3>Politique de confidentialité et protection des données</h3>
            <p>
                En aucun cas vos informations ne seront communiquées à des tiers. 
                Les renseignements collectés par le biais du formulaire seront 
                utilisés uniquement par Millésime 2012 / F et J Galloni SA à des fins 
                d'information et pour vous contacter.
            </p>
            
            <input type="submit" value="S'inscrire" 
                   name="subscribe" id="mc-embedded-subscribe" class="button">
        </form>
    </div>
    <!--End mc_embed_signup-->


    <h2>Anciennes newsletters</h2>
    <p>
        <a href="http://us14.campaign-archive1.com/home/?u=0aa23ead63a9b29f0bf670e26&id=822434b34c" 
           target="blank">Cliquez ici</a> pour accéder aux dernières newsletters.
    </p>
@endsection
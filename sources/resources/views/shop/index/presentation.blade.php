@extends('layouts.shop')

@section('headTitle', 'Présentation')

@section('content')
    {{-- Inclure la vue partielle pour le diaporama --}}
    @include('includes.pictures-diaporama')

    <h1>Présentation</h1>
    
    <h2 style="margin-bottom: 0;">Œnothèque</h2>
    <div class="columns-container">
        <div class="column one-half">
            <p>
                Ouverte en 2012, notre œnothèque est spécialisée dans les vins valaisans 
                et italiens.
            </p>

            <p>
                Tous les 15 jours, un producteur valaisan différent est mis à l'honneur 
                et ses crus sont servis au verre. Deux vins rouges italiens complètent
                cette sélection.
            </p>

            <p>
                Huit grands vins du monde peuvent également être dégustés chaque mois à 
                l'<a href="{{ route("decouverte") }}">Espace Découverte</a>.
            </p>

            <p>
                De délicieuses planchettes de charcuteries et fromages de qualité 
                accompagnent idéalement tous nos grands crus.
            </p>
        </div>
        <div class="column one-half">
            @include('includes.pictures-grid', ['pictures' => $photosOenotheque, 'minWidth' => 150])
        </div>
    </div>
    
    <h2>Magasin</h2>
    
    <div class="columns-container">
        <div class="column one-half">
            @include('includes.pictures-grid', ['pictures' => $photosMagasin, 'minWidth' => 150])
        </div>
        <div class="column one-half">
            <p>
                Nos étagères regorgent de produits d'épicerie divers : pâtes et sauces, 
                huiles et vinaigres, farines, charcuteries, fromages, biscuits, etc.
            </p>
            <p>
                Nos critères de sélection sont simples : des produits savoureux de 
                première qualité ! Nous privilégions les artisans qui travaillent sans
                additifs ni produits aux noms barbares, sans huile de palme et dans le
                respect de la nature.
            </p>
            <p>
                A l'approche des fêtes de fin d'année, nous proposons également
                les excellents panettoni de la maison Filippi.
            </p>
        </div>
    </div>
    
    <div class="columns-container">
        <div class="column one-half">
            <h3>Cadeaux</h3>
            <p>
                Nous réalisons des paniers cadeaux sur mesure : pour tous les
                budgets, avec ou sans vin, dans des caisses de vin, des cartons ou emballés
                pour envoi postal. Vous pouvez les composer vous-mêmes ou en
                choisir de tout prêts pour un cadeau de dernière minute.
            </p>
        </div>
        <div class="column one-half">
            @include('includes.pictures-grid', ['pictures' => $photosCadeaux, 'minWidth' => 150])
        </div>
    </div>
@endsection
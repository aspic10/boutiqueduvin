@extends('layouts.shop')

@section('headTitle', 'Conditions générales de vente')

@section('content')
    <h1>Conditions générales de vente</h1>
    {!! $terms !!}
@endsection
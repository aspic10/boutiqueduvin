@extends('layouts.shop')

@section('headTitle', 'Producteurs')

@section('content')
    <h1>Producteurs</h1>
    
    {{-- Filtres --}}
    @php
    $filtersArray = array();
    $column = 0;
    
    // Recherche
    $filtersArray[$column][] = array(
        "type" => "text",
        "title" => "Filtres",
        "field" => "search",
        "value" => $values["search"],
        "label" => "Recherche",
    );
    
    // Pays
    $filtersArray[$column][] = array(
        "type" => "select",
        "title" => "Origine",
        "field" => "country",
        "emptyOption" => "Pays",
        "options" => $filters["countries"],
        "selectedValue" => $values["country"],
    );

    // Région
    $filtersArray[$column][] = array(
        "type" => "region",
        "options" => $filters["regions"],
        "selectedValue" => $values["region"],
        "selectedCountry" => $values["country"],
    );
    @endphp

    @include('includes.filters.filters', [
        "filters" => $filtersArray,
        "route" => "producers",
        "appliedFilters" => $appliedFilters,
    ])
    
    <div class="nb-results">
        {{ $producers->total() }}
        
        @if($producers->total() > 1)
            producteurs trouvés
        @else
            producteur trouvé
        @endif
    </div>
    
    <div class="columns-container">
        {{-- Détail de chaque producteur --}}
        @each('includes.producer-overview', $producers, 'producer')
    </div>
    
    {{ $producers->links() }}
    
    <script type="text/javascript">
        /**
         * Recharger la page en appliquant les filtres
         */
        function redirect() {
            var search = jQuery("#search").val().trim().replace(/%/g, "%25");

            var url = '{{ route("producers", [
                "country" => "COUNTRYVALUE",
                "region" => "REGIONVALUE",
                "search" => "SEARCHVALUE",
            ]) }}';

            url = url.replace("COUNTRYVALUE", jQuery("#sel-country").val());
            url = url.replace("REGIONVALUE", jQuery("#sel-region").val());
            url = url.replace("SEARCHVALUE", search);
            
            document.location.href = url;
        }
    </script>
@endsection
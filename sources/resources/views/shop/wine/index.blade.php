@extends('layouts.shop')

@section('headTitle', 'Vins')

@section('content')
    {{-- Inclure le dialog pour ajouter un article au panier --}}
    @include('includes.dialog.add-to-cart')

    <h1>Vins</h1>
    
    {{-- Filtres --}}
    @php
    $filtersArray = array();
    $column = 0;
    
    // Recherche
    $filtersArray[$column][] = array(
        "type" => "text",
        "title" => "Filtres",
        "field" => "search",
        "value" => $values["search"],
        "label" => "Recherche",
    );
    
    // Couleur
    $filtersArray[$column][] = array(
        "type" => "colour",
        "title" => "Couleur",
        "colours" => $filters["colours"],
        "colour" => $values["colour"],
    );
    
    // Prix
    $filtersArray[$column][] = array(
        "type" => "range",
        "title" => "Prix",
        "field" => "price",
        "min" => $values["minPrice"],
        "max" => $values["maxPrice"],
        "minRange" => $filters["minPrice"],
        "maxRange" => $filters["maxPrice"],
    );
    
    // Uniquement disponibles
    $filtersArray[$column][] = array(
        "type" => "checkbox",
        "title" => "Disponibilité",
        "field" => "available",
        "label" => "Montrer uniquement les vins disponibles",
        "checked" => $values["available"],
    );
    
    // Nouvelle colonne
    $column ++;

    // Coups de coeur
    $filtersArray[$column][] = array(
        "type" => "checkbox",
        "title" => "Vins spéciaux",
        "field" => "favourite",
        "label" => "Coups de cœur",
        "checked" => $values["favourite"],
    );
/*
    // Promotions
    $filtersArray[$column][] = array(
        "type" => "checkbox",
        "field" => "sale",
        "label" => "Promotions",
        "checked" => $values["sale"],
    ); */

    // Vins primés
    $filtersArray[$column][] = array(
        "type" => "checkbox",
        "field" => "distinction",
        "label" => "Vins primés",
        "checked" => $values["distinction"],
    );

    // Vins rares
    $filtersArray[$column][] = array(
        "type" => "checkbox",
        "field" => "rare",
        "label" => "Vins rares",
        "checked" => $values["rare"],
    );

    // Nouvelle colonne
    $column ++;

    // Pays
    $filtersArray[$column][] = array(
        "type" => "select",
        "title" => "Origine",
        "field" => "country",
        "emptyOption" => "Pays",
        "options" => $filters["countries"],
        "selectedValue" => $values["country"],
    );

    // Région
    $filtersArray[$column][] = array(
        "type" => "region",
        "options" => $filters["regions"],
        "selectedValue" => $values["region"],
        "selectedCountry" => $values["country"],
    );

    // Appellations protégées (DOC, AOC, IGT...)
    $filtersArray[$column][] = array(
        "type" => "select",
        "title" => "Appellation protégée",
        "field" => "appellation",
        "emptyOption" => "Appellations",
        "options" => $filters["appellations"],
        "selectedValue" => $values["appellation"],
    );

    // Producteur
    $filtersArray[$column][] = array(
        "type" => "select",
        "title" => "Producteur",
        "field" => "producer",
        "emptyOption" => "Producteurs",
        "options" => $filters["producers"],
        "selectedValue" => $values["producer"],
    );

    // Nouvelle colonne
    $column ++;

    // Cépage
    $filtersArray[$column][] = array(
        "type" => "select",
        "title" => "Cépage",
        "field" => "variety",
        "emptyOption" => "Cépages",
        "options" => $filters["varieties"],
        "selectedValue" => $values["variety"],
    );

    // Millésime
    $filtersArray[$column][] = array(
        "type" => "select",
        "title" => "Millésime",
        "field" => "year",
        "emptyOption" => "Millésimes",
        "options" => $filters["years"],
        "selectedValue" => $values["year"],
    );

    // Format
    $filtersArray[$column][] = array(
        "type" => "select",
        "title" => "Format",
        "field" => "litre",
        "emptyOption" => "Formats",
        "options" => $filters["litres"],
        "selectedValue" => $values["litre"],
    );

    // Nouvelle colonne
    /*
    $column ++;
    
    // Caractéristiques
    $filtersArray[$column][] = array(
        "type" => "multi-checkbox",
        "title" => "Caractéristiques",
        "field" => "characteristic",
        "options" => $filters["characteristics"],
        "selectedValues" => $values["characteristics"],
    );*/
    @endphp

    @include('includes.filters.filters', [
        "filters" => $filtersArray,
        "route" => "wines",
        "appliedFilters" => $appliedFilters,
        "variety" => $values["variety"],
    ])
    
    <div class="nb-results">
        {{ $wines->total() }}
        
        @if($wines->total() > 1)
            vins trouvés
        @else
            vin trouvé
        @endif
    </div>
    
    <div class="columns-container">
        {{-- Détail de chaque vin --}}
        @foreach($wines as $wine)
            @include('includes.wine-overview', ["client" => Auth::user(), "onlyVisible" => true])
        @endforeach
    </div>
    
    {{ $wines->links() }}

    <script type="text/javascript">
        /**
         * Recharger la page en appliquant les filtres
         */
        function redirect() {
            var search = jQuery("#search").val().trim().replace(/%/g, "%25");
            var characteristics = 0;
            var characteristicsArray = [];
            var colour = 0;

            jQuery(".filter .characteristic:checked").each(function(){
                characteristicsArray.push(jQuery(this).val());
            });

            if(characteristicsArray.length > 0){
                characteristics = characteristicsArray.join("-");
            }

            if(jQuery(".filter .colour.selected").length > 0) {
                colour = jQuery(".filter .colour.selected").data("id");
            }

            var url = '{{ route("wines", [
                "colour" => "COLOURVALUE",
                "country" => "COUNTRYVALUE",
                "region" => "REGIONVALUE",
                "producer" => "PRODUCERVALUE",
                "variety" => "VARIETYVALUE",
                "characteristics" => "CHARACTERISTICSVALUE",
                "year" => "YEARVALUE",
                "litre" => "LITREVALUE",
                "favourite" => "FAVOURITEVALUE",
                "available" => "AVAILABLEVALUE",
//                "sale" => "SALEVALUE",
                "distinction" => "DISTINCTIONVALUE",
                "rare" => "RAREVALUE",
                "appellation" => "APPELLATIONVALUE",
                "minPrice" => "MINPRICEVALUE",
                "maxPrice" => "MAXPRICEVALUE",
                "search" => "SEARCHVALUE",
            ]) }}';

            url = url.replace("COLOURVALUE", colour);
            url = url.replace("COUNTRYVALUE", jQuery("#sel-country").val());
            url = url.replace("REGIONVALUE", jQuery("#sel-region").val());
            url = url.replace("PRODUCERVALUE", jQuery("#sel-producer").val());
            url = url.replace("VARIETYVALUE", jQuery("#sel-variety").val());
            url = url.replace("CHARACTERISTICSVALUE", characteristics);
            url = url.replace("YEARVALUE", jQuery("#sel-year").val());
            url = url.replace("LITREVALUE", jQuery("#sel-litre").val());
            url = url.replace("SEARCHVALUE", search);
            url = url.replace("FAVOURITEVALUE", jQuery("#favourite").is(":checked") ? 1 : 0);
            url = url.replace("AVAILABLEVALUE", jQuery("#available").is(":checked") ? 1 : 0);
//            url = url.replace("SALEVALUE", jQuery("#sale").is(":checked") ? 1 : 0);
            url = url.replace("DISTINCTIONVALUE", jQuery("#distinction").is(":checked") ? 1 : 0);
            url = url.replace("RAREVALUE", jQuery("#rare").is(":checked") ? 1 : 0);
            url = url.replace("APPELLATIONVALUE", jQuery("#sel-appellation").val());
            url = url.replace("MINPRICEVALUE", jQuery("#min-price").html().trim());
            url = url.replace("MAXPRICEVALUE", jQuery("#max-price").html().trim());
            
            document.location.href = url;
        }
    </script>
@endsection
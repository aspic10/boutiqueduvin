<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Boutique
 */
Route::namespace('Shop')->group(function () {
    /**
     * Page d'accueil
     */
    Route::get('/', 'IndexController@index')->name('home');
    
    /**
     * Présentation et contact
     */
    Route::get('/presentation', 'IndexController@presentation')->name("presentation");
    Route::get('/decouverte', 'IndexController@decouverte')->name("decouverte");
    Route::get('/newsletter', function(){
        return view('shop.index.newsletter');
    })->name("newsletter");
    
    /**
     * CGV
     */
    Route::get('/cgv', 'IndexController@terms')->name("terms");
    
    /**
     * Contact
     */
    // Contact et horaires
    Route::get('/contact', "IndexController@contact")->name("contact");
    
    /* POST */
    // Envoi du message
    Route::post('/contact', "MailController@contact")->name('sendMailContact');
    
    /**
     * Boutique
     */
    // Liste des vins
    Route::get('/wines/{producer?}/{colour?}/{country?}/{region?}/{variety?}/{year?}/{litre?}/{favourite?}/{distinction?}/{rare?}/{appellation?}/{minPrice?}/{maxPrice?}/{available?}/{search?}', 'WineController@index')->name('wines');
    // Détail d'un vin
    Route::get('/wine/{wine}', 'WineController@detail')->name('wine');
    
    /* POST */
    // Ajouter un vin aux favoris
    Route::post('/setFavourite', "ClientController@setFavourite")->name('setFavourite');
    // Informer de la disponibilité
    Route::post('/available', "MailController@available")->name('available');
    
    /**
     * Producteurs
     */
    // Liste des producteurs
    Route::get('/producers/{country?}/{region?}/{search?}', 'ProducerController@index')->name('producers');
    // Détail d'un producteur
    Route::get('/producer/{producer}', 'ProducerController@detail')->name('producer');
    
    /**
     * Cadeaux
     */
    // Liste des bons cadeaux
    Route::get('/gift', function(){
        return view('shop.gift.index', array(
            "amounts" => [20,50,100,200],
        ));
    })->name('gift');
    
//    // Abonnements
//    Route::get('/subscription', 'SubscriptionController@index')->name('subscription');
    
    /**
     * Panier d'achat
     */
    // Aperçu du panier
    Route::get('/cart', function(){
        return view('shop.cart.index');
    })->name('cart');
    // Connexion
    Route::get('/cart/login', 'CartController@login')->name("cart/login");
    // Adresse
    Route::get('/cart/shipping', function(){
        return view('shop.cart.shipping');
    })->name("shipping");
    // Paiement
    Route::get('/cart/payment', 'CartController@payment')->name("payment");
    // Validation avant transmission
    Route::get('/cart/validate/{retry?}', 'CartController@validateOrder')->name("validate");
    // Récapitulatif de la commande transmise
    Route::get('/cart/confirm/{billId}', function(){
        return view('shop.cart.confirm');
    })->name("confirm");
    
    /* POST */
    // Contenu du panier
    Route::post('/cart/get-cart-info', 'CartController@getCartInfo')->name('getCartInfo');
    // Récupérer une carte cadeau
    Route::post('/get-gift-card', 'CartController@getGiftCard')->name('getGiftCard');
    // Ajouter une carte cadeau
    Route::post('/add-gift-card', 'CartController@addGiftCard')->name('addGiftCard');
    // Calcul des frais de livraison
    Route::post('/cart/get-delivery-cost', 'CartController@getDeliveryCost')->name('getDeliveryCost');
    // Ajouter la commande
    Route::post('/cart/add-order', 'CartController@addOrder')->name('addOrder');
    
    /**
     * Compte client
     */
    // Coordonnées
    Route::get('/account', function(){
        return view('shop.client.index', ["client" => Auth::user()]);
    })->name('account')->middleware('verified');
    // Edition des coordonnées
    Route::get('/account/edit', function(){
        return view('shop.client.edit', ["client" => Auth::user()]);
    })->name('clientEdit')->middleware('verified');
    // Modification du mot de passe
    Route::get('/account/password', function(){
        return view('shop.client.password', ["client" => Auth::user()]);
    })->name('clientPassword')->middleware('verified');
    // Commandes du client
    Route::get('/account/bills', 'ClientController@bills')->name('clientBills')
        ->middleware('verified');
    // Détail d'une commande
    Route::get('/bill/{bill}', 'ClientController@bill')->name('bill')
        ->middleware('verified');
    // Vins favoris du client
    Route::get('/account/wines', 'ClientController@wines')->name('clientWines')
        ->middleware('verified');
    // Bons cadeaux du client
    Route::get('/account/gift-cards', 'ClientController@giftCards')->name('clientGiftCards')
        ->middleware('verified');
    // Suppression du compte
    Route::get('/account/delete', function(){
        return view('shop.client.delete');
    })->name('clientDelete')->middleware('verified');
    
    /* POST */
    // Mise à jour des coordonnées
    Route::post('/client/update', 'ClientController@update')->name('updateClient')
        ->middleware('verified');
    // Modification du mot de passe
    Route::post('/client/change-password', 'ClientController@changePassword')->name('changePassword')
        ->middleware('verified');
    // Suppression du compte
    Route::post('/account/delete', 'ClientController@deleteAccount')->name('deleteAccount')
        ->middleware('verified');
});

Auth::routes(['verify' => true]);
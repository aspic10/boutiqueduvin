const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    // Convertir chaque fichier .scss en .css
    .sass('resources/sass/shop/global.scss', 'public/css/tmp')
    .sass('resources/sass/shop/layout.scss', 'public/css/tmp')
    .sass('resources/sass/shop/dialog.scss', 'public/css/tmp')
    .sass('resources/sass/shop/menu.scss', 'public/css/tmp')
    .sass('resources/sass/shop/title.scss', 'public/css/tmp')
    .sass('resources/sass/shop/table.scss', 'public/css/tmp')
    .sass('resources/sass/shop/form.scss', 'public/css/tmp')
    .sass('resources/sass/shop/column.scss', 'public/css/tmp')
    .sass('resources/sass/shop/filters.scss', 'public/css/tmp')
    .sass('resources/sass/shop/pagination.scss', 'public/css/tmp')
    .sass('resources/sass/shop/input.scss', 'public/css/tmp')
    .sass('resources/sass/shop/slider.scss', 'public/css/tmp')
    .sass('resources/sass/shop/wine.scss', 'public/css/tmp')
    .sass('resources/sass/shop/producer.scss', 'public/css/tmp')
    .sass('resources/sass/shop/photo.scss', 'public/css/tmp')
    .sass('resources/sass/shop/map.scss', 'public/css/tmp')
    .sass('resources/sass/shop/cart.scss', 'public/css/tmp')
    .sass('resources/sass/shop/client.scss', 'public/css/tmp')
    .sass('resources/sass/shop/gift.scss', 'public/css/tmp')
    .sass('resources/sass/shop/miscellaneous.scss', 'public/css/tmp')
    .sass('resources/sass/shop/responsive.scss', 'public/css/tmp')
    // Grouper tous les fichiers .css en un seul
    .styles([
        'public/css/tmp/global.css',
        'public/css/tmp/layout.css',
        'public/css/tmp/dialog.css',
        'public/css/tmp/menu.css',
        'public/css/tmp/title.css',
        'public/css/tmp/table.css',
        'public/css/tmp/form.css',
        'public/css/tmp/column.css',
        'public/css/tmp/filters.css',
        'public/css/tmp/pagination.css',
        'public/css/tmp/input.css',
        'public/css/tmp/slider.css',
        'public/css/tmp/wine.css',
        'public/css/tmp/producer.css',
        'public/css/tmp/photo.css',
        'public/css/tmp/map.css',
        'public/css/tmp/cart.css',
        'public/css/tmp/client.css',
        'public/css/tmp/gift.css',
        'public/css/tmp/miscellaneous.css',
        'public/css/tmp/responsive.css'
    ], 'public/css/shop.css')
//    .styles('public/css/tmp/*.css', 'public/css/shop.css')
    ;